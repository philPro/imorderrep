window.suggestmeyes_loaded = true;

function hidetoc()
{
    document.getElementById('toc-list').style.display = 'none';
    document.getElementById('toc-show-hide').innerHTML = '<a href="javascript:showtoc()">show</a>';
}

function showtoc()
{
    document.getElementById('toc-list').style.display = 'block';
    document.getElementById('toc-show-hide').innerHTML = '<a href="javascript:hidetoc()">hide</a>';
}

function ord (string)
{
var str = string + '',
    code = str.charCodeAt(0);
  if (0xD800 <= code && code <= 0xDBFF) { // High surrogate (could change last hex to 0xDB7F to treat high private surrogates as single characters)
    var hi = code;
    if (str.length === 1) {
      return code; // This is just a high surrogate with no following low surrogate, so we return its value;
      // we could also throw an error as it is not a complete character, but someone may want to know
    }
    var low = str.charCodeAt(1);
    return ((hi - 0xD800) * 0x400) + (low - 0xDC00) + 0x10000;
  }
  if (0xDC00 <= code && code <= 0xDFFF) { // Low surrogate
    return code; // This is just a low surrogate with no preceding high surrogate, so we return its value;
    // we could also throw an error as it is not a complete character, but someone may want to know
  }
  return code;
}

function chr(codePt)
{
  if (codePt > 0xFFFF) { // Create a four-byte string (length 2) since this code point is high
    //   enough for the UTF-16 encoding (JavaScript internal use), to
    //   require representation with two surrogates (reserved non-characters
    //   used for building other characters; the first is "high" and the next "low")
    codePt -= 0x10000;
    return String.fromCharCode(0xD800 + (codePt >> 10), 0xDC00 + (codePt & 0x3FF));
  }
  return String.fromCharCode(codePt);
}

function getAnchorHref(current_href, anchor)
{
    return current_href.replace(/^(.+?)#.*$/,'$1')+'#'+anchor;
}

function getParams()
{
    var key;
    var val;
    var spl;
    var paramArray = {};
    var qstring = window.location.href.split('?');
    if (qstring.length > 1)
    {
        var params = qstring[1].split('&');
        for (var x in params)
        {
            spl = params[x].split('=');
            key = spl[0];
            val = spl[1];
            paramArray[key] = val;
        }
    }
    return paramArray;
}

function text_to_slug(txt)
{
    var div = document.createElement("div");
    div.innerHTML = txt;
    var text = div.textContent || div.innerText || "";

    text = text.replace(/[^A-Za-z0-9]/,'');
    text = text.toLowerCase();

    text = text.replace(/ /,'-');

    text = text.replace(/--+/,'-');

    return text;
}

var game_icons = [{"game":"Story of Seasons","url":"\/story-of-seasons\/","icon":"story-of-seasons.png","released":"February 10, 2015"},
{"game":"Pok\u00e9mon Omega Ruby and Alpha Sapphire","url":"\/pokemon-omega-ruby-alpha-sapphire\/","icon":"omega-ruby-alpha-sapphire.png","released":"November 21, 2014"},
{"game":"Disco Zoo","url":"\/disco-zoo\/","icon":"disco-zoo.png","released":"February 27, 2014"},
{"game":"The Legend of Zelda: A Link Between Worlds","url":"\/zelda-link-between-worlds\/","icon":"lbw.png","released":"November 30, 2013"},
{"game":"Pokémon X and Y","url":"\/pokemon-x-y\/","icon":"pokemon-x-y.png","released":"October 1, 2013"},
{"game":"Animal Crossing: New Leaf","url":"\/animal-crossing-new-leaf\/","icon":"new-leaf.png","released":"June 9, 2013"},
{"game":"Nimble Quest","url":"\/nimble-quest\/","icon":"nimblequest.png","released":"March 28, 2013"},
{"game":"Zero Escape: Virtue's Last Reward","url":"\/virtues-last-reward\/","icon":"vlr.png","released":"November 29, 2012"},
{"game":"Little Big Planet Vita","url":"\/little-big-planet-vita\/","icon":"lbpvita.png","released":"September 12, 2012"},
{"game":"Pokémon Black 2 and White 2","url":"\/pokemon-black-2-white-2\/","icon":"black-2-white-2.png","released":"June 23, 2012"},
{"game":"Pocket Planes","url":"\/pocket-planes\/","icon":"pocketplanes.png","released":"June 13, 2012"},
{"game":"The Legend of Zelda: Skyward Sword","url":"\/zelda-skyward-sword\/","icon":"skyward-sword.png","released":"November 18, 2011"},
{"game":"Professor Layton's London Life","url":"\/professor-layton-london-life\/","icon":"london-life.png","released":"October 17, 2011"},
{"game":"Tiny Tower","url":"\/tiny-tower-guide\/","icon":"tinytower.png","released":"June 23, 2011"},
{"game":"Pokédex 3D Guide","url":"\/pokedex-3d-guide\/","icon":"pokedex-3d.png","released":"June 7, 2011"},
{"game":"Portal 2","url":"\/portal-2-walkthrough\/","icon":"portal-2.png","released":"April 19, 2011"},
{"game":"Find Mii 3DS Walkthrough","url":"\/find-mii-3ds\/","icon":"find-mii-3ds.png","released":"February 26, 2011"},
{"game":"Nintendogs + Cats Guide","url":"\/nintendogs-plus-cats\/","icon":"nintendogs-cats.png","released":"February 26, 2011"},
{"game":"Super Scribblenauts Walkthrough","url":"\/super-scribblenauts\/","icon":"super-scribblenauts.png","released":"October 12, 2010"},
{"game":"Pokémon Black and White Walkthrough","url":"\/pokemon-black-white\/","icon":"pokemon-bw.png","released":"September 18, 2010"},
{"game":"Pocket Frogs Guide","url":"\/pocket-frogs\/","icon":"pocket-frogs.png","released":"September 15, 2010"},
{"game":"Nine Hours, Nine Persons, Nine Doors Walkthrough","url":"\/nine-hours-nine-persons-nine-doors\/","icon":"ninehours.png","released":"December 10, 2009"},
{"game":"Zero Escape","url":"\/zero-escape\/","icon":"zero-escape.png","released":"December 10, 2009"},
{"game":"The Legend of Zelda: Spirit Tracks Walkthrough","url":"\/zelda-spirit-tracks\/","icon":"spirittracks.png","released":"December 7, 2009"},
{"game":"Animal Crossing: Wild World Guide","url":"\/acww\/","icon":"acww.png","released":"November 23, 2009"},
{"game":"Pokémon HeartGold and SoulSilver Walkthrough","url":"\/pokemon-heartgold-soulsilver\/","icon":"pokemon.png","released":"September 12, 2009"},
{"game":"Dragon Quest IX","url":"\/dragon-quest-ix\/","icon":"dqix.png","released":"July 11, 2009"},
{"game":"The Sims 3 Guide","url":"\/the-sims-3\/","icon":"sims3.png","released":"June 2, 2009"},
{"game":"Portal Walkthrough","url":"\/portal-walkthrough\/","icon":"portal.png","released":"October 9, 2007"},
{"game":"Contact for Nintendo DS Walkthrough","url":"\/contact-ds\/","icon":"contact-ds.png","released":"March 30, 2006"},
{"game":"White Chamber Walkthrough","url":"\/whitechamber\/","icon":"whitechamber.png","released":"December 20, 2005"},
{"game":"The Legend of Zelda: The Minish Cap Walkthrough","url":"\/zelda-minish-cap\/","icon":"minish.png","released":"November 4, 2004"},
{"game":"Blue Chamber Walkthrough","url":"\/blue-chamber\/","icon":"blue-chamber.png","released":"August 30, 2004"},
{"game":"Crimson Room Walkthrough","url":"\/crimson\/","icon":"crimson-room.png","released":"June 27, 2004"},
{"game":"Viridian Room Walkthrough","url":"\/viridian\/","icon":"viridian.png","released":"April 29, 2004"},
{"game":"EVE Online Guide","url":"\/eve-online-guide\/","icon":"eveonline.png","released":"May 6, 2003"},
{"game":"Final Fantasy Tactics Advance Walkthrough","url":"\/tactics-advance\/","icon":"ffta.png","released":"February 14, 2003"},
{"game":"The Legend of Zelda: The Wind Waker Walkthrough","url":"\/zelda-wind-waker\/","icon":"windwaker.png","released":"December 13, 2002"},
{"game":"Pok\u00e9mon Ruby, Sapphire, Emerald","url":"\/pokemon-ruby-sapphire-emerald\/","icon":"pokemon-rse.png","released":"November 21, 2002"},
{"game":"Metroid Fusion Walkthrough","url":"\/metroid-fusion\/","icon":"metroidfusion.png","released":"November 17, 2002"},
{"game":"The Legend of Zelda: Oracle of Ages","url":"\/zelda-oracle-of-ages\/","icon":"oracle-of-ages.png","released":"February 27, 2001"},
{"game":"The Legend of Zelda: Oracle of Seasons","url":"\/zelda-oracle-of-seasons\/","icon":"oracle-seasons.png","released":"February 27, 2001"},
{"game":"The Legend of Zelda: Majora's Mask","url":"\/zelda-majoras-mask\/","icon":"majoras-mask.png","released":"April 27, 2000"},
{"game":"The Legend of Zelda: Ocarina of Time Walkthrough","url":"\/ocarina-of-time\/","icon":"ocarina.png","released":"November 21, 1998"},
{"game":"Dink Smallwood","url":"\/dink-smallwood-walkthrough\/","icon":"dink-smallwood.png","released":"January 1, 1997"},
{"game":"Pokémon","url":"\/pokemon\/","icon":"pokeball.png","released":"February 27, 1996"},
{"game":"Tales of Phantasia Walkthrough","url":"\/tales-of-phantasia\/","icon":"tales.png","released":"December 15, 1995"},
{"game":"Chrono Trigger","url":"\/chrono-trigger\/","icon":"chronotrigger.png","released":"March 11, 1995"},
{"game":"EarthBound Walkthrough","url":"\/earthbound-walkthrough\/","icon":"earthbound.png","released":"August 27, 1994"},
{"game":"QR Code Tutorial","url":"\/qr-code-tutorial\/","icon":"qr-code.png","released":"June 30, 1994"},
{"game":"Final Fantasy 3\/6 Walkthrough","url":"\/final-fantasy-three-six\/","icon":"ff36.png","released":"April 2, 1994"},
{"game":"Illusion of Gaia","url":"\/illusion-of-gaia\/","icon":"illusion-of-gaia.png","released":"November 27, 1993"},
{"game":"Secret of Mana Walkthrough","url":"\/secret-of-mana\/","icon":"secret-of-mana.png","released":"August 6, 1993"},
{"game":"Soul Blazer Walkthrough","url":"\/soul-blazer\/","icon":"soul-blazer.png","released":"January 31, 1992"},
{"game":"The Legend of Zelda: A Link to the Past Walkthrough","url":"\/zelda-link-to-the-past\/","icon":"lttp.png","released":"November 21, 1991"},
{"game":"Zelda II: The Adventure of Link Walkthrough","url":"\/zelda-ii-adventure-of-link\/","icon":"link.png","released":"January 14, 1987"},
{"game":"Picross Tutorial","url":"\/picross\/","icon":"picross.png","released":"January 1, 1987"},
{"game":"Chess","url":"\/chess\/","icon":"chess2.png","released":"January 1, 1802"}];

function getAjaxLoader()
{
    var loadimg = document.createElement('img');
    loadimg.setAttribute('src','/ajax-loader.gif');
    loadimg.setAttribute('alt','Loading...');
    return loadimg;
}

function updateGameIcons()
{
    guidebox = document.getElementById('other-guides');
    if ( (null === guidebox) || (guidebox.style.display == 'none') )
    {
        return;
    }

    while (guidebox.firstChild)
    {
        guidebox.removeChild(guidebox.firstChild);
    }
    var loadimg = getAjaxLoader();
    loadimg.setAttribute('id','game-icons-loader');
    guidebox.appendChild(loadimg);

    game_icons.sort(function (a,b)
    {
        return Date.parse(b.released)-Date.parse(a.released);
    });

    var p = document.createElement('p');
    p.appendChild(document.createTextNode('Get help with games!'));
    guidebox.appendChild(p);

    var holder;
    var a;
    var icon;
    var ctr = 0;
    var morelink = document.createElement('div');
    morelink.setAttribute('id','more-games-link');
    morelink.setAttribute('style','display: block; clear: both');
    var morelinka = document.createElement('a');
    morelinka.setAttribute('href','javascript:toggle_game_display()');
    morelinka.setAttribute('id','morelinkhref');
    morelinka.appendChild(document.createTextNode('Show More Games'));
    morelink.appendChild(morelinka);
    var morebox = document.createElement('div');
    morebox.setAttribute('id','more-games-container');
    morebox.setAttribute('style','display: none;');
    var clearboth = document.createElement('div');
    clearboth.setAttribute('style','clear: both');
    clearboth.appendChild(document.createTextNode(' '));
    var spriteclassname = '';
    var x = 0;
    for (x in game_icons)
    {
        try
        {
            spriteclassname = game_icons[x].icon.replace('.','');
            holder = document.createElement('div');
            ++ctr;
            if ( (ctr == 1) || ((ctr-1)%3==0) )
            {
                guidebox.appendChild(clearboth);
                holder.setAttribute('class','game-icon-holder first-game-icon '+spriteclassname);
            }
            else if (ctr%3==0)
            {
                holder.setAttribute('class','game-icon-holder last-game-icon '+spriteclassname);
            }
            else
            {
                holder.setAttribute('class','game-icon-holder '+spriteclassname);
            }
            a = document.createElement('a');
            a.setAttribute('href',game_icons[x].url);
            icon = document.createElement('img');
    //        icon.setAttribute('src','/game-icons/'+game_icons[x].icon);
            icon.setAttribute('src','/smallestblank.gif');
    //        icon.setAttribute('alt',game_icons[x].game);
            icon.setAttribute('style','width:40px;height:40px');
            a.appendChild(icon);
            holder.appendChild(a);
            if (ctr > 3)
            {
                morebox.appendChild(holder);
            }
            else
            {
                guidebox.appendChild(holder);
            }
            if (ctr == 3)
            {
                guidebox.appendChild(morelink);
                guidebox.appendChild(morebox);
            }
        }
        catch (e)
        {
        }
    }
    guidebox.appendChild(clearboth);
    guidebox.removeChild(loadimg);
}


window.onload = function()
{
    updateGameIcons();
}