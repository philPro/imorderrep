API Command Example:
Byte Sequence of a TX Request (0x10 Transmit Request, PDF-Page 144)
0x7E, 0x00, 0x12, 0x10, 0x01, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xB0, 0x99, 0x37, 0xFF, 0xFE, 0x00, 0x00, 0x54, 0x65, 0x73, 0x74, 0xDC

StartByte, Length #1, Length #2, Frame Type, Frame ID, 64 Bit Destination Address (8 High Bytes, 8 Low Bytes), 16 Bit Destination Address(disabled with 0xFF, 0xFE),
2 reserved (case of protocol changes), {Payload} , Checksum

0 StartByte: 0x7E = '~'
1 Length #1: 0x00
2 Length #2: 0x12 = 18 decimal -> 18 Bytes long
	Length: 18 = Number of bytes between the length and the checksum (both excluded from counting)
3 Frame Type: -> 0x10 ZigBee Transmit Request (ZNet, ZigBee) *find all Frame Types on bottom of this document
4 Frame ID: 0x01 (from 0x00 to 0xFF)
	-> to link Xbee's response (same ID)
	--> exception: Frame Id = 0x00: suppress any response from the Xbee, command will be carried out though
5 - 8  64 Bit Address High: 0x00, 0x13, 0xA2, 0x00 (see pics in C:\Projekte\imorderrep\Information\Xbee\Xbee devices)
9 - 12 64 Bit Address Low: 00x40, 0xB0, 0x99, 0x37 (see pics in C:\Projekte\imorderrep\Information\Xbee\Xbee devices)
13 16-bit Address High: 0xFF
14 16-bit Address Low:  0xFE --> disabled (0xFF, 0xFE)
15 reserved Byte: 0x00 (always 0x00)
16 reserved Byte: 0x00 (always 0x00)
17 - 20 Payload: 0x54, 0x65, 0x73, 0x74 -> "Test" ... actual data byte "array"
	-> depending on specific settings (i.e. encryption ~70 bytes possible per transmission)
21 Checksum: 0xDC
	-> the sum of bytes from offset/byte #3 (Frame Type) to this byte(excluded -> bytes 0 - 2 & checksum excluded)
	-> calculation: * sum up relevant bytes: 0x10 + 0x01 + 0x00 + 0x13 + 0xA2 + 0x00 + 0x40 + 0xB0 + 0x99 + 0x37 + 0xFF + 0xFE + 0x00 + 0x00 + 0x54 + 0x65 + 0x73 + 0x74 = 623
					* subtract the 2 far right literals of the result from FF		 -> FF - 23 = DC = CheckSum

	If an API data packet is composed with an incorrect checksum, the radio will consider the packet invalid and the data will be ignored.
	To verify the checksum of an API packet add all bytes including the checksum (do not include the delimiter and length) and if correct, the last two far right digits of the sum will equal FF.
	
	0x10 + 0x01 + 0x00 + 0x13 + 0xA2 + 0x00 + 0x40 + 0xB0 + 0x99 + 0x37 + 0xFF + 0xFE + 0x00 + 0x00 + 0x54 + 0x65 + 0x73 + 0x74 + 0xDC = 6FF

Reply (C:\Projekte\imorderrep\Information\Xbee\DataSheets\Building Wireless Sensor Networks by Rob Faludi.pdf, PDF-Page 148) :
(successful) reply from remote Xbee (i.e. Router), received on local Xbee(i.e. Coordinator):
126, 0, 7, 139, 1, 27, 150, 0, 0, 0, 194, 
7E, 0, 7, 8B, 1, 1B, 96, 0, 0, 0, C2, 
0x7E ... Start Byte
0x00 ... Length #1
0x07 ... Length #2
0x8B ... Frame Type (AT Command Response (802.15.4, ZNet, ZigBee))
0x01 ... Frame Id (same as Command)
0x1B ... 16-bit Address High (Destination module (Router))
0x96 ... 16-bit Address Low (Destination module (Router))
	--> if successful: 16-bit Address where the packet was delivered to
	--> if not successful: 16-bit Address that was provided in the Transmit Request frame (command above)
0x00 ... number of application transmission retries
0x00 ... delivery status (0x00 = success, 0x01 = MAC ACK failure, ... more in documentation)
0x00 ... discovery status (0x00 = no discovery overhead, 0x01 = Address discovery)
0xC2 ... Checksum
	

AT Command Example:
0x7E 0x00 0x04 0x08 0x01 0x4E 0x4A {0x01 0x02 0x03 0x04...} 0x5E

StartByte, Length #1, Length #2, Frame Type, Frame ID, AT Command, {Payload} , Checksum
0 StartByte: 0x7E = '~'
1 Length #1: 0x00
2 Length #2: 0x04 -> 4 Bytes long
	Length: 4 = Number of bytes between the length and the checksum (both excluded from counting)
3 Frame Type: -> 0x08 AT command
4 Frame ID: 0x01 (from 0x00 to 0xFF)
	-> to link Xbee's response (same ID)
	--> exception: Frame Id = 0x00: suppress any response from the Xbee, command will be carried out though
5 AT Command #1: 0x4E = 'N'
6 AT Command #2: 0x4A = 'J'
{7 Payload: {0x01 0x02 0x03 0x04...} Hex Values for Payload}
7 Checksum: 0xFF the 8-bit sum of bytes from offset/byte #3 (Frame Type) to this byte(excluded -> bytes 0 - 2 & checksum excluded)
	-> calculation: * sum up relevant bytes: 0x08 + 0x01 + 0x4E + 0x4A = A1
					* subtract the 2 far right literals from FF		 -> FF - A1 = 5E = CheckSum

	If an API data packet is composed with an incorrect checksum, the radio will consider the packet invalid and the data will be ignored.
	To verify the checksum of an API packet add all bytes including the checksum (do not include the delimiter and length) and if correct, the last two far right digits of the sum will equal FF.
	
	0x08 + 0x01 + 0x4E + 0x4A + 0x5E = FF
	

	
API Frame Types							
Frame	Type Description				
0x00:	TX (Transmit) Request: 64-bit address (802.15.4)
0x01:	TX (Transmit) Request: 16-bit address (802.15.4)
0x08:	AT Command (802.15.4, ZNet, ZigBee)
0x09:	AT Command Queue Payload Value (802.15.4, ZNet, ZigBee)
0x17:	Remote Command Request (802.15.4, ZNet, ZigBee)
0x80:	RX (Receive) Packet: 64-bit Address (802.15.4)
0x81:	RX (Receive) Packet: 16-bit Address (802.15.4)
0x82:	RX (Receive) Packet: 64-bit Address IO (802.15.4)
0x83:	RX (Receive) Packet: 16-bit Address IO (802.15.4)
0x88:	AT Command Response (802.15.4, ZNet, ZigBee)
0x89:	TX (Transmit) Status (802.15.4)
0x8A:	Modem Status (802.15.4, ZNet, ZigBee)
0x97:	Remote Command Response (802.15.4, ZNet, ZigBee)
0x10:	ZigBee Transmit Request (ZNet, ZigBee)
0x11:	Explicit Addressing ZigBee Command Frame (ZNet, ZigBee)
0x8B:	ZigBee Transmit Status (ZNet, ZigBee)
0x90:	ZigBee Receive Packet (AO=0) (ZNet, ZigBee)
0x91:	ZigBee Explicit Rx Indicator (AO=1) (ZNet, ZigBee)
0x92:	ZigBee IO Data Sample Rx Indicator (ZNet, ZigBee)
0x94:	XBee Sensor Read Indicator (AO=0) (ZNet, ZigBee)
0x95:	Node Identification Indicator (AO=0) (ZNet, ZigBee)


Adressing:
	http://www.digi.com/support/kbase/kbaseresultdetl?id=2187
	http://www.digi.com/support/kbase/kbaseresultdetl?id=2184
	
	https://www.npmjs.org/package/xbee-api