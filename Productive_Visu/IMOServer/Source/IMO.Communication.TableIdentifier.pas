unit IMO.Communication.TableIdentifier;

interface

uses
  Dialogs, System.SysUtils, System.Classes,
  Spring.Services, Spring.Container,
  IMO.Communication.Interfaces, IMO.Communication.Errors, IMO.DataModul.Interfaces;

  { ################### PROTOCOL ################### }
  { #                                              # }
  { #----------------------------------------------# }
  { #     REQUESTS         |         REPLIES       # }
  { #----------------------------------------------# }
  { #     SENDQR           |          TIMEOUT      # }
  { #    DELETEQR          |       CLIENTNOTFOUND  # }
  { #  CLIENTONLINE        |         CLIENTRDY     # }
  { #    NOT SET           |      DATATRANSMITTED  # }
  { #                      |         NOT SET       # }
  { #                      |                       # }
  { ################################################ }

type
  TTIConnection = class(TInterfacedObject, ICommunicationTIConn)
  private
    [Inject('CommunicationArduinoSerial')]
    //[Inject('CommunicationTCPIndy')]
    FCommunicationDriver: ICommunicationDriver;
    FsWholeReply: String;
    FbReplyStarted: Boolean;
    FActiveMessages: TInterfaceList;
    FEvConnStateChanged: TTIConnectionStateChanged;
    procedure OnMsgReceived(const aMsg: String);
    procedure SendMessage(aTableID: IDataModulTableIdentifier; aReq: TRequest);
    procedure ProcessReply(aMsg: String);
  public
    constructor Create;
    destructor Destroy; override;
    procedure InitializeConnection;
    procedure CheckTableIdentifierOnline(aTableIdentifier: IDataModulTableIdentifier);
    procedure SendQRCodeToTI(aTableIdentifier: IDataModulTableIdentifier);
    procedure DeleteQRCodeOnTI(aTableIdentifier: IDataModulTableIdentifier);
    procedure SetTIConnectionStateChangedEvent(aConnStateChanged: TTIConnectionStateChanged);
    function  GetTIConnectionStateChangedEvent: TTIConnectionStateChanged;
    property OnConnectionStateChanged: TTIConnectionStateChanged read GetTIConnectionStateChangedEvent write SetTIConnectionStateChangedEvent;
  end;

implementation

uses
  System.StrUtils;

{ TTIConnection }

constructor TTIConnection.Create;
begin
  inherited Create;
  FsWholeReply := '';
  FbReplyStarted := False;

  FActiveMessages := TInterfaceList.Create;
end;

destructor TTIConnection.Destroy;
begin
  FCommunicationDriver.DisconnectMaster;
  if Assigned(FActiveMessages) then FActiveMessages.Free;
  inherited;
end;


procedure TTIConnection.InitializeConnection;
begin
  { Initializations }
  FCommunicationDriver.OnMessageReceived := OnMsgReceived;
  FActiveMessages.Clear;

  if not FCommunicationDriver.InitializeMaster then
    raise ECommunicationError.Create('Failed to initialize Connection-Driver! Please check if the Master is connected to the PC or the server is running!');
end;

procedure TTIConnection.CheckTableIdentifierOnline(aTableIdentifier: IDataModulTableIdentifier);
begin
  SendMessage(aTableIdentifier, reqClientOnline);
end;

procedure TTIConnection.SendQRCodeToTI(aTableIdentifier: IDataModulTableIdentifier);
begin
  SendMessage(aTableIdentifier, reqSendQR);
end;

procedure TTIConnection.DeleteQRCodeOnTI(aTableIdentifier: IDataModulTableIdentifier);
begin
  SendMessage(aTableIdentifier, reqDeleteQR);
end;

procedure TTIConnection.SetTIConnectionStateChangedEvent(aConnStateChanged: TTIConnectionStateChanged);
begin
  FEvConnStateChanged := aConnStateChanged;
end;

function TTIConnection.GetTIConnectionStateChangedEvent: TTIConnectionStateChanged;
begin
  Result := FEvConnStateChanged;
end;

procedure TTIConnection.OnMsgReceived(const aMsg: String);
var
  iStartPos: Integer;
  iStopPos: Integer;
begin
  iStartPos := Pos(START_CHAR, aMsg);
  iStopPos := Pos(END_CHAR, aMsg);

  if (iStartPos > 0) AND (iStopPos > 0) then
  begin
    { Die gesamte Message befindet sich im aktuellen String }
    FsWholeReply := Copy(aMsg, iStartPos + 1, iStopPos - iStartPos - 1);
    // ########################## DO SMTH with the reply
    ProcessReply(FsWholeReply);
    FsWholeReply := '';
    FbReplyStarted := False;
  end else
  if iStartPos > 0 then
  begin
    { Nur Beginn der Message vorhanden }
    FsWholeReply := Copy(aMsg, iStartPos + 1, Length(aMsg) - iStartPos);
    FbReplyStarted := True;
  end else
  if FbReplyStarted then
  begin
    { wir befinden uns nun mitten in der Message }
    if iStopPos > 0 then
    begin
      { Ende der message gefunden }
      FsWholeReply := FsWholeReply + Copy(aMsg, 1, iStopPos - 1);
      // ########################## DO SMTH with the reply
      ProcessReply(FsWholeReply);
      FsWholeReply := '';
      FbReplyStarted := False;
    end else
    begin
      { Ende noch nicht gefunden }
      FsWholeReply := FsWholeReply + aMsg;
    end;
  end;
end;

procedure TTIConnection.SendMessage(aTableID: IDataModulTableIdentifier; aReq: TRequest);
var
  idGUI: TGUID;
  CommProt: ICommunicationProtocol;
begin
  CommProt := ServiceLocator.GetService<ICommunicationProtocol>;
  CreateGUID(idGUI);
  CommProt.GUID := GUIDToString(idGUI);
  CommProt.ReqMessage := aReq;
  CommProt.RepMessage := repNotSet;
  CommProt.TableID := aTableID;
  CommProt.TimeStamp := Now;

  FActiveMessages.Add(CommProt);

  FCommunicationDriver.SendMessage(CommProt.ToSendString);
end;

procedure TTIConnection.ProcessReply(aMsg: string);
var
  CommProt: ICommunicationProtocol;
  iListIdx: Integer;

  function GetListIdx(aGUID: String): Integer;
  var
    iTmp: Integer;
  begin
    Result := -1;

    for iTmp := 0 to FActiveMessages.Count-1 do
    begin
      if ICommunicationProtocol(FActiveMessages.Items[iTmp]).GUID = aGUID then
      begin
        Result := iTmp;
        Break;
      end;
    end;
  end;

begin
  CommProt := ServiceLocator.GetService<ICommunicationProtocol>;

  CommProt.FromSendString(aMsg);

  iListIdx := GetListIdx(CommProt.GUID);
  if iListIdx < 0 then
    raise ECommunicationError.Create('Reply GUID not found in Active-Messages!' + sLineBreak + 'GUID: ' + CommProt.GUID);

  { TI nicht verf�gbar }
  if CommProt.RepMessage in [repTimeout, repClientNotFound] then
  begin
    ICommunicationProtocol(FActiveMessages.Items[iListIdx]).TableID.Connected := False;
  end else
  { TI verf�gbar }
    if CommProt.RepMessage in [repClientRdy] then
    begin
      ICommunicationProtocol(FActiveMessages.Items[iListIdx]).TableID.Connected := True;
    end else
    { TI Daten �bertragen }
    if CommProt.RepMessage in [repDataTransmitted] then
    begin
      ICommunicationProtocol(FActiveMessages.Items[iListIdx]).TableID.Connected := True;
      // QR-Code is on SLAVE now -> Save in DB
      if Assigned(FEvConnStateChanged) then
        FEvConnStateChanged(ICommunicationProtocol(FActiveMessages.Items[iListIdx]).TableID, CommProt.RepMessage);
    end;

  { Delete from Active messages }
  FActiveMessages.Delete(iListIdx);
end;

initialization
  GlobalContainer.RegisterType<TTIConnection>.Implements<ICommunicationTIConn>('CommunicationTIConn');

end.
