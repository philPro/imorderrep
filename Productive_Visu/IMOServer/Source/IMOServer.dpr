program IMOServer;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  IMO.Communication.Core in 'IMO.Communication.Core.pas',
  IMO.Communication.Errors in 'IMO.Communication.Errors.pas',
  IMO.Communication.Interfaces in 'IMO.Communication.Interfaces.pas',
  IMO.Communication.Protocol in 'IMO.Communication.Protocol.pas',
  IMO.Communication.Serial.Arduino in 'IMO.Communication.Serial.Arduino.pas',
  IMO.Communication.TableIdentifier in 'IMO.Communication.TableIdentifier.pas',
  IMO.Communication.TCP.Indy in 'IMO.Communication.TCP.Indy.pas',
  IMO.Settings.Global in 'IMO.Settings.Global.pas',
  IMO.Settings.Interfaces in 'IMO.Settings.Interfaces.pas',
  uMainForm in 'uMainForm.pas' {MainForm},
  IMO.DataModul.Core in '..\..\Global\DataModul\IMO.DataModul.Core.pas',
  IMO.DataModul.Errors in '..\..\Global\DataModul\IMO.DataModul.Errors.pas',
  IMO.DataModul.Interfaces in '..\..\Global\DataModul\IMO.DataModul.Interfaces.pas',
  IMO.DataModul.Meeting.Item in '..\..\Global\DataModul\IMO.DataModul.Meeting.Item.pas',
  IMO.DataModul.Meeting in '..\..\Global\DataModul\IMO.DataModul.Meeting.pas',
  IMO.DataModul.TableIdentifier.Item in '..\..\Global\DataModul\IMO.DataModul.TableIdentifier.Item.pas',
  IMO.DataModul.TableIdentifier in '..\..\Global\DataModul\IMO.DataModul.TableIdentifier.pas',
  IMO.DataModul.TableIdentifier.QRCode in '..\..\Global\DataModul\IMO.DataModul.TableIdentifier.QRCode.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
