object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object ButtonInit: TButton
    Left = 120
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Init'
    TabOrder = 0
    OnClick = ButtonInitClick
  end
  object ButtonDeliver: TButton
    Left = 320
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Deliver'
    TabOrder = 1
    OnClick = ButtonDeliverClick
  end
end
