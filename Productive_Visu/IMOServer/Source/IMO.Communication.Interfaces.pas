unit IMO.Communication.Interfaces;

interface

uses
  IMO.DataModul.Interfaces;

type
  TRequest = (reqSendQR, reqDeleteQR, reqClientOnline, reqNotSet);
  TReply = (repTimeout, repClientNotFound, repClientRdy, repDataTransmitted, repNotSet);

type
  TMessageReceivedEvent = procedure(const aMsg: String) of Object;
  TTIConnectionStateChanged = procedure(aTableIdentifier: IDataModulTableIdentifier; aReply: TReply) of Object;

type
  ICommunicationProtocol = interface(IInterface)
    ['{76995274-C104-4FB4-93D4-82FEF600AE68}']
    procedure SetGUID(aGUID: String);
    function GetGUID: String;
    procedure SetReqMessage(aReq: TRequest);
    function GetReqMessage: TRequest;
    procedure SetRepMessage(aRep: TReply);
    function GetRepMessage: TReply;
    procedure SetTableID(aTableID: IDataModulTableIdentifier);
    function GetTableID: IDataModulTableIdentifier;
    procedure SetTimeStamp(aTimeStamp: TDateTime);
    function GetTimeStamp: TDateTime;
    function ToSendString: String;
    procedure FromSendString(const aString: String);
    property GUID: String read GetGUID write SetGUID;
    property ReqMessage: TRequest read GetReqMessage write SetReqMessage;
    property RepMessage: TReply read GetRepMessage write SetRepMessage;
    property TableID: IDataModulTableIdentifier read GetTableID write SetTableID;
    property TimeStamp: TDateTime read GetTimeStamp write SetTimeStamp;
  end;

type
  ICommunicationDriver = interface(IInterface)
    ['{0907466A-777C-4EF5-9F29-FD00A66E99B1}']
    function InitializeMaster: Boolean; //Slave
    procedure DisconnectMaster;
    function MasterConnected: Boolean;
    procedure SetMessageReceivedEvent(aMessageReceivedEvent: TMessageReceivedEvent);
    function  GetMessageReceivedEvent: TMessageReceivedEvent;
    procedure SendMessage(const aMessage: String);
    property  OnMessageReceived: TMessageReceivedEvent read GetMessageReceivedEvent write SetMessageReceivedEvent;
  end;

  ICommunicationTIConn = interface(IInterface)
    ['{9D252E95-02D0-4C60-A2D4-67364F733E17}']
    procedure InitializeConnection;
    procedure CheckTableIdentifierOnline(aTableIdentifier: IDataModulTableIdentifier);
    procedure SendQRCodeToTI(aTableIdentifier: IDataModulTableIdentifier);
    procedure DeleteQRCodeOnTI(aTableIdentifier: IDataModulTableIdentifier);
    { Event if Connection changed }
    procedure SetTIConnectionStateChangedEvent(aConnStateChanged: TTIConnectionStateChanged);
    function  GetTIConnectionStateChangedEvent: TTIConnectionStateChanged;
    property OnConnectionStateChanged: TTIConnectionStateChanged read GetTIConnectionStateChangedEvent write SetTIConnectionStateChangedEvent;
  end;

  ICommunicationCore = interface(IInterface)
    ['{48FE1681-B0A8-4B9A-AE61-96C85EFB4956}']
    function Initialize: Boolean;
    procedure DeliverAllTIs;
    procedure SetAllActiveTI;
  end;

const
  START_CHAR = '#';
  END_CHAR = '*';
  SEPARATOR = '|';

implementation

end.
