unit IMO.Communication.Serial.Arduino;

interface

uses
  CPort, System.SysUtils, Winapi.Windows, System.IniFiles, Classes,
  Spring.Container, Spring.Services,
  { Own Libs}
  IMO.Communication.Interfaces, IMO.DataModul.Interfaces, IMO.Settings.Interfaces,
  IMO.Communication.Errors;

type
  TThreadFinished = procedure(const aMsg: String) of Object;

  TCommunicationSerialThread = class(TThread)
  private const
    MAX_CHAR_READ = 1000;
  private
    FComPort: TComport;
    FsMessage: String;
    FsException: String;
    FThreadFinished: TThreadFinished;
    procedure SyncFinished;
    procedure HandleException;
  protected
    procedure Execute; override;
  public
    constructor Create(var aComPort: TComPort; CreateSuspended: Boolean = True);
    property OnComMsgReceived: TThreadFinished read FThreadFinished write FThreadFinished;
  end;

  TCommunicationSerialArduino = class(TInterfacedObject, ICommunicationDriver)
  private
    ComPortMaster: TComPort;
    FEvMessageReceived: TMessageReceivedEvent;
    FSerialWatchThread: TCommunicationSerialThread;
    [Inject('SettingsGlobal')]
    FGlobalSettings: ISettingsGlobal;
    function ComPortSettSaved: Boolean;
    procedure OnThreadFinished(const aMsg: String);
  public
    constructor Create;
    destructor Destroy; override;
    { ICommunicationDriver }
    function InitializeMaster: Boolean; //Slave
    procedure DisconnectMaster;
    function MasterConnected: Boolean;
    procedure SetMessageReceivedEvent(aMessageReceivedEvent: TMessageReceivedEvent);
    function  GetMessageReceivedEvent: TMessageReceivedEvent;
    procedure SendMessage(const aMessage: String);
    property  OnMessageReceived: TMessageReceivedEvent read GetMessageReceivedEvent write SetMessageReceivedEvent;
  end;

implementation

{ TCommunicationSerialThread }

constructor TCommunicationSerialThread.Create(var aComPort: TComPort; CreateSuspended: Boolean = True);
begin
  FComPort := aComPort;
  inherited Create(CreateSuspended);
end;

procedure TCommunicationSerialThread.SyncFinished;
begin
  if Assigned(FThreadFinished) then
    FThreadFinished(FsMessage);
end;

procedure TCommunicationSerialThread.HandleException;
begin
  raise ECommunicationError.Create('Error in Comm-Read-Thread (Thread terminated), Message: ' + FsException);
end;

procedure TCommunicationSerialThread.Execute;
begin
  try
    while not Terminated do
    begin
      FComPort.ReadStr(FsMessage, MAX_CHAR_READ);
      if FsMessage <> '' then Synchronize(SyncFinished);
    end;
  except
    on E: Exception do
    begin
      FsException := E.Message;
      Synchronize(HandleException);
      Terminate;
    end;
  end;
end;

{ TCommunicationSerialArduino }

constructor TCommunicationSerialArduino.Create;
begin
  inherited Create;
  ComPortMaster := TComPort.Create(nil);
  ComPortMaster.Name := 'ComPortMaster'; // needed to save settings in ini

  FSerialWatchThread := TCommunicationSerialThread.Create(ComPortMaster, True);
  FSerialWatchThread.OnComMsgReceived := OnThreadFinished;
end;

destructor TCommunicationSerialArduino.Destroy;
begin
  if not FSerialWatchThread.Terminated then
  begin
    FSerialWatchThread.Terminate;
    FSerialWatchThread.Free;
  end;
  FreeAndNil(ComPortMaster);
  inherited Destroy;
end;

function TCommunicationSerialArduino.ComPortSettSaved: Boolean;
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(FGlobalSettings.SettingsFileName);
  try
    Result := IniFile.SectionExists(ComPortMaster.Name);
  finally
    IniFile.Free;
  end;
end;

procedure TCommunicationSerialArduino.OnThreadFinished(const aMsg: string);
begin
  if Assigned(FEvMessageReceived) then
  begin
    FEvMessageReceived(StringReplace(aMsg, sLineBreak, '', [rfReplaceAll]));
  end;
end;

function TCommunicationSerialArduino.GetMessageReceivedEvent: TMessageReceivedEvent;
begin
  Result := FEvMessageReceived;
end;

function TCommunicationSerialArduino.InitializeMaster: Boolean;
begin
  if ComPortSettSaved then
    ComPortMaster.LoadSettings(stIniFile, FGlobalSettings.SettingsFileName)
  else begin
    ComPortMaster.ShowSetupDialog;
    ComPortMaster.StoreSettings(stIniFile, FGlobalSettings.SettingsFileName);
  end;
  ComPortMaster.Open;
  Result := ComPortMaster.Connected;
  if Result then
    FSerialWatchThread.Start;
end;

procedure TCommunicationSerialArduino.DisconnectMaster;
begin
  ComPortMaster.Close;
end;

function TCommunicationSerialArduino.MasterConnected: Boolean;
begin
  Result := ComPortMaster.Connected;
end;

procedure TCommunicationSerialArduino.SendMessage(const aMessage: String);
var
  arByte: Array[0..21] of Byte;
begin
  //ComPortMaster.WriteStr(aMessage);
  //arByte[0] := $7e;
  arByte[0] := $7E;
  arByte[1] :=$00;
  arByte[2] := $12;
  arByte[3] := $10;
  arByte[4] := $01;
  arByte[5] :=$00; arByte[6] :=$13; arByte[7] :=$A2; arByte[8] :=$00;
  arByte[9] :=$40; arByte[10] :=$B0; arByte[11] :=$99; arByte[12] :=$37; arByte[13] :=$FF; arByte[14] :=$FE; arByte[15] :=$00;
  arByte[16] :=$00;
  arByte[17] :=$54; arByte[18] :=$65;
  arByte[19] :=$73; arByte[20] :=$74;
  arByte[21] :=$DC;
  ComPortMaster.Write(arByte, 22);
end;

procedure TCommunicationSerialArduino.SetMessageReceivedEvent(
  aMessageReceivedEvent: TMessageReceivedEvent);
begin
  FEvMessageReceived := aMessageReceivedEvent;
end;

initialization
  GlobalContainer.RegisterType<TCommunicationSerialArduino>.Implements<ICommunicationDriver>('CommunicationArduinoSerial').AsSingleton;

end.
