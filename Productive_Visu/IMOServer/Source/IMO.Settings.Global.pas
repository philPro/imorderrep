unit IMO.Settings.Global;

interface

uses
  Spring.Container, System.IniFiles, System.SysUtils,
  { Own libs }
  IMO.Settings.Interfaces;

type
  TSettingsGlobal = class(TInterfacedObject, ISettingsGlobal)
  private const
    _SEC_GLOB = 'GLOBAL';
  private
    FSimulationUsed: Boolean;
    FSettingsFileName: String;
  public
    constructor Create;
    destructor Destroy; override;
    function IsSimulationUsed: Boolean;
    procedure SetSimulationUsed(aValue: Boolean);
    function GetGlobSettingsFileName: String;
    procedure LoadSettings;
    procedure StoreSettings;
    property SimUsed: Boolean read IsSimulationUsed write SetSimulationUsed;
    property SettingsFileName: String read GetGlobSettingsFileName;
  end;

implementation

{ TSettingsGlobal }

constructor TSettingsGlobal.Create;
begin
  inherited Create;
  FSettingsFileName := ChangeFileExt(ParamStr(0), '.ini');
  LoadSettings;
end;

destructor TSettingsGlobal.Destroy;
begin
  StoreSettings;
  inherited Destroy;;
end;

function TSettingsGlobal.GetGlobSettingsFileName: String;
begin
  Result := FSettingsFileName;
end;

function TSettingsGlobal.IsSimulationUsed: Boolean;
begin
  Result := FSimulationUsed;
end;

procedure TSettingsGlobal.LoadSettings;
var
  GlobIni: TIniFile;
begin
  GlobIni := TIniFile.Create(GetGlobSettingsFileName);
  try
    SimUsed := GlobIni.ReadBool(_SEC_GLOB, 'SimulationUsed', False);
  finally
    GlobIni.Free;
  end;
end;

procedure TSettingsGlobal.SetSimulationUsed(aValue: Boolean);
begin
  FSimulationUsed := aValue;
end;

procedure TSettingsGlobal.StoreSettings;
var
  GlobIni: TIniFile;
begin
  GlobIni := TIniFile.Create(GetGlobSettingsFileName);
  try
    GlobIni.WriteBool(_SEC_GLOB, 'SimulationUsed', SimUsed);
  finally
    GlobIni.Free;
  end;
end;

initialization
  GlobalContainer.RegisterType<TSettingsGlobal>.Implements<ISettingsGlobal>('SettingsGlobal').AsSingleton;

end.
