unit uMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Spring.Services, IMO.Communication.Interfaces, Vcl.StdCtrls;

type
  TMainForm = class(TForm)
    ButtonInit: TButton;
    ButtonDeliver: TButton;
    procedure ButtonInitClick(Sender: TObject);
    procedure ButtonDeliverClick(Sender: TObject);
  private
    { Private-Deklarationen }
    TIConn: ICommunicationCore;
  public
    { Public-Deklarationen }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.ButtonDeliverClick(Sender: TObject);
begin
  TIConn.DeliverAllTIs;
end;

procedure TMainForm.ButtonInitClick(Sender: TObject);
begin
  TIConn := ServiceLocator.GetService<ICommunicationCore>('CommunicationCore');
  TIConn.Initialize;
end;

end.
