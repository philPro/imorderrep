unit IMO.DataModul.Interfaces;

interface

uses
  ZConnection;

{ CORE ####################################################################### }
type
  PZConnection = ^TZConnection;
  IDataModulCore = interface(IInterface)
    ['{4B63CBD4-D12A-4126-B370-1F0065BF5C9E}']
    function ConnectToDB: Boolean;
    procedure DisconnectDB;
    function Connected: Boolean;
    function GetSession: PZConnection;
  end;

{ TABLE-IDENTIFIER ########################################################### }
type
  IDataModulQRCode = interface(IInterface)
    ['{EC0EE1A4-7BB0-4D65-A40A-214B6C050E06}']
    function GetID: Integer;
    function GetQRText: String;
    function GetPIN: String;
    procedure SetID(aID: Integer);
    procedure SetQRText(aText: String);
    procedure SetPIN(aPin: String);
    property ID: Integer read GetID write SetID;
    property QRText: String read GetQRText write SetQRText;
    property PIN: String read GetPIN write SetPIN;
  end;

  IDataModulTableIdentifier = interface(IInterface)
    ['{1B7C60A8-A94E-4137-BC6B-9F1D021CAF8E}']
    function GetID: Integer;
    function GetName: String;
    function GetRID: Integer;
    function GetTableNumber: String;
    function GetQRCode: IDataModulQRCode;
    function GetXBeeHexID: String;
    function GetConnected: Boolean;
    function GetActive: Boolean;
    procedure SetID(aID: Integer);
    procedure SetName(aName: String);
    procedure SetRID(aRID: Integer);
    procedure SetTableNumber(aTableNumber: String);
    procedure SetQRCode(aQRCode: IDataModulQRCode);
    procedure SetXBeeHexID(aXBeeID: String);
    procedure SetConnected(aConnected: Boolean);
    procedure SetActive(aActive: Boolean);
    property ID: Integer read GetID write SetID;
    property Name: String read GetName write SetName;
    property RID: Integer read GetRID write SetRID;
    property TableNumber: String read GetTableNumber write SetTableNumber;
    property QRCode: IDataModulQRCode read GetQRCode write SetQRCode;
    property XBeeHexID: String read GetXBeeHexID write SetXBeeHexID;
    property Connected: Boolean read GetConnected write SetConnected;
    property Active: Boolean read GetActive write SetActive;
  end;

  IDataModulTableIdentifiers = interface(IInterface)
    ['{38B01D9F-42E8-4146-ADE2-A30902EEFE54}']
    function LoadData: Boolean;
    function SaveData(aTableIdentifier: IDataModulTableIdentifier): Boolean;
    function GetItem(aIndex: Integer): IDataModulTableIdentifier;
    function GetCount: Integer;
    procedure SetDataModulCore(aDataModulCore: IDataModulCore);
    property Count: Integer read GetCount;
    property DataModulCore: IDataModulCore write SetDataModulCore;
  end;

{ MEETING #################################################################### }
type
  IDataModulMeeting = interface(IInterface)
    ['{CC783CA3-CF6F-4E7B-8DE0-6CB9464000F4}']
    function GetID: Integer;
    function GetTableIdentifier: IDataModulTableIdentifier;
    function GetBeginDate: TDateTime;
    function GetEndDate: TDateTime;
    function GetParticipants: Integer;
    function GetBillSum: Double;
    function GetActiveQRCode: IDataModulQRCode;
    function GetActive: Boolean;
    procedure SetID(aID: Integer);
    procedure SetTableIdentifier(aTableIdentifier: IDataModulTableIdentifier);
    procedure SetBeginDate(aBeginDate: TDateTime);
    procedure SetEndDate(aEndDate: TDateTime);
    procedure SetParticipants(aParticipants: Integer);
    procedure SetBillSum(aBillSum: Double);
    procedure SetActiveQRCode(aQRCode: IDataModulQRCode);
    procedure SetActive(aActive: Boolean);
    property ID: Integer read GetID write SetID;
    property TableIdentifier: IDataModulTableIdentifier read GetTableIdentifier write SetTableIdentifier;
    property BeginDate: TDateTime read GetBeginDate write SetBeginDate;
    property EndDate: TDateTime read GetEndDate write SetEndDate;
    property Participants: Integer read GetParticipants write SetParticipants;
    property BillSum: Double read GetBillSum write SetBillSum;
    property ActiveQRCode: IDataModulQRCode read GetActiveQRCode write SetActiveQRCode;
    property IsActive: Boolean read GetActive write SetActive;
  end;

type
  IDataModulMeetings = interface(IInterface)
    ['{CB5FC67E-D6F2-4B3A-976C-80E790E0A30D}']
    function LoadData: Boolean;
    function GetActiveItem(aIndex: Integer): IDataModulMeeting;
    function GetHistoryItem(aIndex: Integer): IDataModulMeeting;
    function GetActiveCount: Integer;
    function GetHistoryCount: Integer;
    procedure SetDataModulCore(aDataModulCore: IDataModulCore);
    property ActiveCount: Integer read GetActiveCount;
    property HistoryCount: Integer read GetHistoryCount;
    property DataModulCore: IDataModulCore write SetDataModulCore;
  end;

const
    dbERR_NOT_CONNECTED = 1;
    { Tables }
    TABLE_ARTICLE_CATEGORY = 't_article_category';
    TABLE_TABLE_IDENTIFIERS = 't_table_identifier';
    TABLE_ARTICLES = 't_articles';
    TABLE_USERS = 't_users';
    TABLE_QR_CODE = 't_qr_code';
    TABLE_MEETING = 't_meeting';

implementation

end.
