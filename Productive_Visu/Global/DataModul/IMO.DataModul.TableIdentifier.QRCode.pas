unit IMO.DataModul.TableIdentifier.QRCode;

interface

uses
  Spring.Container,
  IMO.DataModul.Interfaces;

type
  TQRCode = class(TInterfacedObject, IDataModulQRCode)
  private
    FID: Integer;
    FQRText: String;
    FPIN: String;
  public
    function GetID: Integer;
    function GetQRText: String;
    function GetPIN: String;
    procedure SetID(aID: Integer);
    procedure SetQRText(aText: String);
    procedure SetPIN(aPin: String);
    property ID: Integer read GetID write SetID;
    property QRText: String read GetQRText write SetQRText;
    property PIN: String read GetPIN write SetPIN;
  end;

implementation

{ TQRCode }

function TQRCode.GetID: Integer;
begin
  Result := FID;
end;

function TQRCode.GetPIN: String;
begin
  Result := FPIN;
end;

function TQRCode.GetQRText: String;
begin
  Result := FQRText;
end;

procedure TQRCode.SetID(aID: Integer);
begin
  FID := aID;
end;

procedure TQRCode.SetPIN(aPin: String);
begin
  FPIN := aPin;
end;

procedure TQRCode.SetQRText(aText: String);
begin
  FQRText := aText;
end;

initialization
  GlobalContainer.RegisterType<TQRCode>.Implements<IDataModulQRCode>('DataModulQRCode');

end.
