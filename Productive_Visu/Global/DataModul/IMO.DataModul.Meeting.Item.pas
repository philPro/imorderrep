unit IMO.DataModul.Meeting.Item;

interface

uses
  Spring.Container, Spring.Services,
  IMO.DataModul.Interfaces;

type
  TDataModulMeeting = class(TInterfacedObject, IDataModulMeeting)
  private
    FID: Integer;
    [Inject('DataModulTableIdentifier')]
    FTableIdentifier: IDataModulTableIdentifier;
    FBeginDate: TDateTime;
    FEndDate: TDateTime;
    FParticipants: Integer;
    FBillSum: Double;
    [Inject('DataModulQRCode')]
    FActiveQRCode: IDataModulQRCode;
    FActive: Boolean;
  public
    function GetID: Integer;
    function GetTableIdentifier: IDataModulTableIdentifier;
    function GetBeginDate: TDateTime;
    function GetEndDate: TDateTime;
    function GetParticipants: Integer;
    function GetBillSum: Double;
    function GetActiveQRCode: IDataModulQRCode;
    function GetActive: Boolean;
    procedure SetID(aID: Integer);
    procedure SetTableIdentifier(aTableIdentifier: IDataModulTableIdentifier);
    procedure SetBeginDate(aBeginDate: TDateTime);
    procedure SetEndDate(aEndDate: TDateTime);
    procedure SetParticipants(aParticipants: Integer);
    procedure SetBillSum(aBillSum: Double);
    procedure SetActiveQRCode(aQRCode: IDataModulQRCode);
    procedure SetActive(aActive: Boolean);
    property ID: Integer read GetID write SetID;
    property TableIdentifier: IDataModulTableIdentifier read GetTableIdentifier write SetTableIdentifier;
    property BeginDate: TDateTime read GetBeginDate write SetBeginDate;
    property EndDate: TDateTime read GetEndDate write SetEndDate;
    property Participants: Integer read GetParticipants write SetParticipants;
    property BillSum: Double read GetBillSum write SetBillSum;
    property ActiveQRCode: IDataModulQRCode read GetActiveQRCode write SetActiveQRCode;
    property IsActive: Boolean read GetActive write SetActive;
  end;

implementation

{ TDataModulMeeting }

function TDataModulMeeting.GetActive: Boolean;
begin
  Result := FActive;
end;

function TDataModulMeeting.GetActiveQRCode: IDataModulQRCode;
begin
  Result := FActiveQRCode;
end;

function TDataModulMeeting.GetBeginDate: TDateTime;
begin
  Result := FBeginDate;
end;

function TDataModulMeeting.GetBillSum: Double;
begin
  Result := FBillSum;
end;

function TDataModulMeeting.GetEndDate: TDateTime;
begin
  Result := FEndDate;
end;

function TDataModulMeeting.GetID: Integer;
begin
  Result := FID;
end;

function TDataModulMeeting.GetParticipants: Integer;
begin
  Result := FParticipants;
end;

function TDataModulMeeting.GetTableIdentifier: IDataModulTableIdentifier;
begin
  Result := FTableIdentifier;
end;

procedure TDataModulMeeting.SetActive(aActive: Boolean);
begin

end;

procedure TDataModulMeeting.SetActiveQRCode(aQRCode: IDataModulQRCode);
begin
  FActiveQRCode := aQRCode;
end;

procedure TDataModulMeeting.SetBeginDate(aBeginDate: TDateTime);
begin
  FBeginDate := aBeginDate;
end;

procedure TDataModulMeeting.SetBillSum(aBillSum: Double);
begin
  FBillSum := aBillSum;
end;

procedure TDataModulMeeting.SetEndDate(aEndDate: TDateTime);
begin
  FEndDate := aEndDate;
end;

procedure TDataModulMeeting.SetID(aID: Integer);
begin
  FID := aID;
end;

procedure TDataModulMeeting.SetParticipants(aParticipants: Integer);
begin
  FParticipants := aParticipants;
end;

procedure TDataModulMeeting.SetTableIdentifier(
  aTableIdentifier: IDataModulTableIdentifier);
begin
  FTableIdentifier := aTableIdentifier;
end;

initialization
  GlobalContainer.RegisterType<TDataModulMeeting>.Implements<IDataModulMeeting>('DataModulMeeting');

end.
