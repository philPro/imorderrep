unit DataModul.TableIdentifier.Item;

interface

uses
  DataModul.TableIdentifier.QRCode, XSuperObject;

type
  TTableIdentifier = class
  private
    FID: Integer;
    FName: String;
    FRID: Integer;
    FTableNumber: String;
    FQRCode: TQRCode;
    FXBeeHexID: String;
    { For Server only }
    FConnected: Boolean;
  public
    FWhatToDoStatus: Integer;
    FGUID: String;
    function GetID: Integer;
    function GetName: String;
    function GetRID: Integer;
    function GetTableNumber: String;
    function GetQRCode: TQRCode;
    function GetXBeeHexID: String;
    function GetConnected: Boolean;
    procedure SetID(aID: Integer);
    procedure SetName(aName: String);
    procedure SetRID(aRID: Integer);
    procedure SetTableNumber(aTableNumber: String);
    procedure SetQRCode(aQRCode: TQRCode);
    procedure SetXBeeHexID(aXBeeID: String);
    procedure SetConnected(aConnected: Boolean);
    property ID: Integer read GetID write SetID;
    property Name: String read GetName write SetName;
    property RID: Integer read GetRID write SetRID;
    property TableNumber: String read GetTableNumber write SetTableNumber;
    property QRCode: TQRCode read GetQRCode write SetQRCode;
    property XBeeHexID: String read GetXBeeHexID write SetXBeeHexID;
    property Connected: Boolean read GetConnected write SetConnected;
  end;

implementation

{ TTableIdentifier }

function TTableIdentifier.GetConnected: Boolean;
begin
  Result := FConnected;
end;

function TTableIdentifier.GetID: Integer;
begin
  Result := FID;
end;

function TTableIdentifier.GetName: String;
begin
  Result := FName;
end;

function TTableIdentifier.GetQRCode: TQRCode;
begin
  Result := FQRCode;
end;

function TTableIdentifier.GetRID: Integer;
begin
  Result := FRID;
end;

function TTableIdentifier.GetTableNumber: String;
begin
  Result := FTableNumber;
end;

function TTableIdentifier.GetXBeeHexID: String;
begin
  Result := FXBeeHexID;
end;

procedure TTableIdentifier.SetConnected(aConnected: Boolean);
begin
  FConnected := aConnected;
end;

procedure TTableIdentifier.SetID(aID: Integer);
begin
  FID := aID;
end;

procedure TTableIdentifier.SetName(aName: String);
begin
  FName := aName;
end;

procedure TTableIdentifier.SetQRCode(aQRCode: TQRCode);
begin
  FQRCode := aQRCode;
end;

procedure TTableIdentifier.SetRID(aRID: Integer);
begin
  FRID := aRID;
end;

procedure TTableIdentifier.SetTableNumber(aTableNumber: String);
begin
  FTableNumber := aTableNumber;
end;

procedure TTableIdentifier.SetXBeeHexID(aXBeeID: String);
begin
  FXBeeHexID := aXBeeID;
end;

end.
