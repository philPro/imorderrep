object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'MainForm'
  ClientHeight = 745
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelReceiveLog: TPanel
    Left = 0
    Top = 495
    Width = 634
    Height = 250
    Align = alBottom
    TabOrder = 0
    object LabelReceiveLog: TLabel
      Left = 8
      Top = 14
      Width = 124
      Height = 13
      Caption = 'Empfangene Nachrichten:'
    end
    object ListBoxReceiveLog: TListBox
      Left = 1
      Top = 40
      Width = 632
      Height = 209
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
    end
    object ButtonSaveReceiveLog: TButton
      Left = 152
      Top = 9
      Width = 105
      Height = 25
      Caption = 'Log speichern ...'
      TabOrder = 1
      OnClick = ButtonSaveReceiveLogClick
    end
  end
  object PanelSendLog: TPanel
    Left = 0
    Top = 245
    Width = 634
    Height = 250
    Align = alBottom
    TabOrder = 1
    object LabelSendLog: TLabel
      Left = 8
      Top = 14
      Width = 116
      Height = 13
      Caption = 'Gesendete Nachrichten:'
    end
    object ListBoxSendLog: TListBox
      Left = 1
      Top = 40
      Width = 632
      Height = 209
      Align = alBottom
      Anchors = [akLeft, akTop, akRight, akBottom]
      ItemHeight = 13
      TabOrder = 0
    end
    object ButtonSaveLog: TButton
      Left = 152
      Top = 9
      Width = 105
      Height = 25
      Caption = 'Log speichern ...'
      TabOrder = 1
      OnClick = ButtonSaveLogClick
    end
  end
  object PanelMessage: TPanel
    Left = 0
    Top = 41
    Width = 634
    Height = 89
    Align = alTop
    TabOrder = 2
    object LabelGUID: TLabel
      Left = 8
      Top = 11
      Width = 29
      Height = 13
      Caption = 'GUID:'
    end
    object LabelRequest: TLabel
      Left = 8
      Top = 38
      Width = 44
      Height = 13
      Caption = 'Request:'
    end
    object LabelXBeeHexID: TLabel
      Left = 312
      Top = 11
      Width = 58
      Height = 13
      Caption = 'XBeeHexID:'
    end
    object LabelQRCode: TLabel
      Left = 312
      Top = 38
      Width = 48
      Height = 13
      Caption = 'QR-Code:'
    end
    object LabelQRPin: TLabel
      Left = 312
      Top = 65
      Width = 40
      Height = 13
      Caption = 'QR-PIN:'
    end
    object EditGUID: TEdit
      Left = 96
      Top = 8
      Width = 161
      Height = 21
      TabOrder = 0
    end
    object ComboBoxRequest: TComboBox
      Left = 96
      Top = 35
      Width = 161
      Height = 21
      Style = csDropDownList
      ItemIndex = 3
      TabOrder = 1
      Text = 'NOT SET'
      Items.Strings = (
        'SEND QR'
        'DELETE QR'
        'CLIENT ONLINE'
        'NOT SET')
    end
    object EditXBeeHex: TEdit
      Left = 408
      Top = 8
      Width = 161
      Height = 21
      TabOrder = 2
    end
    object EditQRCode: TEdit
      Left = 408
      Top = 35
      Width = 161
      Height = 21
      TabOrder = 3
    end
    object EditQRPin: TEdit
      Left = 408
      Top = 62
      Width = 161
      Height = 21
      TabOrder = 4
    end
    object ButtonCreateGUID: TButton
      Left = 49
      Top = 6
      Width = 41
      Height = 25
      Caption = 'New'
      TabOrder = 5
      OnClick = ButtonCreateGUIDClick
    end
  end
  object PanelList: TPanel
    Left = 0
    Top = 130
    Width = 634
    Height = 115
    Align = alClient
    TabOrder = 3
    object ButtonAddMessage: TButton
      Left = 8
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Hinzuf'#252'gen'
      TabOrder = 0
      OnClick = ButtonAddMessageClick
    end
    object ButtonDeleteMessage: TButton
      Left = 112
      Top = 6
      Width = 75
      Height = 25
      Caption = 'L'#246'schen'
      TabOrder = 1
    end
    object ListViewMessages: TListView
      Left = 1
      Top = 37
      Width = 632
      Height = 77
      Align = alBottom
      Columns = <
        item
          Caption = 'GUID'
        end
        item
          Caption = 'Request'
          Width = 70
        end
        item
          AutoSize = True
          Caption = 'XBeeHexID'
        end
        item
          Caption = 'QRCode'
          Width = 90
        end
        item
          Caption = 'QRPin'
          Width = 90
        end>
      RowSelect = True
      TabOrder = 2
      ViewStyle = vsReport
    end
  end
  object PanelSettings: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 41
    Align = alTop
    TabOrder = 4
    object LabelStatus: TLabel
      Left = 208
      Top = 14
      Width = 126
      Height = 13
      Caption = 'Comport nicht verbunden!'
    end
    object LabelMSBetween: TLabel
      Left = 487
      Top = 14
      Width = 21
      Height = 13
      Caption = '[ms]'
    end
    object ButtonConnectPort: TButton
      Left = 15
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Verbinden'
      TabOrder = 0
      OnClick = ButtonConnectPortClick
    end
    object ButtonDisconnectPort: TButton
      Left = 112
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Trennen'
      Enabled = False
      TabOrder = 1
    end
    object EditMSBetween: TEdit
      Left = 360
      Top = 11
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '200'
    end
    object ButtonStart: TButton
      Left = 536
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Start'
      Enabled = False
      TabOrder = 3
      OnClick = ButtonStartClick
    end
  end
end
