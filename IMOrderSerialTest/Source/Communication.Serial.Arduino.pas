unit Communication.Serial.Arduino;

interface

uses
  CPort, System.SysUtils, Winapi.Windows, System.IniFiles, Classes,
  { Own Libs}
  Communication.Errors;

type
  TThreadFinished = procedure(const aMsg: String) of Object;

  TCommunicationSerialThread = class(TThread)
  private const
    MAX_CHAR_READ = 1000;
  private
    FComPort: TComport;
    FsMessage: String;
    FsException: String;
    FThreadFinished: TThreadFinished;
    procedure SyncFinished;
    procedure HandleException;
  protected
    procedure Execute; override;
  public
    constructor Create(var aComPort: TComPort; CreateSuspended: Boolean = True);
    property OnComMsgReceived: TThreadFinished read FThreadFinished write FThreadFinished;
  end;

type
  TMessageReceivedEvent = procedure(const aMsg: String) of Object;

  TCommunicationSerialArduino = class
  private
    ComPortMaster: TComPort;
    FEvMessageReceived: TMessageReceivedEvent;
    FSerialWatchThread: TCommunicationSerialThread;
    function GetIniFileName: String;
    function ComPortSettSaved: Boolean;
    procedure OnThreadFinished(const aMsg: String);
  public
    constructor Create;
    destructor Destroy; override;
    { ICommunicationSerial }
    function InitializeMaster: Boolean; //Slave
    function ChangeMasterPort: Boolean;
    function MasterConnected: Boolean;
    procedure OnConnectionChanged(aComPort: Word);
    { ICommunicationTransfer }
    procedure SetMessageReceivedEvent(aMessageReceivedEvent: TMessageReceivedEvent);
    function  GetMessageReceivedEvent: TMessageReceivedEvent;
    procedure SendMessage(const aMessage: String);
    property  OnMessageReceived: TMessageReceivedEvent read GetMessageReceivedEvent write SetMessageReceivedEvent;
  end;

implementation

{ TCommunicationSerialThread }

constructor TCommunicationSerialThread.Create(var aComPort: TComPort; CreateSuspended: Boolean = True);
begin
  FComPort := aComPort;
  inherited Create(CreateSuspended);
end;

procedure TCommunicationSerialThread.SyncFinished;
begin
  if Assigned(FThreadFinished) then
    FThreadFinished(FsMessage);
end;

procedure TCommunicationSerialThread.HandleException;
begin
  raise ECommunicationError.Create('Error in Comm-Read-Thread (Thread terminated), Message: ' + FsException);
end;

procedure TCommunicationSerialThread.Execute;
begin
  try
    while not Terminated do
    begin
      FComPort.ReadStr(FsMessage, MAX_CHAR_READ);
      if FsMessage <> '' then Synchronize(SyncFinished);
    end;
  except
    on E: Exception do
    begin
      FsException := E.Message;
      Synchronize(HandleException);
      Terminate;
    end;
  end;
end;

{ TCommunicationSerialArduino }

constructor TCommunicationSerialArduino.Create;
begin
  inherited Create;
  ComPortMaster := TComPort.Create(nil);
  ComPortMaster.Name := 'ComPortMaster'; // needed to save settings in ini

  FSerialWatchThread := TCommunicationSerialThread.Create(ComPortMaster, True);
  FSerialWatchThread.OnComMsgReceived := OnThreadFinished;
end;

destructor TCommunicationSerialArduino.Destroy;
begin
  if not FSerialWatchThread.Terminated then
  begin
    FSerialWatchThread.Terminate;
    FSerialWatchThread.Free;
  end;
  FreeAndNil(ComPortMaster);
  inherited Destroy;
end;

function TCommunicationSerialArduino.GetIniFileName: String;
begin
  Result := ChangeFileExt(ParamStr(0), '.ini');
end;

function TCommunicationSerialArduino.ComPortSettSaved: Boolean;
var
  IniFile: TIniFile;
begin
  IniFile := TIniFile.Create(GetIniFileName);
  try
    Result := IniFile.SectionExists(ComPortMaster.Name);
  finally
    IniFile.Free;
  end;
end;

procedure TCommunicationSerialArduino.OnThreadFinished(const aMsg: string);
begin
  if Assigned(FEvMessageReceived) then
  begin
    FEvMessageReceived(StringReplace(aMsg, sLineBreak, '', [rfReplaceAll]));
  end;
end;

function TCommunicationSerialArduino.GetMessageReceivedEvent: TMessageReceivedEvent;
begin
  Result := FEvMessageReceived;
end;


function TCommunicationSerialArduino.ChangeMasterPort: Boolean;
begin
  ComPortMaster.ShowSetupDialog;
  ComPortMaster.StoreSettings(stIniFile, GetIniFileName);
  Result := InitializeMaster;
end;


function TCommunicationSerialArduino.InitializeMaster: Boolean;
begin
  if ComPortSettSaved then
    ComPortMaster.LoadSettings(stIniFile, GetIniFileName)
  else begin
    ComPortMaster.ShowSetupDialog;
    ComPortMaster.StoreSettings(stIniFile, GetIniFileName);
  end;
  ComPortMaster.Open;
  Result := ComPortMaster.Connected;
  if Result then
    FSerialWatchThread.Start;
end;

function TCommunicationSerialArduino.MasterConnected: Boolean;
begin
  Result := ComPortMaster.Connected;
end;

procedure TCommunicationSerialArduino.OnConnectionChanged(aComPort: Word);
begin
  // TODO
end;

procedure TCommunicationSerialArduino.SendMessage(const aMessage: String);
begin
  ComPortMaster.WriteStr(aMessage);
end;

procedure TCommunicationSerialArduino.SetMessageReceivedEvent(
  aMessageReceivedEvent: TMessageReceivedEvent);
begin
  FEvMessageReceived := aMessageReceivedEvent;
end;

end.
