unit Communication.TableIdentifier;

interface

uses
  Dialogs, Generics.Collections, System.SysUtils, System.Classes,
  Communication.Errors, DataModul.TableIdentifier, Communication.Serial.Arduino,
  DataModul.TableIdentifier.Item, Vcl.StdCtrls, XSuperObject;

  { ################### PROTOCOL ################### }
  { #                                              # }
  { #----------------------------------------------# }
  { #     REQUESTS         |         REPLIES       # }
  { #----------------------------------------------# }
  { #     SENDQR           |          TIMEOUT      # }
  { #    DELETEQR          |       CLIENTNOTFOUND  # }
  { #  CLIENTONLINE        |         CLIENTRDY     # }
  { #                      |      DATATRANSMITTED  # }
  { #                      |                       # }
  { ################################################ }

type
  TRequest = (reqSendQR, reqDeleteQR, reqClientOnline, reqNotSet);
  TReply = (repTimeout, repClientNotFound, repClientRdy, repDataTransmitted, repNotSet);

type
  TCommProt = class
    GUID: String;
    ReqMessage: TRequest;
    RepMessage: TReply;
    TableID: TTableIdentifier;
    TimeStamp: TDateTime;//timestamp of send
  end;
  TActiveMessages = class(TObjectList<TCommProt>);

const
  _reqSENDQR          = 'SENDQR';
  _reqDELETEQR        = 'DELETEQR';
  _reqCLIENTONLINE    = 'CLIENTONLINE';
  _repTIMEOUT         = 'TIMEOUT';
  _repCLIENTNOTFOUND  = 'CLIENTNOTFOUND';
  _repCLIENTRDY       = 'CLIENTRDY';
  _repDATATRANSMITTED = 'DATATRANSMITTED';
  START_CHAR = '#';
  END_CHAR = '*';
  SEPARATOR = '|';

type
  TTIConnection = class
  private
    FSerialMasterConn: TCommunicationSerialArduino;
    FsWholeReply: String;
    FbReplyStarted: Boolean;
    FActiveMessages: TActiveMessages;
    LoggerOutMsg: TStrings;
    LoggerInMsg: TStrings;
    procedure OnMsgReceived(const aMsg: String);
    procedure SendMessage(aTableID: TTableIdentifier; aReq: TRequest);
    procedure ProcessReply(aMsg: String);
    procedure DoOutLog(aMsg: String);
    procedure DoInLog(aMsg: String);
  public
    FShowAllRecData: Boolean;
    constructor Create;
    destructor Destroy; override;
    function InitializeConnection: Boolean;
    procedure CheckTableIdentifierOnline(aTableIdentifier: TTableIdentifier);
    procedure SendQRCodeToTI(aTableIdentifier: TTableIdentifier);
    procedure DeleteQRCodeOnTI(aTableIdentifier: TTableIdentifier);
    procedure SetLogger(aOutMsg: TStrings; aInMsg: TStrings);
  end;

implementation

uses
  System.StrUtils;

{ TTIConnection }

constructor TTIConnection.Create;
begin
  inherited Create;
  FsWholeReply := '';
  FbReplyStarted := False;
  FShowAllRecData := False;

  FSerialMasterConn := TCommunicationSerialArduino.Create;
  FActiveMessages := TActiveMessages.Create(True);
end;

destructor TTIConnection.Destroy;
begin
  if Assigned(FActiveMessages) then FActiveMessages.Free;
  if Assigned(FSerialMasterConn) then FSerialMasterConn.Free;
  inherited;
end;


function TTIConnection.InitializeConnection: Boolean;
begin
  { Initializations }
  FSerialMasterConn.OnMessageReceived := OnMsgReceived;
  FActiveMessages.Clear;

  Result := FSerialMasterConn.InitializeMaster;
  if not Result then
    raise ECommunicationError.Create('Failed to initialize ComPort! Please check if the Master is connected to the PC!');
end;

procedure TTIConnection.CheckTableIdentifierOnline(aTableIdentifier: TTableIdentifier);
begin
  SendMessage(aTableIdentifier, reqClientOnline);
end;

procedure TTIConnection.SendQRCodeToTI(aTableIdentifier: TTableIdentifier);
begin
  SendMessage(aTableIdentifier, reqSendQR);
end;

procedure TTIConnection.DeleteQRCodeOnTI(aTableIdentifier: TTableIdentifier);
begin
  SendMessage(aTableIdentifier, reqDeleteQR);
end;

procedure TTIConnection.SetLogger(aOutMsg: TStrings; aInMsg: TStrings);
begin
  LoggerOutMsg := aOutMsg;
  LoggerInMsg := aInMsg;
end;

procedure TTIConnection.OnMsgReceived(const aMsg: String);
var
  iStartPos: Integer;
  iStopPos: Integer;
begin
  iStartPos := Pos(START_CHAR, aMsg);
  iStopPos := Pos(END_CHAR, aMsg);

  if (iStartPos > 0) AND (iStopPos > 0) then
  begin
    { Die gesamte Message befindet sich im aktuellen String }
    FsWholeReply := Copy(aMsg, iStartPos + 1, iStopPos - iStartPos - 1);
    // ########################## DO SMTH with the reply
    ProcessReply(FsWholeReply);
    FsWholeReply := '';
    FbReplyStarted := False;
  end else
  if iStartPos > 0 then
  begin
    { Nur Beginn der Message vorhanden }
    FsWholeReply := Copy(aMsg, iStartPos + 1, Length(aMsg) - iStartPos);
    FbReplyStarted := True;
  end else
  if FbReplyStarted then
  begin
    { wir befinden uns nun mitten in der Message }
    if iStopPos > 0 then
    begin
      { Ende der message gefunden }
      FsWholeReply := FsWholeReply + Copy(aMsg, 1, iStopPos - 1);
      // ########################## DO SMTH with the reply
      ProcessReply(FsWholeReply);
      FsWholeReply := '';
      FbReplyStarted := False;
    end else
    begin
      { Ende noch nicht gefunden }
      FsWholeReply := FsWholeReply + aMsg;
    end;
  end;

  if FShowAllRecData then DoInLog(aMsg);
end;

procedure TTIConnection.SendMessage(aTableID: TTableIdentifier; aReq: TRequest);
var
  sMsg: String;
begin
  FActiveMessages.Add(TCommProt.Create);
  FActiveMessages.Items[FActiveMessages.Count-1].GUID := aTableID.FGUID;
  FActiveMessages.Items[FActiveMessages.Count-1].ReqMessage := aReq;
  FActiveMessages.Items[FActiveMessages.Count-1].RepMessage := repNotSet;
  FActiveMessages.Items[FActiveMessages.Count-1].TableID := aTableID;
  FActiveMessages.Items[FActiveMessages.Count-1].TimeStamp := Now;

  sMsg := START_CHAR + FActiveMessages.Items[FActiveMessages.Count-1].GUID + SEPARATOR +          { GUID }
          IntToStr(Ord(FActiveMessages.Items[FActiveMessages.Count-1].ReqMessage)) + SEPARATOR +  { Request }
          IntToStr(Ord(FActiveMessages.Items[FActiveMessages.Count-1].RepMessage)) + SEPARATOR +  { Reply }
          FActiveMessages.Items[FActiveMessages.Count-1].TableID.XBeeHexID + SEPARATOR +          { XBeeHexID }
          FActiveMessages.Items[FActiveMessages.Count-1].TableID.QRCode.QRText + SEPARATOR +      { QRCode }
          FActiveMessages.Items[FActiveMessages.Count-1].TableID.QRCode.PIN + END_CHAR;           { QR-Pin }

  FSerialMasterConn.SendMessage(sMsg);
  DoOutLog(sMsg);
end;

procedure TTIConnection.ProcessReply(aMsg: string);
var
  sGUID: String;
  hlpReq: TRequest;
  hlpRep: TReply;
  sXBeeID: String;
  sQRCode: String;
  sQRPin: String;
  iStartPos: Integer;
  iStopPos: Integer;
  iListIdx: Integer;

  function GetListIdx(aGUID: String): Integer;
  var
    iTmp: Integer;
  begin
    Result := -1;

    for iTmp := 0 to FActiveMessages.Count-1 do
    begin
      if FActiveMessages.Items[iTmp].GUID = aGUID then
      begin
        Result := iTmp;
        Break;
      end;
    end;
  end;

begin
  { Message-Aufbau }
  { START_CHAR  GUID | REQUEST | REPLY | XBeeHexID | QRCode | QR-Pin  END_CHAR }

  { GUID }
  iStartPos := 2;
  iStopPos := Pos(SEPARATOR, aMsg);
  sGUID := Copy(aMsg, iStartPos, iStopPos - iStartPos);

  { Request }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aMsg, iStartPos);
  hlpReq := TRequest(StrToInt(Copy(aMsg, iStartPos, iStopPos - iStartPos)));

  { Reply }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aMsg, iStartPos);
  hlpRep := TReply(StrToInt(Copy(aMsg, iStartPos, iStopPos - iStartPos)));

  { XBeeHex }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aMsg, iStartPos);
  sXBeeID := Copy(aMsg, iStartPos, iStopPos - iStartPos);

  { QRCode }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aMsg, iStartPos);
  sQRCode := Copy(aMsg, iStartPos, iStopPos - iStartPos);

  { QRPin }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(END_CHAR, aMsg, iStartPos);
  sQRPin := Copy(aMsg, iStartPos, iStopPos - iStartPos);

  iListIdx := GetListIdx(sGUID);
  if iListIdx < 0 then
    raise ECommunicationError.Create('Reply GUID not found in Active-Messages!' + sLineBreak + 'GUID: ' + sGUID);

  { TI nicht verf�gbar }
  if hlprep in [repTimeout, repClientNotFound] then
  begin
    FActiveMessages.Items[iListIdx].TableID.Connected := False;
  end else
  { TI verf�gbar }
  if hlpRep in [repClientRdy] then
  begin
    FActiveMessages.Items[iListIdx].TableID.Connected := True;
  end else
  { TI Daten �bertragen }
  if hlpRep in [repDataTransmitted] then
  begin
    FActiveMessages.Items[iListIdx].TableID.Connected := True;
    // DB QR on Slave -> Set Flag
  end;

  DoInLog(aMsg);

  { Delete from Active messages }
  FActiveMessages.Delete(iListIdx);
end;

procedure TTIConnection.DoOutLog(aMsg: string);
begin
  if Assigned(LoggerOutMsg) then LoggerOutMsg.Add(aMsg)
  else raise ECommunicationError.Create('Logger-Out nicht instanziert!');
end;

procedure TTIConnection.DoInLog(aMsg: string);
begin
  if Assigned(LoggerInMsg) then LoggerInMsg.Add(aMsg)
  else raise ECommunicationError.Create('Logger-In nicht instanziert!');
end;

end.
