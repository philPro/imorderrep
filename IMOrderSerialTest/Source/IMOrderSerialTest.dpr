program IMOrderSerialTest;

uses
  Vcl.Forms,
  uMainForm in 'uMainForm.pas' {MainForm},
  Communication.Serial.Arduino in 'Communication.Serial.Arduino.pas',
  Communication.TableIdentifier in 'Communication.TableIdentifier.pas',
  DataModul.TableIdentifier.Item in 'DataModul.TableIdentifier.Item.pas',
  DataModul.TableIdentifier in 'DataModul.TableIdentifier.pas',
  DataModul.TableIdentifier.QRCode in 'DataModul.TableIdentifier.QRCode.pas',
  DataModul.Errors in 'DataModul.Errors.pas',
  Communication.Errors in 'Communication.Errors.pas',
  DataModul.Core in 'DataModul.Core.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
