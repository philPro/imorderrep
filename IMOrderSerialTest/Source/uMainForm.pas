unit uMainForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.Grids,
  Vcl.ComCtrls, Communication.TableIdentifier, DataModul.TableIdentifier,
  DataModul.TableIdentifier.Item, DataModul.TableIdentifier.QRCode, XSuperObject;

type
  TMainForm = class(TForm)
    PanelReceiveLog: TPanel;
    ListBoxReceiveLog: TListBox;
    LabelReceiveLog: TLabel;
    ButtonSaveReceiveLog: TButton;
    PanelSendLog: TPanel;
    LabelSendLog: TLabel;
    ListBoxSendLog: TListBox;
    ButtonSaveLog: TButton;
    PanelMessage: TPanel;
    EditGUID: TEdit;
    ComboBoxRequest: TComboBox;
    EditXBeeHex: TEdit;
    EditQRCode: TEdit;
    EditQRPin: TEdit;
    LabelGUID: TLabel;
    ButtonCreateGUID: TButton;
    LabelRequest: TLabel;
    LabelXBeeHexID: TLabel;
    LabelQRCode: TLabel;
    LabelQRPin: TLabel;
    PanelList: TPanel;
    ButtonAddMessage: TButton;
    ButtonDeleteMessage: TButton;
    PanelSettings: TPanel;
    ButtonConnectPort: TButton;
    ButtonDisconnectPort: TButton;
    ListViewMessages: TListView;
    LabelStatus: TLabel;
    EditMSBetween: TEdit;
    LabelMSBetween: TLabel;
    ButtonStart: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonConnectPortClick(Sender: TObject);
    procedure ButtonCreateGUIDClick(Sender: TObject);
    procedure ButtonAddMessageClick(Sender: TObject);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonSaveLogClick(Sender: TObject);
    procedure ButtonSaveReceiveLogClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FConnection: TTIConnection;
    FTIData: TTableIdentifiers;
    procedure ShowAllDataInList;
  public
    { Public-Deklarationen }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.ButtonAddMessageClick(Sender: TObject);
var
  aTI: TTableIdentifier;
begin
  aTI := TTableIdentifier.Create;
  aTI.ID := FTIData.GetCount;
  aTI.FGUID := EditGUID.Text;
  aTI.QRCode := TQRCode.Create;
  aTI.QRCode.ID := 0;
  aTI.QRCode.QRText := EditQRCode.Text;
  aTI.QRCode.PIN := EditQRPin.Text;
  aTI.XBeeHexID := EditXBeeHex.Text;

  aTI.FWhatToDoStatus := ComboBoxRequest.ItemIndex;

  FTIData.AddItem(aTI);

  ShowAllDataInList;
end;

procedure TMainForm.ButtonConnectPortClick(Sender: TObject);
begin
  if FConnection.InitializeConnection then
  begin
    ButtonConnectPort.Enabled := False;
    LabelStatus.Caption := 'Comport verbunden!';
    ButtonStart.Enabled := True;
  end;
end;

procedure TMainForm.ButtonCreateGUIDClick(Sender: TObject);
var
  aID: TGUID;
begin
  CreateGUID(aID);
  EditGUID.Text := GUIDToString(aID);
end;

procedure TMainForm.ButtonSaveLogClick(Sender: TObject);
begin
  ListBoxSendLog.Items.SaveToFile(ExtractFilePath(Application.ExeName) + 'SendLog.txt');
end;

procedure TMainForm.ButtonSaveReceiveLogClick(Sender: TObject);
begin
  ListBoxReceiveLog.Items.SaveToFile(ExtractFilePath(Application.ExeName) + 'ReceiveLog.txt');
end;

procedure TMainForm.ButtonStartClick(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to FTIData.GetCount-1 do
  begin
    case FTIData.GetItem(I).FWhatToDoStatus of
      0:
        begin
          FConnection.SendQRCodeToTI(FTIData.GetItem(I));
        end;
      1:
        begin
          FConnection.DeleteQRCodeOnTI(FTIData.GetItem(I));
        end;
      2:
        begin
          FConnection.CheckTableIdentifierOnline(FTIData.GetItem(I));
        end;
    end;
    Sleep(StrToInt(EditMSBetween.Text));
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  slSave: TStringList;
begin
  FConnection := TTIConnection.Create;
  FTIData := TTableIdentifiers.Create;

  if FileExists(ExtractFilePath(Application.ExeName) + 'SavedData.txt') then
  begin
//    FTIData.LoadDataFromFile(ExtractFilePath(Application.ExeName) + 'SavedData.txt');
  end;

  FConnection.SetLogger(ListBoxSendLog.Items, ListBoxReceiveLog.Items);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FTIData.SaveDataToFile(ExtractFilePath(Application.ExeName) + 'SavedData.txt');

  FreeAndNil(FConnection);
  FreeAndNil(FTIData);
end;

procedure TMainForm.ShowAllDataInList;
var
  I: Integer;
  item: TTableIdentifier;
begin
  ListViewMessages.Items.Clear;
  for I := 0 to FTIData.GetCount-1 do
  begin
    item := FTIData.GetItem(I);
    ListViewMessages.Items.Add;
    ListViewMessages.Items[ListViewMessages.Items.Count-1].Caption := item.FGUID;
    ListViewMessages.Items[ListViewMessages.Items.Count-1].SubItems.Add(IntToStr(item.FWhatToDoStatus));
    ListViewMessages.Items[ListViewMessages.Items.Count-1].SubItems.Add(item.XBeeHexID);
    ListViewMessages.Items[ListViewMessages.Items.Count-1].SubItems.Add(item.QRCode.QRText);
    ListViewMessages.Items[ListViewMessages.Items.Count-1].SubItems.Add(item.QRCode.PIN);
  end;
end;

end.
