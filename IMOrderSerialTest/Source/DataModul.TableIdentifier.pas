unit DataModul.TableIdentifier;

interface

uses
  System.Classes, System.SysUtils, Winapi.Windows, ZConnection,
  ZDataset, Vcl.Forms, System.UITypes, Data.DB, Generics.Collections,
  { Own Libs }
  DataModul.Errors, DataModul.Core, DataModul.TableIdentifier.Item,
  DataModul.TableIdentifier.QRCode, XSuperObject;

type
  TTableIdentifiers = class
  private
    FTableIdentiefierList: TObjectList<TTableIdentifier>;
    FDBConnection: TDataModulCore;
  public
    constructor Create;
    destructor Destroy; override;
    function LoadData: Boolean;
    function GetItem(aIndex: Integer): TTableIdentifier;
    function GetCount: Integer;
    procedure AddItem(aTableIdentifier: TTableIdentifier);
    procedure DeleteItem(aIndex: Integer);
    procedure SetDataModulCore(aDataModulCore: TDataModulCore);
    property Count: Integer read GetCount;
    property DataModulCore: TDataModulCore write SetDataModulCore;
    procedure SaveDataToFile(aFileName: String);
    procedure LoadDataFromFile(aFileName: String);
  end;

implementation

{ TTableIdentifiers }

constructor TTableIdentifiers.Create;
begin
  Inherited Create;
  FTableIdentiefierList := TObjectList<TTableIdentifier>.Create;
end;

destructor TTableIdentifiers.Destroy;
begin
  FreeAndNil(FTableIdentiefierList);
  inherited;
end;

function TTableIdentifiers.GetCount: Integer;
begin
  Result := FTableIdentiefierList.Count;
end;

procedure TTableIdentifiers.AddItem(aTableIdentifier: TTableIdentifier);
begin
  FTableIdentiefierList.Add(aTableIdentifier);
end;

procedure TTableIdentifiers.DeleteItem(aIndex: Integer);
begin
  if aIndex > FTableIdentiefierList.Count-1 then
    raise EArgumentOutOfRangeException.Create('Table-Identifier mit Index ' + IntToStr(aIndex) + ' existiert nicht!');

  FTableIdentiefierList.Delete(aIndex);
end;

procedure TTableIdentifiers.SaveDataToFile(aFileName: String);
var
  slSave: TStringList;
begin
  slSave := TStringList.Create;
  try
    slSave.Add(FTableIdentiefierList.AsJSON);
    slSave.SaveToFile(aFileName);
  finally
    slSave.Free;
  end;
end;

procedure TTableIdentifiers.SetDataModulCore(aDataModulCore: TDataModulCore);
begin
  FDBConnection := aDataModulCore;
end;

function TTableIdentifiers.GetItem(aIndex: Integer): TTableIdentifier;
begin
  Result := FTableIdentiefierList.Items[aIndex];
end;

function TTableIdentifiers.LoadData: Boolean;
var
  ListQuery: TZQuery;
  QRQuery: TZQuery;
  iQRID: Integer;
  TI: TTableIdentifier;
begin
  Result := False;
  if not FDBConnection.ConnectToDB then
    raise EDataModulError.Create('Database not connected!');

  FTableIdentiefierList.Clear;
  Screen.Cursor := crHourGlass;

  ListQuery := TZQuery.Create(nil);
  QRQuery := TZQuery.Create(nil);
  ListQuery.Connection := FDBConnection.GetSession;
  QRQuery.Connection := ListQuery.Connection;
  try
    ListQuery.SQL.Add('SELECT * FROM ' + TABLE_TABLE_IDENTIFIERS);
    try
      ListQuery.Active := True;
      if ListQuery.IsEmpty then Exit;

      ListQuery.First;
      while not ListQuery.Eof do
      begin
        TI := TTableIdentifier.Create;
        TI.ID := ListQuery.FieldByName('T_ID').AsInteger;
        TI.ID := ListQuery.FieldByName('R_ID').AsInteger;
        TI.Name := ListQuery.FieldByName('NAME').AsString;
        TI.TableNumber := ListQuery.FieldByName('TABLE_NUMBER').AsString;
        TI.XBeeHexID := ListQuery.FieldByName('XBeeHexID').AsString;
        TI.Connected := False;
        iQRID := ListQuery.FieldByName('QR_ID').AsInteger;
        // Get QR Code
        if iQRID > -1 then
        begin
          QRQuery.Active := False;
          QRQuery.SQL.Clear;
          QRQuery.SQL.Add('SELECT * FROM ' + TABLE_QR_CODE + ' WHERE idt_qr_code = ' + IntToStr(iQRID));
          QRQuery.Active := True;
          TI.QRCode := TQRCode.Create;
          TI.QRCode.ID := QRQuery.FieldByName('idt_qr_code').AsInteger;
          TI.QRCode.QRText := QRQuery.FieldByName('qr_code').AsString;
          TI.QRCode.PIN := QRQuery.FieldByName('pin').AsString;
        end;
        FTableIdentiefierList.Add(TI);
        ListQuery.Next;
      end;
    except
      on E: Exception do
      begin
        raise EDataModulError.Create('Couldn''t get Table-IDs from DB! Error: ' + sLineBreak + E.Message);
      end;
    end;
  finally
    ListQuery.Free;
    QRQuery.Free;
    Screen.Cursor := crDefault;
  end;
  Result := True;
end;

procedure TTableIdentifiers.LoadDataFromFile(aFileName: String);
var
  slSave: TStringList;
begin
  slSave := TStringList.Create;
  try
    slSave.LoadFromFile(aFileName);
    FTableIdentiefierList.FromJSON(slSave.Text);
  finally
    slSave.Free;
  end;
end;

end.
