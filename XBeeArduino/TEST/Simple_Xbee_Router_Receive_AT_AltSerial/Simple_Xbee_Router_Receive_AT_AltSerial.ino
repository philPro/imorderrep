//#include <SoftwareSerial.h>
#include <AltSoftSerial.h>

AltSoftSerial altSerial;  //Arduino RX: Pin8, TX: Pin9
//SoftwareSerial newSerial(10, 11); // RX, TX

byte nextByte;

void setup(){
  altSerial.begin(9600);
  Serial.begin(9600);
  Serial.println("Serial Start: Router");
}

void loop(){
  if(altSerial.available()){
    nextByte = altSerial.read();
    Serial.println(nextByte);
  } 
  //delay(1000);
}
