#include <SoftwareSerial.h>
//#include <AltSoftSerial.h>

//AltSoftSerial altSerial;  //Arduino RX: Pin8, TX: Pin9
SoftwareSerial newSerial(10, 11); // RX, TX

byte nextByte;

void setup(){
  newSerial.begin(9600);
  Serial.begin(9600);
  Serial.println("Serial Start: Router");
}

void loop(){
  if(newSerial.available()){
    nextByte = newSerial.read();
    Serial.println(nextByte);
  } 
  //delay(1000);
}
