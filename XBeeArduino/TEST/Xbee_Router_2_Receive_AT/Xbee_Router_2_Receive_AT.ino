#include <AltSoftSerial.h>


byte nextByte;

const int startChar = '*';
const int endChar = '#';
const int separatorChar = '|';
const long _msPerMsg = 1000;
// strPos (StringPosition) defines current String/Value is beeing read
// 0 ... Guid
// 1 ... Request
// 2 ... Reply
// 3 ... XbeeHexId
// 4 ... QRCode
// 5 ... QRPin
int strPos = 0;

String guid = "";
int request;
int reply;
String xbeeHexId = "";
String qrCode = "";
String qrPin = "";

//SoftwareSerial nss(ssRX, ssTX);
AltSoftSerial altSerial; // Arduino Uno RX: Pin8; TX:Pin:9; unusable PWM:Pin10

void setup(){
  Serial.begin(9600);
  altSerial.begin(9600); // 115200: Serial Port Frame Error 104 occurs
  Serial.println("Serial start: Slave table");
}

// ServerMessage
// ----------------------------------------------------------------------------------------
// Message Aufbau:
// START_CHAR  GUID | REQUEST | REPLY | XBeeHexID | QRCode | QR-Pin  END_CHAR
// Beispiel:
// *{0907466A-777C-4EF5-9F29-FD00A66E99B1}|0|4|ABCDEF1234|DasIstEinTestQR|DasIstEinTestPIN#
// *{0907466A-777C-4EF5-9F29-FD00A66E99B1}|0|4|ABC123|TestQR|TestPIN#
// dieses Beispiel kann in dieser Konsole nicht unter einmal abgeschickt werden (wird abgeschnitten)

// important:
// '\0' at the end of every String
// In C: 'a' is of type int, and "a" is of type char[2] (no const)
// char cAlphabet[] = "I know all!";
// is the same as
// char cAlphabet[] = {'I',' ', 'k','n','o','w',' ','a','l','l','!','\0'};


void loop(){

  if(altSerial.available()){
    nextByte = altSerial.read();

    //altSerialprint("input: ");
    //altSerialprintln(nextByte);
    
//    Serial.print("input: ");
//    Serial.println(nextByte);
    if(nextByte == '*'){  //gleiches Ergebnis: if(input = 42)
      // startChar was read -> set strPos to 0
      strPos = 0;
      
      unsigned long startTimeMsgRead = millis();
      boolean noEndChar = false;

      do{
        if(altSerial.available()){
          // read next Byte (should be first data byte from variable guid)
          nextByte = altSerial.read();

          if(nextByte == '|'){
            // reached Separator -> set strPos +1
            strPos++;
          }
          else if(nextByte != '|' and nextByte != '#'){
            switch (strPos){
            case 0:
              guid += nextByte;
              break;
            case 1:
              request = nextByte;
              break;
            case 2:
              reply = nextByte;
              break;
            case 3:
              xbeeHexId += nextByte;
              break;
            case 4:
              qrCode += nextByte;
              break;
            case 5:
              qrPin += nextByte;
              break;
            }
          }
        }
        unsigned long currentTime = millis();

        if((currentTime - startTimeMsgRead) >= _msPerMsg){
          //noEndChar = true;
        }
      }
      while (nextByte != '#' and noEndChar == false);
      // reached endChar or timeout;
      //      --> Error-Check:
      //      - 1. noEndChar == true
      //      - 2. simpleCheck (Guid with 4x "-"), data available for all variables

     
      Serial.print("guid: ");
      Serial.println(guid);

      Serial.print("request: ");
      Serial.println(request);
      Serial.write("request: ");
      Serial.write(request);
      Serial.write('\n');

      Serial.print("reply: ");
      Serial.println(reply);
      Serial.write("reply: ");
      Serial.write(reply);
      Serial.write('\n');

      Serial.print("XbeeHexId: ");
      Serial.println(xbeeHexId);

      Serial.print("QRCode: ");
      Serial.println(qrCode);

      Serial.print("QRPin: ");
      Serial.println(qrPin);

      // eventuell über SerialEvent: http://arduino.cc/en/Tutorial/SerialEvent
    }
  }
  delay(1500); 
}


//    int startByte = Serial.read();
//    if(startByte = 'd'


//    int lastByte = Serial.read();
//    Serial.println(lastByte);
//    Serial.println(lastByte, BIN);
//    Serial.println(lastByte, HEX);
//    Serial.write(lastByte);
//    Serial.write("\n");


//  Serial.println(n1, BIN);
//  Serial.println(n2, HEX);
//  Serial.println(n3, BIN);
//  Serial.println(i1, BIN);

//  Serial.println("a");
//  Serial.write("a");
//  Serial.write("\n");
//  Serial.println(100, BIN);
//  Serial.println(-100, BIN);
//  Serial.write(100);
//  Serial.write("\n");

//  for(int i=1000; i<=3000; i+=1000){
//    Serial.print("*");
//    Serial.print(i, HEX);
//    Serial.println("#");  
//  }

//  if(Serial.available()){
//    Serial.print(Serial.read());
//  }















