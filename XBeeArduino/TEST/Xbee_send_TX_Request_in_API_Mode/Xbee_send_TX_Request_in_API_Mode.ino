#include <AltSoftSerial.h>

AltSoftSerial altSerial;

byte nextByte;
byte reply[20];

uint8_t payload[] = {
  0x7E, 0x00, 0x12, 0x10, 0x01, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xB0, 0x99, 0x37, 0xFF, 0xFE, 0x00, 0x00,
  0x54, 0x65, 0x73, 0x74, 0xDC };

// uint8_t payload[] = {
// {0x7E, 0x00, 0x12, 0x10, 0x01, 0x00, 0x13, 0xA2, 0x00, 0x40, 0xB0, 0x99, 0x37, 0xFF, 0xFE,
// 0x00, 0x00, 0x54, 0x65, 0x73, 0x74, 0xDC};
  
// reply:
// print:
// 126, 0, 7, 139, 1, 27, 150, 0, 0, 0, 194, 
// print: HEX
// 7E, 0, 7, 8B, 1, 1B, 96, 0, 0, 0, C2, 
// write:
// ~  ¢ @°7ÿþ  TestÜ  
  
  
void setup()
{
  pinMode(led, OUTPUT);
  altSerial.begin(9600);
  Serial.begin(9600);
  Serial.write("Coordinator start");
  Serial.write(13); // CR: Carriage Return
  Serial.write(10); // LF: Line Feed
}

void loop()
{

  delay(5000);

  // writes to altSerial Pin
  for(int i = 0; i <= sizeof(payload)-1; i++){
    Serial.write(payload[i]);
    altSerial.write(payload[i]);
  }
  Serial.write(13); // CR: Carriage Return
  Serial.write(10); // LF: Line Feed
  altSerial.write(13); // CR: Carriage Return
  altSerial.write(10); // LF: Line Feed
  delay(200);

  int count = 0;
  // reads Response from altSerial Pin
  while (altSerial.available() > 0) {
    nextByte = (altSerial.read()); 
    reply[count] = nextByte;
    count++;
  }
  count--;

  Serial.println("Reply: (print)");
  for(int i = 0; i <= count; i++){
    Serial.print(reply[i]);
    Serial.print(", ");
  }
  Serial.println();
  Serial.println();

  Serial.println("Reply: (print HEX)");
  for(int i = 0; i <= count; i++){
    Serial.print(reply[i], HEX);
    Serial.print(", ");
  }
  Serial.println();
  Serial.println();

  delay(5000);
}

