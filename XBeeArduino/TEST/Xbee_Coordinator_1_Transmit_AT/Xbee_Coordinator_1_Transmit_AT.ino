#include <AltSoftSerial.h>

//SoftwareSerial softSerial(10, 11); // RX, TX
AltSoftSerial altSerial; // Arduino Uno RX: Pin8; TX:Pin:9; unusable PWM:Pin10
byte nextHWSerialByte;
byte nextAltSerialByte;
byte nextSoftSerialByte;

void setup(){
  Serial.begin(9600);
  Serial.println("Serial start: Master");
  altSerial.begin(9600); // 115200: Serial Port Frame Error 104 occurs
  delay(1000);


  // set the data rate for the SoftwareSerial port
  //softSerial.begin(115200);
}

void loop(){

  if(Serial.available()){
    nextAltSerialByte = Serial.read();
    Serial.println(nextAltSerialByte);
    altSerial.write(nextAltSerialByte);
  }
  //delay(1500);
}
