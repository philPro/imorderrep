#include <AltSoftSerial.h>

#include <XBee.h>

/*
This example is for Series 2 XBee
 Sends a ZB TX request with the value of analogRead(pin5) and checks the status response for success
*/

AltSoftSerial altSerial;
// create the XBee object
XBee xbee = XBee();

uint8_t payload[] = { 0x65, 0x66 };

// SH + SL Address of receiving XBee
XBeeAddress64 addr64 = XBeeAddress64(0x0013a200, 0x40b09937);
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();

int statusLed = 13;
int errorLed = 13;

void flashLed(int pin, int times, int wait) {

  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);

    if (i + 1 < times) {
      delay(wait);
    }
  }
}

void setup() {
  pinMode(statusLed, OUTPUT);
  pinMode(errorLed, OUTPUT);
  altSerial.begin(9600);
  Serial.begin(9600);
  xbee.setSerial(altSerial);
  Serial.println("Coordinator start");
}

void loop() {   
  
  delay(1500);
  xbee.send(zbTx);

  // flash TX indicator
  flashLed(statusLed, 1, 100);
  delay(500);
  
  while(altSerial.available() > 0){
    Serial.println(altSerial.read());
  }

//  // after sending a tx request, we expect a status response
//  // wait up to half second for the status response
//  if (xbee.readPacket(500)) {
//    // got a response!
//
//    // should be a znet tx status            	
//    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
//      xbee.getResponse().getZBTxStatusResponse(txStatus);
//
//      // get the delivery status, the fifth byte
//      if (txStatus.getDeliveryStatus() == SUCCESS) {
//        // success.  time to celebrate
//        flashLed(statusLed, 5, 50);
//      } else {
//        // the remote XBee did not receive our packet. is it powered on?
//        flashLed(errorLed, 3, 500);
//      }
//    }
//  } else if (xbee.getResponse().isError()) {
//    //nss.print("Error reading packet.  Error code: ");  
//    //nss.println(xbee.getResponse().getErrorCode());
//  } else {
//    // local XBee did not provide a timely TX Status Response -- should not happen
//    flashLed(errorLed, 2, 50);
//  }

  delay(1000);
}

