#include <AltSoftSerial.h>

AltSoftSerial altSerial;
//uint16_t payload[] = {7E, 00, 04, 08, 01, 4E, 4A, 5E};

uint8_t led = 13;

byte nextByte;
byte reply[20];

// S:0x53 L:0x4C; Chksum: 0x57
// reply:
//print:
//126, 0, 9, 136, 1, 83, 76, 0, 64, 176, 153, 55, 23, 
//print: HEX
//7E, 0, 9, 88, 1, 53, 4C, 0, 40, B0, 99, 37, 17, 
//write:
//~,  , 	, , , S, L,  , @, °, , 7, , 
// S:0x53 H:0x48; Chksum: 0x5B
//reply:
//print:
//126, 0, 9, 136, 1, 83, 72, 0, 0, 19, 162, 0, 38, 
//
//print: HEX
//7E, 0, 9, 88, 1, 53, 48, 0, 0, 13, A2, 0, 26, 
//
//write:
//~,  , 	, , , S, H,  ,  , , ¢,  , &, 

// I:0x49 D:0x44
// D:0x44 L:0x4C; Chksum: 66
// M:0x4D Y:0x59; Chksum: 50
uint8_t payload[] = {
  0x7E, 0x00, 0x04, 0x08, 0x01, 0x53, 0x4C, 0x57 };
void setup()
{
  pinMode(led, OUTPUT);
  altSerial.begin(9600);
  Serial.begin(9600);
  Serial.write("Start: Router");
  Serial.write(13); // CR: Carriage Return
  Serial.write(10); // LF: Line Feed
}

void loop()
{

  delay(500);

  for(int i = 0; i <= sizeof(payload)-1; i++){
    Serial.write(payload[i]);
    altSerial.write(payload[i]);
  }
  Serial.write(13); // CR: Carriage Return
  Serial.write(10); // LF: Line Feed
  altSerial.write(13); // CR: Carriage Return
  altSerial.write(10); // LF: Line Feed
  delay(200);
  int count = 0;
  while (altSerial.available() > 0) {
    nextByte = (altSerial.read()); 
    reply[count] = nextByte;
    count++;
  }
  count--;
  Serial.println();
  altSerial.println();    

  Serial.println("print:");
  for(int i = 0; i <= count; i++){
    Serial.print(reply[i]);
    Serial.print(", ");
  }
  Serial.println();
  Serial.println("print: HEX");
  for(int i = 0; i <= count; i++){
    Serial.print(reply[i], HEX);
    Serial.print(", ");
  }
  Serial.println();
  Serial.println("write:");
  for(int i = 0; i <= count; i++){
    Serial.write(reply[i]);
    Serial.write(", ");
  }
  Serial.println();
  digitalWrite(led, HIGH);
  delay(5000);
  digitalWrite(led, LOW);
}



