#include <SoftwareSerial.h>
//#include <AltSoftSerial.h>

//AltSoftSerial altSerial;  //Arduino RX: Pin8, TX: Pin9
SoftwareSerial newSerial(10, 11); // RX, TX
byte nextByte;

void setup(){
  newSerial.begin(9600);
  Serial.begin(9600);
  //newSerial.begin(9600);
  Serial.println("Serial Start: Coordinator");
}

void loop(){
  if(Serial.available()){
    nextByte = Serial.read();
    
    newSerial.write(nextByte);
    //altSerial.println("TestString");
    Serial.println(nextByte);
  }  
}

