#include <XBee.h>
#include <AltSoftSerial.h>

AltSoftSerial altSerial;  //Arduino RX: Pin8, TX: Pin9

// create the XBee object
XBee xbee = XBee();

uint8_t payload[] = { 
  0, 0 };
uint8_t a = 97;
uint8_t b = 98;

// SH + SL Address of receiving XBee
XBeeAddress64 addr64 = XBeeAddress64(0x0013a200, 0x40b09937);
ZBTxRequest zbTx = ZBTxRequest(addr64, payload, sizeof(payload));
ZBTxStatusResponse txStatus = ZBTxStatusResponse();

int statusLed = 13;
int errorLed = 13;

void flashLed(int pin, int times, int wait) {

  for (int i = 0; i < times; i++) {
    digitalWrite(pin, HIGH);
    delay(wait);
    digitalWrite(pin, LOW);

    if (i + 1 < times) {
      delay(wait);
    }
  }
}

void setup() {
  pinMode(statusLed, OUTPUT);
  pinMode(errorLed, OUTPUT);
  
  altSerial.begin(9600);
  Serial.begin(9600);
  Serial.println("Serial Start: Coordinator");
  
  xbee.setSerial(altSerial);
}

void loop() {   
  // break down 10-bit reading into two bytes and place in payload
  payload[0] = 97; // a;
  payload[1] = 98; // b;

  xbee.send(zbTx);

  // flash TX indicator
  flashLed(statusLed, 1, 100);

  delay(3500);
}


