#include <SoftwareSerial.h>
//#include <AltSoftSerial.h>

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset
// in which case, set this #define pin to 0!
#define TFT_DC     8

// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Option 2: use any pins but a little slower!
#define TFT_SCLK 13   // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);


//AltSoftSerial altSerial;  //Arduino RX: Pin8, TX: Pin9
SoftwareSerial altSerial(2, 3); // RX, TX

const long _msPerMsg = 1000;

void setup(){
  altSerial.begin(9600);
  Serial.begin(115200);
  // Use this initializer if you're using a 1.8" TFT
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
  
  tft.fillScreen(ST7735_BLACK);
  
  Serial.println("Serial Start: Router");
}

// Es werden maximal 63 Bytes am Remote Xbee empfangen (Serial Buffer wurde eig. auf 256 erhöht)
// -> max Payload = 63 - Preset - Checksum
// Preset ... 12 Bytes (90 RX received)
// Checksum ... 1 Byte

void loop(){
  //delay(1000);

  if(altSerial.available()){
    Serial.println(F("altSerial.available()"));
    char nextByte = altSerial.read();
    Serial.println(nextByte);
    
    // uint16_t, because 2 Bytes are reserved as length Byte
    uint16_t length = 0;
    uint8_t lengthCounter = 0;

    if(nextByte == 126){  // nextByte == '~'; DEC: 126; OCT: 176; HEX: 7E
      Serial.println(F("nextByte == 126"));
      boolean timeout = false;
      boolean success = false;
	  
      unsigned long startTimeMsgRead = millis();

      length = 0;
	  Serial.println("before Do");
      do{
        if(altSerial.available()){
          nextByte = altSerial.read();
          if(lengthCounter == 0){
            // first Length-HEX-Value is shifted (MSB), so add both Length Values can be added together
            length = nextByte << 8;
          }
          else if(lengthCounter == 1){
            // second Length-HEX-Value (LSB)
            length += nextByte;
          }
          lengthCounter++;
        }
        checkTimeout(startTimeMsgRead, timeout);
      }
      while((lengthCounter < 2 &&) (timeout == false));
	  Serial.println("after Do");
	  
      // payloadPreset defines fixed lenght of bytes which can be ignored:
      //- Start delimiter: 7E
      //- Length: 00 10 (16)
      //- Frame type: 90 (Receive Packet) -> IGNORE (= 1 Byte)
      //- 64-bit source address: 00 13 A2 00 40 B0 99 1C  -> IGNORE (= 8 Byte)
      //- 16-bit source address: 00 00  -> IGNORE (= 2 Byte)
      //- Receive options: 01  -> IGNORE (= 1 Byte)
      // --> 12 Byte to ignore
      //- Received data: 54 65 73 74 -> to be read
      //- Checksum: 74 
      uint8_t payloadPreset = 12; // defines amount of bytes to be discarded (xbee protocol)
      char payload[length - payloadPreset];

      Serial.print("sizeof(payload): ");
      Serial.println(sizeof(payload));
	  
	  /*
      lengthCounter = 0;
      do{
        //Serial.println("Start Do...");
        //uint8_t avlbl = altSerial.available();
        //Serial.print("altSerial.available(): ");
        //Serial.println(avlbl);
        //Serial.print("length: ");
        //Serial.println(length);
        if(altSerial.available() >= length){
          //          Serial.println("altSerial.available() / length");
          //          Serial.print(altSerial.available());
          //          Serial.print(" / ");
          //          Serial.println(length);
          for(int i = 0; i < payloadPreset; i++){
            // discard bytes used for xbee protocol
            nextByte = altSerial.read();
            //            Serial.print("discard byte i = ");
            //            Serial.println(i);
            //            Serial.println(nextByte, HEX);
          }
          //          Serial.print("sizeof(payload): ");
          //          Serial.println(sizeof(payload));
          for(int i = 0; i < sizeof(payload); i++){
            payload[i] = altSerial.read();      
          }
          // all needed data was read from Serial Buffer -> exit do-while loop
          success = true;
        }
        checkTimeout(startTimeMsgRead, timeout);
      }
      while(success == false && timeout == false);

      if(success == true){
        success = false;

        boolean statusEnabled = false;
        
        uint16_t pipeIndex = 0;
        if(statusEnabled = true){
          pipeIndex = seperator_position(payload, sizeof(payload), '|', 2);
        }
        else{
          pipeIndex = seperator_position(payload, sizeof(payload), '|', 1);
        }
        
        Serial.print("pipeIndex Position: ");
        Serial.println(pipeIndex);
        // payload: url|pin
        // sizeof(payload) = 7
        // -> pipeIndex = 3
        // sizeof(url) = 3 = pipeIndex
        // sizeof(pin) = 3 = sizeof(payload) - pipeIndex - 1
        uint8_t urlLength = (statusEnabled == true)?(pipeIndex - 2):(pipeIndex);
        char url[urlLength];
        char pin[(sizeof(payload) - pipeIndex - 1)];
        uint8_t status = 0;
        
        status = payload[0];
      
        for(uint16_t i = 2; i < sizeof(url); i++){
          url[i] = payload[i];
        }
        
        for(uint16_t i = 0; i < sizeof(pin); i++){
          pin[i] = payload[i + pipeIndex + 1];
        }    
                             
        Serial.println();
        Serial.print("payload: ");
        for(uint16_t i = 0; i < sizeof(payload); i++){
          Serial.print(payload[i]);        
        }

        Serial.println();
        Serial.print("url: ");
        for(uint16_t i = 0; i < sizeof(url); i++){
          Serial.print(url[i]);       
        }
        Serial.println();
        Serial.print("sizeof(url): ");
        Serial.println(sizeof(url));
        Serial.print("strlen(url): ");
        Serial.println(strlen(url));        
        
        Serial.println();
        Serial.print("pin: ");
        for(uint16_t i = 0; i < sizeof(pin); i++){
          Serial.print(pin[i]);        
        }
        Serial.println();
        
        tft.setTextWrap(false);
        
        tft.fillScreen(ST7735_BLACK);
        tft.setCursor(0, 0);
        tft.setTextColor(ST7735_GREEN);
        tft.setTextSize(1);
        tft.print("URL: ");
        for(uint16_t i = 0; i < sizeof(url); i++){
          tft.print(url[i]);       
        }
        
        tft.println();
        tft.setTextColor(ST7735_BLUE);
        tft.print("Pin: ");
        for(uint16_t i = 0; i < sizeof(pin); i++){
          tft.print(pin[i]);        
        }
        tft.println();

      } // end if(success == true)


      // ToDo: payload defined on this "level" -> draw to LCD display on this "level"

	  */
    } // end if(nextByte == 126)
	
  } // end if(altSerial.available())

} // end loop

//char[10] getZigBeeReceivePacket(){
//
//
//} // end getZigBeeReceivePacket


/// <summary>
/// returns the index of the specific byte
/// </summary>
/// <param name="payload">array of data bytes</param>
/// <param name="sizeOfPayload">size of data arry</param>
/// <param name="specCharacter">character to be searched for (i.e. '|')</param>
/// <param name="byteCount">the index of which occurrence of the specific byte should be returned</param>
uint16_t seperator_position(char *payload, uint16_t sizeOfPayload, byte specCharacter, uint8_t byteCount){
  uint8_t curByteCount = 0;
  
  for(uint16_t i = 0; i < sizeOfPayload; i++){
    if(payload[i] == specCharacter){ //payload[i] == '|'; DEC: 124; OCT: 174; HEX: 7C  
      curByteCount++;
      if(byteCount == curByteCount){
        return i;
      }
    } // end if
  } // end for
} // end function seperator_position

void checkTimeout(unsigned long &startTimeMsgRead, boolean &timeout){
  if((millis() - startTimeMsgRead) >= _msPerMsg){
    timeout = true;
	Serial.println(F("TIMEOUT"));
  }
} // end checkTimeout








