#include <SoftwareSerial.h>
//#include <AltSoftSerial.h>

//AltSoftSerial altSerial;  //Arduino RX: Pin8, TX: Pin9
SoftwareSerial altSerial(2, 3); // RX, TX

const long _msPerMsg = 1000;

void setup(){
  altSerial.begin(9600);
  Serial.begin(115200);
  
  Serial.println("Serial Start: Router");
}

// Es werden maximal 63 Bytes am Remote Xbee empfangen (Serial Buffer wurde eig. auf 256 erhöht)
// -> max Payload = 63 - Preset - Checksum
// Preset ... 12 Bytes (90 RX received)
// Checksum ... 1 Byte

void loop(){
  delay(1000);

  if(altSerial.available()){
    //Serial.println("altSerial.available()");
    char nextByte = altSerial.read();

    // uint16_t, because 2 Bytes are reserved as length Byte
    uint16_t length = 0;
    uint8_t lengthCounter = 0;

    if(nextByte == 126){  // nextByte == '~'; DEC: 126; OCT: 176; HEX: 7E

      boolean timeout = false;
      boolean success = false;

      unsigned long startTimeMsgRead = millis();

      length = 0;
      do{
        if(altSerial.available()){
          nextByte = altSerial.read();
          if(lengthCounter == 0){
            // first Length-HEX-Value is shifted, so add both Length Values can be added together
            // -> MSB
            length = nextByte << 8;
          }
          else if(lengthCounter == 1){
            // second Length-HEX-Value (LSB)
            length += nextByte;
          }
          lengthCounter++;
        }
        checkTimeout(startTimeMsgRead, timeout);
      }
      while(lengthCounter < 2 && timeout == false);

      // payloadPreset defines fixed lenght of bytes which can be ignored:
      //- Start delimiter: 7E
      //- Length: 00 10 (16)
      //- Frame type: 90 (Receive Packet) -> IGNORE (= 1 Byte)
      //- 64-bit source address: 00 13 A2 00 40 B0 99 1C  -> IGNORE (= 8 Byte)
      //- 16-bit source address: 00 00  -> IGNORE (= 2 Byte)
      //- Receive options: 01  -> IGNORE (= 1 Byte)
      // --> 12 Byte to ignore
      //- Received data: 54 65 73 74 -> to be read
      //- Checksum: 74 
      uint8_t payloadPreset = 12;
      char payload[length - payloadPreset];

      Serial.print("sizeof(payload): ");
      Serial.println(sizeof(payload));

      lengthCounter = 0;
      do{
        //Serial.println("Start Do...");
        //uint8_t avlbl = altSerial.available();
        //Serial.print("altSerial.available(): ");
        //Serial.println(avlbl);
        //Serial.print("length: ");
        //Serial.println(length);
        if(altSerial.available() >= length){
          //          Serial.println("altSerial.available() / length");
          //          Serial.print(altSerial.available());
          //          Serial.print(" / ");
          //          Serial.println(length);

          for(int i = 0; i < payloadPreset; i++){
            nextByte = altSerial.read();
            //            Serial.print("discard byte i = ");
            //            Serial.println(i);
            //            Serial.println(nextByte, HEX);
          }
          //          Serial.print("sizeof(payload): ");
          //          Serial.println(sizeof(payload));
          for(int i = 0; i < sizeof(payload); i++){
            payload[i] = altSerial.read();      
          }
          // all needed data was read from Serial Buffer -> exit do-while loop
          success = true;
        }                  
        checkTimeout(startTimeMsgRead, timeout);
      }
      while(success == false && timeout == false);

      if(success == true){
        success = false;

        uint16_t pipeIndex = seperator_position(payload, sizeof(payload));
        Serial.print("pipeIndex Position: ");
        Serial.println(pipeIndex);
        // payload: url|pin
        // sizeof(payload) = 7
        // -> pipeIndex = 3
        // sizeof(url) = 3 = pipeIndex
        // sizeof(pin) = 3 = sizeof(payload) - pipeIndex - 1
        
        char url[pipeIndex];
        char pin[(sizeof(payload) - pipeIndex - 1)];
        
      
        for(uint16_t i = 0; i < sizeof(url); i++){
          url[i] = payload[i];
        }
        
        for(uint16_t i = 0; i < sizeof(pin); i++){
          pin[i] = payload[i + pipeIndex + 1];
        }    
                             
        Serial.println();
        Serial.print("payload: ");
        for(uint16_t i = 0; i < sizeof(payload); i++){
          Serial.print(payload[i]);        
        }

        Serial.println();
        Serial.print("url: ");
        for(uint16_t i = 0; i < sizeof(url); i++){
          Serial.print(url[i]);       
        }
        Serial.println();
        Serial.print("sizeof(url): ");
        Serial.println(sizeof(url));
        Serial.print("strlen(url): ");
        Serial.println(strlen(url));        
        
        Serial.println();
        Serial.print("pin: ");
        for(uint16_t i = 0; i < sizeof(pin); i++){
          Serial.print(pin[i]);        
        }
        Serial.println();
      } // end if(success == true)


      // ToDo: payload defined on this "level" -> draw to LCD display on this "level"


    } // end if(nextByte == 126)
  } // end if(altSerial.available())

} // end loop

//char[10] getZigBeeReceivePacket(){
//
//
//} // end getZigBeeReceivePacket


uint16_t seperator_position(char *payload, uint16_t sizeOfPayload){

  for(uint16_t i = 0; i < sizeOfPayload; i++){
    if(payload[i] == '|'){ //payload[i] == '|'; DEC: 124; OCT: 174; HEX: 7C  
      return i;
    } // end if
  } // end for
} // end function seperator_position

void checkTimeout(unsigned long &startTimeMsgRead, boolean &timeout){
  if((millis() - startTimeMsgRead) >= _msPerMsg){
    timeout = true;
  }
} // end checkTimeout








