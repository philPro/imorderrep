version: 3
encoding mode: Byte
error correction: L
text: "www.testurl.com"
text lenght: 15

DataCodeWordsNeeded: 55
-> DataCodeBitsNeeded = 55*8 = 440

ConcatString (length = 136):
0100000011110111011101110111011101110010111001110100011001010111001101110100011101010111001001101100001011100110 001101101111011011010000
PaddingByte (length = 440-136 = 304):
236, 17

DataCodeWordsGroup1:
[0] 01000000 -> 64
[1] 11110111 -> 247
[2] 01110111 -> 119
[3] 01110111 -> 119
[4] 01110010 -> 114
...
[14] 00110110 -> 54
[15] 11110110 -> 246
[16] 11010000 -> 208
...
[51] 236
[52] 17
[53] 236
[54] 17

CORRECT!!!

my Output:
_concatenatedBitArray: 48, 49, 48, 48, 48, 48, 48, 48, 49, 49, 49, 49, 48, 49, 49, 49, 48, 49, 49, 49, 48, 49, 49, 49, 48, 49, 49, 49, 48, 49, 49, 49, 48, 49, 49, 49, 48, 48, 49, 48, 49, 49, 49, 48, 48, 49, 49, 49, 48, 49, 48, 48, 48, 49, 49, 48, 48, 49, 48, 49, 48, 49, 49, 49, 48, 48, 49, 49, 48, 49, 49, 49, 48, 49, 48, 48, 48, 49, 49, 49, 48, 49, 48, 49, 48, 49, 49, 49, 48, 48, 49, 48, 48, 49, 49, 48, 49, 49, 48, 48, 48, 48, 49, 48, 49, 49, 49, 48, 48, 49, 49, 48, 48, 48, 49, 49, 48, 49, 49, 48, 49, 49, 49, 49, 48, 49, 49, 48, 49, 49, 48, 49, 48, 48, 48, 48, 

DataCodeWord[GROUP][BLOCK][WORD]:
DataCodeWord[0][0][0]: 64
DataCodeWord[0][0][1]: 247
DataCodeWord[0][0][2]: 119
DataCodeWord[0][0][3]: 119
DataCodeWord[0][0][4]: 114
DataCodeWord[0][0][5]: 231
DataCodeWord[0][0][6]: 70
DataCodeWord[0][0][7]: 87
DataCodeWord[0][0][8]: 55
DataCodeWord[0][0][9]: 71
DataCodeWord[0][0][10]: 87
DataCodeWord[0][0][11]: 38
DataCodeWord[0][0][12]: 194
DataCodeWord[0][0][13]: 230
DataCodeWord[0][0][14]: 54
DataCodeWord[0][0][15]: 246
DataCodeWord[0][0][16]: 208
DataCodeWord[0][0][17]: 236
DataCodeWord[0][0][18]: 17
DataCodeWord[0][0][19]: 236
DataCodeWord[0][0][20]: 17
DataCodeWord[0][0][21]: 236
DataCodeWord[0][0][22]: 17
DataCodeWord[0][0][23]: 236
DataCodeWord[0][0][24]: 17
DataCodeWord[0][0][25]: 236
DataCodeWord[0][0][26]: 17
DataCodeWord[0][0][27]: 236
DataCodeWord[0][0][28]: 17
DataCodeWord[0][0][29]: 236
DataCodeWord[0][0][30]: 17
DataCodeWord[0][0][31]: 236
DataCodeWord[0][0][32]: 17
DataCodeWord[0][0][33]: 236
DataCodeWord[0][0][34]: 17
DataCodeWord[0][0][35]: 236
DataCodeWord[0][0][36]: 17
DataCodeWord[0][0][37]: 236
DataCodeWord[0][0][38]: 17
DataCodeWord[0][0][39]: 236
DataCodeWord[0][0][40]: 17
DataCodeWord[0][0][41]: 236
DataCodeWord[0][0][42]: 17
DataCodeWord[0][0][43]: 236
DataCodeWord[0][0][44]: 17
DataCodeWord[0][0][45]: 236
DataCodeWord[0][0][46]: 17
DataCodeWord[0][0][47]: 236
DataCodeWord[0][0][48]: 17
DataCodeWord[0][0][49]: 236
DataCodeWord[0][0][50]: 17
DataCodeWord[0][0][51]: 236
DataCodeWord[0][0][52]: 17
DataCodeWord[0][0][53]: 236
DataCodeWord[0][0][54]: 17
