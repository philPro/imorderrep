//this is a Sketch to test the QRCodeBitGenerator
// libraries will be included from Arduino library directory (C:\Programs\Arduino-1.0.6\libraries)

#include <QRCodeBitGenerator.h>
#include <QRCodeDrawer.h>
#include <myLayout.h>
#include <myArray.h>

#include <Adafruit_GFX\Adafruit_GFX.h>
#include <Adafruit_ST7735\Adafruit_ST7735.h>
#include <SPI\SPI.h>

//ToDo: Why does it have to be included here and not in QRCodeDrawer...?
#include <EEPROM\EEPROM.h>

uint16_t loopCount = 0;
void setup(){
	Serial.begin(115200);

	Serial.println(F("Setup DONE"));

}

void loop(){
	Serial.print(F("\nLoop START "));
	Serial.println(++loopCount);
	Serial.println();

	Serial.println();
	//char* charUrl = "www.testurl.com";
	//uint8_t length = 15;

	uint8_t const version = 2;
	ErrCorrCodeWordsInfo::errCorrLevel_enum const errCorrLvl = ErrCorrCodeWordsInfo::ecl_L;
	QRCodeBitGenerator::encMode_enum const encMode = QRCodeBitGenerator::em_BYTE;

	//char* charUrl = "There\\'s a frood who really knows where his towel is."; //Double Backslash for '\' at index 8
	//uint8_t length = 53;
	
	//char* charUrl = "www.testurl.com";
	//uint8_t length = 15;

	//char* charUrl = "www.testurl.com|123456789112345";
	//uint8_t length = 31;

	//char* charUrl = "koa Stress - mia ham da jetzt Platz f. 53 Zeichen :-P";
	//uint8_t length = 53;

	char* charUrl = "12345678911234567891123456789112";
	uint8_t length = 32;

	uint8_t* url = (uint8_t *) charUrl;
	
	QRCodeBitGenerator* bitGen = new QRCodeBitGenerator(version, encMode, errCorrLvl);
	uint8_t* finalStructuredQRbyteArray = NULL;
	finalStructuredQRbyteArray = bitGen->getFinalStructuredQRbyteArrayOfText(url, length);
	uint8_t finalStructuredQRbyteArrayLength = bitGen->getFinalStructuredQRbyteArrayLength();
	uint8_t finalRemainderBitLength = bitGen->getFinalRemainderBitLength();
	
	delete bitGen;
	
	//version 5; erCorrLvl Q; url: "There\\'s a frood who really knows where his towel is.";  length = 53:
	//uint8_t finalStructuredQRbyteArray[134] = { 67, 246, 182, 70, 85, 246, 230, 247, 70, 66, 247, 118, 134, 7, 119, 86, 87, 118, 50, 194, 38, 134, 7, 6, 85, 242, 118, 151, 194, 7, 134, 50, 119, 38, 87, 16, 50, 86, 38, 236, 6, 22, 82, 17, 18, 198, 6, 236, 6, 199, 134, 17, 103, 146, 151, 236, 38, 6, 50, 17, 7, 236, 213, 87, 148, 235, 199, 204, 116, 159, 11, 96, 177, 5, 45, 60, 212, 173, 115, 202, 76, 24, 247, 182, 133, 147, 241, 124, 75, 59, 223, 157, 242, 33, 229, 200, 238, 106, 248, 134, 76, 40, 154, 27, 195, 255, 117, 129, 230, 172, 154, 209, 189, 82, 111, 17, 10, 2, 86, 163, 108, 131, 161, 163, 240, 32, 111, 120, 192, 178, 39, 133, 141, 236 };
	//uint8_t finalStructuredQRbyteArrayLength = 134;
	//uint8_t finalRemainderBitLength = 7;

	////version 3; errCorrLvl L, url: "www.testurl.com"; length = 15:
	//uint8_t const finalStructuredQRbyteArrayLength = 70;
	//uint8_t finalStructuredQRbyteArray[finalStructuredQRbyteArrayLength] = { 64, 247, 119, 119, 114, 231, 70, 87, 55, 71, 87, 38, 194, 230, 54, 246, 208, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 236, 17, 102, 74, 179, 164, 110, 89, 207, 22, 255, 213, 150, 9, 48, 93, 246 };
	//uint8_t finalRemainderBitLength = 7;


	Serial.print(F("finalStructuredQRbyteArrayLength: "));
	Serial.println(finalStructuredQRbyteArrayLength);

	Serial.println(F("finalStructuredQRbyteArray: "));
	myMath::print1dArray(finalStructuredQRbyteArray, finalStructuredQRbyteArrayLength);

	Serial.print(F("finalRemainderBitLength: "));
	Serial.println(finalRemainderBitLength);


	Serial.println(F("finalStructuredQRBits: "));
	for (uint8_t i = 0; i < finalStructuredQRbyteArrayLength; i++)
	{
		uint8_t* bitArray = myMath::getBinaryArrayOfDec(finalStructuredQRbyteArray[i], 8);
		
		for (uint8_t j = 0; j < 8; j++)
		{
			Serial.print(bitArray[j]);
			Serial.print(", ");
		}
		myMath::free1dArray(bitArray, 8);
	}
	Serial.println();


	//myArray arr1 = myArray((uint8_t) 3);
	//arr1.printMyArray();
	//arr1.increase(5);
	//arr1.printMyArray();


	myLayout tmpLayout = myLayout(myLayout::centred, myLayout::middle);
	QRCodeDrawer qrDrawer = QRCodeDrawer(version, errCorrLvl, tmpLayout, 128, 160);
	qrDrawer.drawQRCode(finalStructuredQRbyteArray, finalStructuredQRbyteArrayLength, finalRemainderBitLength);
	
	myMath::free1dArray(finalStructuredQRbyteArray, finalStructuredQRbyteArrayLength);

	//Serial.print(F("delay(3000)..."));
	//delay(3000); // wait 3 seconds
	//Serial.println(F("Loop END"));
	Serial.println();
}