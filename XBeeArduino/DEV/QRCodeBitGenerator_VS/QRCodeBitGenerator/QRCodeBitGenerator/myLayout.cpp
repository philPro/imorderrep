#include "myLayout.h"

#pragma region  constructor
myLayout::myLayout()
{
}

myLayout::myLayout(horizontal horLayout, vertical vertLayout){
	this->_horizontal = horLayout;
	this->_vertical = vertLayout;
}

/// <summary>
/// Initializes a new instance of the <see cref="myLayout"/> class.
/// <para>// copy constructor - used for objects, which not yet exist</para>
/// </summary>
/// <param name="sourceLayout">The source layout.</param>
myLayout::myLayout(const myLayout& sourceLayout){
	this->_horizontal = sourceLayout._horizontal;
	this->_vertical = sourceLayout._vertical;
}
#pragma endregion  constructor


#pragma region operators
myLayout& myLayout::operator=(const myLayout& sourceLayout){

	if (this == &sourceLayout)
		return *this;

	this->_horizontal = sourceLayout._horizontal;
	this->_vertical = sourceLayout._vertical;
}
#pragma endregion operators

#pragma region  destructor
myLayout::~myLayout()
{
}
#pragma endregion  destructor