#include "myMath.h"


myMath::myMath()
{
}


myMath::~myMath()
{
}
	
/// <summary>
/// copies the values from source to destination
/// </summary>
/// <param name="dest">The destination</param>
/// <param name="source">The source</param>
/// <returns></returns>
uint8_t* myMath::uint8_tcpy(uint8_t *dest, uint8_t *source){
	//// from the internet, not sure about that...
	// uint8_t *retVal = dest;
	//do{
	//	*dest++ = *source++;
	//} while (*source != NULL);
	//*dest = NULL;
	// |-> Problem here: if source[0] = NULL:
	// 1.) dest[0] = source[0] = NULL; (correct), after this expression both are pointers are incremented
	// 2.) now dest[1] will also be set to NULL;
	//return retVal;

	////should work IMHO
	//while (*source != NULL){
	//	*dest++ = *source++;
	//}
	//*dest = NULL;
	//

	uint8_t counter = 0;

	while (source[counter] != NULL){
		dest[counter] = source[counter];
		counter++;
	}
	if (counter == 0){
		dest[0] = NULL;
	}
	return dest;
}

	uint8_t  myMath::uint8_ArrLength(uint8_t *array){
		uint8_t counter = 0;
		while (array[counter] != NULL){
			counter++;
		}
		return counter;
	}


uint8_t* myMath::getBinaryArrayOfDec(uint16_t const decimalNumber, uint8_t const optArrLength = 0){
	uint8_t remainder = 0;
	uint8_t count = 0;
	
	uint16_t decTmp = decimalNumber;

	do{
		decTmp /= 2;
		count++;
	} while (decTmp > 0);

	if (optArrLength > count){
		count = optArrLength;
	}
	else if (optArrLength < count)
	{
		//ToDo: set Error
		//setError(5)
	}
	decTmp = decimalNumber;
	uint8_t counttmp = count-1;
	uint8_t* arr = (uint8_t*)malloc(count);

	do{
		remainder = decTmp % 2;
		
		if (remainder == 0){
			arr[counttmp] = 0; // '0';
		}
		else{
			arr[counttmp] = 1; // '1';
		}

		decTmp /= 2;
		counttmp--;
	} while (decTmp > 0);

	while (counttmp < 255){
		arr[counttmp] = 0; // '0';
		counttmp--;
	}
	return arr;
}


/// <summary>
/// <para>Switches the values of the array</para>
/// <para>i.e.: [0][1][2] -> [2][1][0]</para>
/// </summary>
/// <param name="arr">The arr.</param>
/// <param name="length">The length.</param>
void myMath::switchArrValues(uint8_t* arr, uint8_t const length){

	uint8_t tmpVal = 0;
	uint8_t tmpLength = length / 2;
	for (uint8_t i = 0; i < tmpLength; i++)
	{
		tmpVal = arr[i];
		arr[i] = arr[length - 1 - i];
		arr[length - 1 - i] = tmpVal;
	}
}

/// <summary>
/// mallocs a uint8_t 2D array
/// </summary>
/// <param name="rowCount">The first size.</param>
/// <param name="colCount">Size of the sec.</param>
/// <returns></returns>
void** myMath::malloc2dArray(uint8_t const rowCount, uint8_t const colCount){
	//ToDo: use Template
	void** arr = (void**)malloc(rowCount * sizeof(uint8_t*));
	if (arr == NULL){
		Serial.println(F("Error: malloc not succesful -> arr == NULL"));
	}
	else
	{
		for (uint8_t i = 0; i < rowCount; i++)
		{
			arr[i] = (uint8_t*)malloc(colCount * sizeof(uint8_t));
			if (arr[i] == NULL){
				Serial.print(F("Error: malloc not succesful -> arr[i] == NULL; i = "));
				Serial.println(i);
			}
		}
	}
	return arr;
}

/// <summary>
/// callocs a uint8_t 2D array
/// </summary>
/// <param name="rowCount">The first size.</param>
/// <param name="colCount">Size of the sec.</param>
/// <returns></returns>
void** myMath::calloc2dArray(uint8_t const rowCount, uint8_t const colCount){
	//ToDo: use Template
	void** arr = (void**)calloc(rowCount, sizeof(uint8_t*));
	if(arr == NULL)
		Serial.println(F("arr == NULL"));

	for (uint8_t i = 0; i < rowCount; i++)
	{
		arr[i] = (uint8_t*)calloc(colCount, sizeof(uint8_t));
		if(arr[i] == NULL){
			Serial.print(F("arr[i] == NULL; i = "));
			Serial.println(i);
		}
	}
	return arr;
}

/// <summary>
/// Frees a 1D array
/// <para>length defines the size (number of elements) of the array</para>
/// </summary>
/// <param name="array">The array.</param>
/// <param name="length">The size of the array</param>
void myMath::free1dArray(uint8_t* array, uint8_t length){
	if (array == NULL){
		Serial.println(F("arr == NULL"));
	}
	else
	{
		free(array);
		array = NULL;
	}
}

/// <summary>
/// Frees a 2D array
/// <para>rowCount defines the size of the "first level array": array[rowCount][colCount]</para>
/// </summary>
/// <param name="array">The array.</param>
/// <param name="size">The size of the "first level array"</param>
void myMath::free2dArray(uint8_t** array, uint8_t rowCount){

	if (array != NULL)
	{
		for (uint8_t i = 0; i < rowCount; i++)
		{
			if (array[i] != NULL)
			{
				free(array[i]);
			}
		}
		free(array);
	}
}


/// <summary>
/// <para>mallocs an uint8_t 2D array as 1D array</para>
/// <para>accessing arr[r][c]: uint8_t tmp = arr[r * colCount + c]: </para>
/// <para>attention: returned array has to be freed after usage: free(arr);</para>
/// </summary>
/// <param name="rowCount">amount of rows</param>
/// <param name="colCount">amount of columns</param>
/// <returns></returns>
void* myMath::malloc2dArrayAs1dArray(uint8_t const rowCount, uint8_t colCount){
	uint8_t* arr = (uint8_t*)malloc(rowCount * colCount * sizeof(uint8_t));
	if (arr == NULL)
		Serial.println(F("arr == NULL"));
	return arr;
}

/// <summary>
/// <para>callocs an uint8_t 2D array as 1D array</para>
/// <para>accessing arr[r][c]: uint8_t tmp = arr[r * colCount + c]: </para>
/// <para>attention: returned array has to be freed after usage: free(arr);</para>
/// </summary>
/// <param name="rowCount">amount of rows</param>
/// <param name="colCount">amount of columns</param>
/// <returns></returns>
void* myMath::calloc2dArrayAs1dArray(uint8_t const rowCount, uint8_t colCount){
	uint8_t* arr = (uint8_t*)calloc(rowCount * colCount, sizeof(uint8_t));
	if (arr == NULL)
		Serial.println(F("arr == NULL"));
	return arr;
}


/// <summary>
/// calculates and returns the result for exponential expresions (i.e. 2^3 = 8) (attention: works only for integer values (base & exp) and positive exponents)
/// </summary>
/// <param name="base">The base.</param>
/// <param name="exp">The exp.</param>
/// <returns></returns>
int16_t myMath::myIntExp(int16_t const base, const int16_t exp){
	int16_t result = base;
	if (exp == 0)
	{
		result = 1;
	}
	else
	{
		int16_t tmpExp = exp;
		if (tmpExp < 0)
			tmpExp *= -1;

		for (size_t i = 1; i < tmpExp; i++)
		{
			result *= base;
		}

		if (exp < 0)
		{
			result = 1 / result;
		}
	}
	return result;
}


/// <summary>
/// <para>Prints an 1d array</para>
/// </summary>
/// <param name="array1D">The array1 d.</param>
/// <param name="count">The count.</param>
void myMath::print1dArray(uint8_t* const array1D, uint16_t const count){
	if (array1D == NULL)
	{
		Serial.println(F("array == NULL"));
	}
	else
	{
		for (uint16_t i = 0; i < count; i++)
		{
			Serial.print(array1D[i]);
			(i + 1 < count) ? (Serial.print(F(", "))) : (Serial.println());
		}
	}
}


/// <summary>
/// <para>Prints an 2d array</para>
/// <para>rowCount and colCount define the amount of elements:</para>
/// <para>rowCount = 2 -> 2 rows with indexes: 0, 1</para>
/// <para>colCount = 3 -> 3 cols with indexes: 0, 1, 2</para>
/// <para>i.e.:</para>
/// <para>1 2 3</para>
/// <para>4 5 6</para>
/// </summary>
/// <param name="array2D">The 2d array</param>
/// <param name="xSize">size of x</param>
/// <param name="ySize">size of y</param>
void myMath::print2dArray(uint8_t** const array2D, uint8_t const rowCount, uint8_t const colCount){
	Serial.println(F("2D Array:"));
	
	if (array2D == NULL)
	{
		Serial.println(F("array == NULL"));
	}
	else
	{
		for (uint8_t rowIndex = 0; rowIndex < rowCount; rowIndex++)
		{
			if (array2D[rowIndex] == NULL)
			{
				Serial.print(F(" | array["));
				Serial.print(rowIndex);
				Serial.print(F("] == NULL | "));
			}

			for (uint8_t colIndex = 0; colIndex < colCount; colIndex++)
			{
				Serial.print(array2D[rowIndex][colIndex]);
				(colIndex + 1 < colCount) ? (Serial.print(F(", "))) : (Serial.println());
			}
		}
	}
}

/// <summary>
/// <para>Prints an 2d array</para>
/// <para>rowCount and colCount define the amount of elements:</para>
/// <para>rowCount = 2 -> 2 rows with indexes: 0, 1</para>
/// <para>colCount = 3 -> 3 cols with indexes: 0, 1, 2</para>
/// <para>i.e.:</para>
/// <para>1 2 3</para>
/// <para>4 5 6</para>
/// </summary>
/// <param name="array2D">The 2d array</param>
/// <param name="xSize">size of x</param>
/// <param name="ySize">size of y</param>
void myMath::print1dArrayAs2dArray(uint8_t* const array1D, uint8_t const rowCount, uint8_t const colCount){
	Serial.println(F("2D Array:"));
	for (uint8_t rowIndex = 0; rowIndex < rowCount; rowIndex++)
	{
		for (uint8_t colIndex = 0; colIndex < colCount; colIndex++)
		{
			Serial.print(array1D[rowIndex * colCount + colIndex]);
			(colIndex + 1 < colCount) ? Serial.print(F(", ")) : Serial.println();
		}
	}
}
