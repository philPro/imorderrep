#pragma once

#ifndef myLayout_H
#define	myLayout_H

//#include <cstdlib>
////#include <iostream>
//#include <stdint.h>

//#include "Arduino.h"
#if ARDUINO >= 100
#include "Arduino.h"
//#include "Print.h"
#else
#include "WProgram.h"
#endif


class myLayout
{
public:
	enum horizontal
	{
		left = 0,
		centred = 1,
		right = 2,
	};
	
	enum vertical
	{
		top = 0,
		middle = 1,
		bottom = 2,
	};

#pragma region constructor  
	myLayout();
	myLayout(horizontal horLayout, vertical vertLayout);
	myLayout(const myLayout& sourceLayout);
#pragma endregion constructor

#pragma region operators
	myLayout& operator=(const myLayout& sourceLayout);
#pragma endregion operators

#pragma region destructor  
	~myLayout();
#pragma endregion destructor  		

#pragma region variables
	horizontal _horizontal;
	vertical _vertical;
#pragma endregion variables
};

#endif