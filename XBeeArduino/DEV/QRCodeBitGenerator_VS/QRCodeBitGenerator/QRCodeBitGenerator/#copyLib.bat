@ECHO OFF
cls

for %%* in (.) do set CurrDirName=%%~n*
echo Library: %CurrDirName%

echo aktuelle Lib-Files von %CurrDirName% wirklich umbenennen und neu-einspielen?

set libFolderC=C:\Programs\Arduino-1.6.1\libraries
set libFolderDocuments=%UserProfile%\Documents\Arduino\libraries
pause

set curTimeStamp=%date:~6,4%-%date:~3,2%-%date:~0,2%_%time:~0,2%%time:~3,2%%time:~6,2%

for %%a in (%libFolderC%\%CurrDirName%\*.*) do xcopy /Y "%%~a" "%libFolderC%\%CurrDirName%\archive\%%~na_%curTimeStamp%%%~xa"*
xcopy /Y .\*.cpp %libFolderC%\%CurrDirName%\
xcopy /Y .\*.h %libFolderC%\%CurrDirName%\

for %%a in (%libFolderDocuments%\%CurrDirName%\*.*) do xcopy /Y "%%~a" "%libFolderDocuments%\%CurrDirName%\archive\%%~na_%curTimeStamp%%%~xa"*
xcopy /Y .\*.cpp %libFolderDocuments%\%CurrDirName%\
xcopy /Y .\*.h %libFolderDocuments%\%CurrDirName%\

pause
cd\