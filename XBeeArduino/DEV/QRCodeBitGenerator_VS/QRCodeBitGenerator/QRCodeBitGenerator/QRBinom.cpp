#include "QRBinom.h"

QRBinom::QRBinom()
{
	this->operation = '+';
}


QRBinom::~QRBinom()
{
}

QRBinom::QRBinom(QRMonom monom1, QRMonom monom2){
	this->monom1 = monom1;
	this->monom2 = monom2;
	this->operation = '+';
}
QRBinom::QRBinom(uint16_t a1, uint16_t x1, uint16_t a2, uint16_t x2){
	this->monom1.a = a1;
	this->monom1.x = x1;
	this->monom2.a = a2;
	this->monom2.x = x2;
	this->operation = '+';
}