#pragma once

#include<QRCodeBitGenerator\QRMonom.h>

#ifndef QRBinom_H
#define	QRBinom_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class QRBinom
{
public:
	QRBinom();
	QRBinom(QRMonom monom1, QRMonom monom2);
	QRBinom(uint16_t a1, uint16_t x1, uint16_t a2, uint16_t x2);
	~QRBinom();

#pragma region getterSetter_Methods

#pragma endregion getterSetter_Methods

#pragma region  public_Variables
	QRMonom monom1;
	QRMonom monom2;
	uint8_t operation; // '+' or '-'
#pragma endregion public_Variables

private:



};


#endif