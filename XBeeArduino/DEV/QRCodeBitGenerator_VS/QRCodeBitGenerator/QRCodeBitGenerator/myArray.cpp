#include "myArray.h"


myArray::myArray()
{
	Serial.println(F("constructor myArray()"));
	length = 0;
	data = NULL;
}

myArray::myArray(uint16_t length)
{
	Serial.println(F("constructor myArray(uint16_t length)"));
	data = (uint8_t*)calloc(length, sizeof(uint8_t));
	if (data == NULL)
	{
		//ToDo: set Error
		Serial.println(F("Error: calloc not succescull: data == NULL"));
	}
	else
	{
		this->length = length;
	}
}


myArray::~myArray()
{
	Serial.println(F("destructor myArray()"));
	freeMyArray();
}

#pragma region public methods


void myArray::freeMyArray(){
	Serial.println(F("freeMyArray()"));
	if (data == NULL)
	{
		Serial.println(F("data == NULL"));
	}
	else
	{
		free(data);
		data = NULL;
		length = NULL;
	}
}

void myArray::increase(uint8_t const dataByte){
	Serial.println(F("increase(uint8_t const dataByte)"));
	
	uint16_t newLength = length + 1;
	uint8_t* newDataArray = (uint8_t*)malloc((newLength) * sizeof(uint8_t));
	
	for (uint16_t i = 0; i < newLength-1; i++)
	{
		newDataArray[i] = this->data[i];
	}
	newDataArray[newLength-1] = dataByte;

	this->freeMyArray();
	if (this->data == NULL)
	{
		this->data = newDataArray;
		this->length = newLength;
	}
	else
	{
		//ToDo: set Error
		Serial.println(F("Error: couldn't increase myArray"));
		free(newDataArray);
		newDataArray = NULL;
	}
	





	
	//myArray* newMyArray = new myArray(length+1);

	//for (uint16_t i = 0; i < length; i++)
	//{
	//	newMyArray->data[i] = this->data[i];
	//}
	//newMyArray->data[length] = dataByte;

	//this->freeMyArray();
	//
	//this->data = newMyArray->data;
	//this->length = newMyArray->length;
	//newMyArray = NULL; //same without this line

	////attention: leaks (after 193 loops)
}

void myArray::printMyArray(){
	Serial.println(F("myArray:"));
	Serial.print(F("  lenght: "));
	Serial.println(length);
	Serial.print(F("  data: "));
	myMath::print1dArray(data, length);
}


#pragma endregion public methods
