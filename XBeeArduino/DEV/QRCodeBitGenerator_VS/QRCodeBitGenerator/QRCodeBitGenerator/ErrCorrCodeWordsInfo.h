#pragma once

#ifndef ErrCorrCodeWordsInfo_H
#define	ErrCorrCodeWordsInfo_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "QRPolynom.h"
#include "QRBinom.h"
#include "myMath.h"

class ErrCorrCodeWordsInfo
{
public:
	ErrCorrCodeWordsInfo();
	~ErrCorrCodeWordsInfo();

	static const uint8_t _maxGroupCount = 2;

	enum errCorrLevel_enum {
		ecl_L = 0,
		ecl_M = 1,
		ecl_Q = 2,
		ecl_H = 3
	};

#pragma region nested_Class
	class CodeWordIndexes
	{
	public:
		CodeWordIndexes();
		CodeWordIndexes(uint8_t groupIndex, uint8_t blockIndex, uint8_t wordIndex){
			this->groupIndex = groupIndex;
			this->blockIndex = blockIndex;
			this->wordIndex = wordIndex;
		}
		uint8_t groupIndex;
		uint8_t blockIndex;
		uint8_t wordIndex;
	};
#pragma endregion nestedClass


#pragma region  GetterSetter_Methods

	uint8_t numberOfBlocksInGroup1();

	uint8_t numberOfCodeWordsInBlocksOfGroup1();
	
	uint8_t numberOfBlocksInGroup2();

	uint8_t** dataCodeWordsGroup1();
	uint8_t** dataCodeWordsGroup2();
	
	uint8_t numberOfCodeWordsInBlocksOfGroup2();
		
	uint8_t numberOfDataCodeWordsTotal();
	uint16_t numberOfNeededBitArrayLength();

	uint8_t numberOfErrCorrCodeWordsPerBlock();
	uint16_t numberOfErrCorrCodeWordsTotal();
	
	uint8_t** errCorrCodeWordsGroup1();
	uint8_t** errCorrCodeWordsGroup2();

#pragma endregion GetterSetter_Methods

#pragma region public_Methods
	void setCodeWordsInfo(uint8_t version, errCorrLevel_enum errCorrLvl);
	
	void writeDataCodeWordsToAccordingBlock(uint8_t* concatenatedBitArray, uint16_t concatenatedBitArraysLength, uint8_t _paddingByteArrayLength);

	void writeErrorCorrectionCodeWordsToAccordingBlock();

	void printDataCodeWords();
	
	void printErrCorrCodeWords();

#pragma endregion public_Methods


private:
#pragma region private_Variales
	

	uint8_t _numberOfBlocksInGroup1;
	uint8_t _numberOfDataCodeWordsInBlocksOfGroup1;
	uint8_t** _dataCodeWordsGroup1;
	uint8_t** _errCorrCodeWordsGroup1;
	
	uint8_t _numberOfBlocksInGroup2;
	uint8_t _numberOfDataCodeWordsInBlocksOfGroup2;
	uint8_t** _dataCodeWordsGroup2;
	uint8_t** _errCorrCodeWordsGroup2;

	uint8_t _numberOfDataCodeWordsTotal;
	uint16_t _numberOfNeededBitArrayLength;

	uint8_t _numberOfErrCorrCodeWordsPerBlock;
	uint16_t _numberOfErrCorrCodeWordsTotal;
#pragma endregion private_Variales

#pragma region private_Methods
	void writeByteIntoDataCodeWordBlock(uint8_t curByte, ErrCorrCodeWordsInfo::CodeWordIndexes* indexes);

	QRPolynom createGeneratorPolynomial();
	
#pragma endregion private_Variales

};

#endif