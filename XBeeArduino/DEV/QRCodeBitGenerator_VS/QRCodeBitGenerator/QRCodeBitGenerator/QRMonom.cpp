#include "QRMonom.h"

#pragma region constructors
/// <summary>
/// Initializes a new instance of the <see cref="QRMonom"/> class.
/// </summary>
/// <param name="a">a.</param>
/// <param name="x">The x.</param>
QRMonom::QRMonom(uint16_t a, uint16_t x){
	this->a = a;
	this->x = x;
}
QRMonom::QRMonom()
{
	a = 0;
	x = 0;
}

QRMonom::QRMonom(const QRMonom& sourceMonom)
{
	a = sourceMonom.a;
	x = sourceMonom.x;
}

QRMonom& QRMonom::operator= (const QRMonom& sourceMonom)
{
	if (this == &sourceMonom)
		return *this;
	
	// do the copy
	a = sourceMonom.a;
	x = sourceMonom.x;

	// return the existing object
	return *this;
}

#pragma endregion constructors

#pragma region destructors
QRMonom::~QRMonom()
{
}
#pragma endregion destructors

#pragma region getterSetter_Methods

#pragma endregion getterSetter_Methods


