#pragma once

#ifndef QRMonom_H
#define	QRMonom_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class QRMonom
{
public:
#pragma region constructors
	QRMonom();
	QRMonom(uint16_t a, uint16_t x);
	
	// copy constructor - used for objects, which not yet exist
	QRMonom(const QRMonom& sourcePolynom);
	// overload of assignment operator
	QRMonom& operator= (const QRMonom& sourceMonom);
#pragma endregion constructors

#pragma region destructors
	~QRMonom();
#pragma endregion destructors

#pragma region getterSetter_Methods
#pragma endregion getterSetter_Methods

#pragma region  public_Variables
	uint16_t a; // as alpha
	uint16_t x;
#pragma endregion public_Variables

private:

};

#endif