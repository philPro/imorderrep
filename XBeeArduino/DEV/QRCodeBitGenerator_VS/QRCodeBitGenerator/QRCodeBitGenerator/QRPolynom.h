#pragma once

#include<QRCodeBitGenerator\QRMonom.h>
#include<QRCodeBitGenerator\myMath.h>

#ifndef QRPolynom_H
#define QRPolynom_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

class QRPolynom
{
public:
	enum alphaNotation {
		alpha = 0,
		integer = 1,
	};

#pragma region constructor
	QRPolynom();
	QRPolynom(uint8_t monomCount, alphaNotation notation = alpha);
	QRPolynom(uint16_t a, uint16_t x, alphaNotation notation = alpha);
	QRPolynom(uint16_t a1, uint16_t x1, uint16_t a2, uint16_t x2, alphaNotation notation = alpha);

	// copy constructor - used for objects, which not yet exist
	QRPolynom(const QRPolynom& sourcePolynom);
	
	/// <summary>
	/// <para>overload of the assignment operator=</para>
	/// <para>1. "deletes" momory of destination object</para>
	/// <para>2. allocates memory and assignes value to destination object</para>
	/// <para>source object stays untouched (=constant)</para>
	/// </summary>
	/// <param name="sourcePolynom">The source polynom.</param>
	/// <returns></returns>
	QRPolynom& operator= (const QRPolynom& sourcePolynom);

#pragma endregion constructor  

#pragma region deconstructor
	~QRPolynom();
#pragma region deconstructor




#pragma region public_Methods

	QRPolynom& getReference();

	static void saveDelete(QRPolynom* polynom);
	
	static void printQRPolynom(const QRPolynom& polynom, const boolean printCountFlag = true);
	static void printQRPolynom(QRPolynom* polynom, const boolean printCountFlag = true);

	/// <summary>
	/// Multiplies two polynoms and returns a polynom object
	/// </summary>
	/// <param name="polynom1">The polynom1.</param>
	/// <param name="polynom2">The polynom2.</param>
	/// <param name="n">The n.</param>
	/// <returns></returns>
	static QRPolynom multiplyPolynoms(const QRPolynom& polynom1, const QRPolynom& polynom2);
	/// <summary>
	/// Checks the exponents of a's (alpha) for higher values than 255 and if true applies modulo 255.
	/// (i.e. a^257x^4 = a^(257 % 255)x^4 = a^2x^4)
	/// </summary>
	/// <param name="polynom">The polynom.</param>
	static void checkAndModExponents(QRPolynom& polynom);

	static void combineLikeTerms(QRPolynom& polynom);

	static QRPolynom xorPolynoms(QRPolynom& polynom1, QRPolynom& polynom2);
	
	static boolean discardLead0Term(QRPolynom& polynom);
	
	static void changeNotation(QRPolynom& polynom, const alphaNotation notation);

	/// <summary>
	/// returns the integer value for alphas exponent notation
	/// (i.e. a=5->32; a=7->128; a=8->29; a=13->135)
	/// </summary>
	/// <param name="a">a.</param>
	/// <returns></returns>
	static uint8_t alphaExpToInteger(uint8_t a);
	static uint8_t integerToAlphaExp(uint8_t aInt);

#pragma endregion public_Methods

#pragma region  public_Variables
	QRMonom* monoms = NULL;
	uint8_t monomCount = 0;
	alphaNotation notation;
#pragma endregion public_Variables

private:


};

#endif