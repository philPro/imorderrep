#include <EEPROM\EEPROM.h>

#pragma once

#ifndef QRCODEDRAWER_H
#define	QRCODEDRAWER_H

//#include <Adafruit_GFX.h>    // Core graphics library
//#include <Adafruit_ST7735.h> // Hardware-specific library
//#include <SPI.h>

//#include <Adafruit_GFX\Adafruit_GFX.h>
//#include <Adafruit_ST7735\Adafruit_ST7735.h>
//#include <SPI\SPI.h>

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset
// in which case, set this #define pin to 0!
#define TFT_DC     8

// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)


// Option 2: use any pins but a little slower!
#define TFT_SCLK 13   // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);



//#include "Arduino.h"
#if ARDUINO >= 100
#include "Arduino.h"
//#include "Print.h"
#else
#include "WProgram.h"
#endif

#include "myMath.h"
#include "myLayout.h"
#include "myArray.h"

#include "ErrCorrCodeWordsInfo.h"

//ToDo: remove includes in .ino file
#include <Adafruit_GFX\Adafruit_GFX.h>
#include <Adafruit_ST7735\Adafruit_ST7735.h>
#include <SPI\SPI.h>


class QRCodeDrawer
{
public:


#pragma region constructor  
	QRCodeDrawer();
	/*QRCodeDrawer(uint8_t version);
	QRCodeDrawer(uint8_t version, Layout layout);
	QRCodeDrawer(uint8_t version, Layout::horizontal horLayout, Layout::vertical vertLayout);*/
	
	QRCodeDrawer(uint8_t version, ErrCorrCodeWordsInfo::errCorrLevel_enum errCorrLvl, myLayout layout, uint16_t displayXPixelAmount, uint16_t displayYPixelAmount, uint8_t moduleSize = 0);
	
#pragma endregion constructor  

#pragma region destructor
	~QRCodeDrawer();
#pragma endregion destructor


#pragma region public_Methods
	void drawQRCode(uint8_t* finalStructuredQRbyteArray, uint16_t finalStructuredQRbyteArrayLength, uint8_t finalRemainderBitLength);
#pragma endregion public_Methods
	


private:

#pragma region nested_Class
	class MatrixIndex
	{
	public:
		MatrixIndex();

		/// <summary>
		/// Initializes a new instance of the <see cref="MatrixIndex"/> class.
		/// <para>nextModuelIndicator is set to 0 &amp; direction is set to 1 (= bottom -> top)</para>
		/// <para>nextModuelIndicator defines where next module is to be set:</para>
		/// <para>--> 0  ...  one module to the left in same row</para>
		/// <para>--> 1  ... first module in the next row (depending on direction (up: row--; down: row++)</para>
		/// </summary>
		/// <param name="rowIndex">Index of the row.</param>
		/// <param name="colIndex">Index of the col.</param>
		/// <param name="maxRowIndex">Maximum index of the row.</param>
		/// <param name="maxColIndex">Maximum index of the col.</param>
		MatrixIndex(uint8_t rowIndex, uint8_t colIndex, uint8_t maxRowIndex, uint8_t maxColIndex){
			this->rowIndex = rowIndex;
			this->colIndex = colIndex;
			this->maxRowIndex = maxRowIndex;
			this->maxColIndex = maxColIndex;
		}
		uint8_t rowIndex = 0;
		uint8_t colIndex = 0;

		uint8_t maxRowIndex = 0;
		uint8_t maxColIndex = 0;

		// defines where next module is to be set:
		// 0  ...  one module to the left in same row
		// 1  ... first module in the next row (depending on direction (up: row--; down: row++)
		int8_t nextModuleIndicator = 0;
		// direction defines in which direction the modules will be set
		// 0 ... down | 1 ... up
		int8_t direction = 1;

		/// <summary>
		/// Increases the matrix index depending on current index position, nextModuleIndicator &amp; direction
		/// </summary>
		void increaseMatrixIndex(){
			//Serial.println(F("increaseMatrixIndex()"));
			/*
			Serial.print(F("MatrixIndex before: row:"));
			Serial.print(this->rowIndex);
			Serial.print(F(" | col: "));
			Serial.println(this->colIndex);
			Serial.print(F("\tnextModuleIndicator: "));
			Serial.println(nextModuleIndicator);
			Serial.print(F("\tdirection: "));
			Serial.println(direction);
			*/
			// colIndex = 6 ... timing pattern -> skip this row
			if (colIndex == 6)
			{
				colIndex = 5;
			}		
			
			switch (nextModuleIndicator)
			{
			case 0:
				colIndex--;
				nextModuleIndicator = 1;
				break;
			case 1:
				if (rowIndex == 0)
				{
					rowIndex = (direction == 1) ? 0 : rowIndex + 1;
					colIndex = (direction == 1) ? colIndex - 1 : colIndex + 1;
					nextModuleIndicator = 0;
					direction = 0;
				}
				else if (rowIndex == maxRowIndex)
				{
					rowIndex = (direction == 1) ? rowIndex - 1 : maxRowIndex;
					colIndex = (direction == 1) ? colIndex + 1 : colIndex - 1;
					nextModuleIndicator = 0;
					direction = 1;
				}
				else if (rowIndex > 0 && rowIndex < maxRowIndex)
				{
					rowIndex = (direction == 1) ? rowIndex - 1 : rowIndex + 1;
					colIndex++;
					nextModuleIndicator = 0;
				}
				else
				{
					//ToDo: setError(
					Serial.println(F("Error: wrong rowIndex in _moduleMatrix"));
				}
				break;
			default:
				//ToDo: setError(
				Serial.println(F("Error: wrong state of nextModuleIndicator"));
				break;
			}
			/*
			Serial.print(F("MatrixIndex after: row:"));
			Serial.print(this->rowIndex);
			Serial.print(F(" | col: "));
			Serial.println(this->colIndex);
			*/
		};
	};
#pragma endregion nestedClass

#pragma region private_Variables
	
	uint8_t _version = 0; // 1 - 40 (pixel Amount))
	ErrCorrCodeWordsInfo::errCorrLevel_enum _errCorrLvl;
	uint8_t _moduleAmountPerSide; // 21, 25, 29, ..., 177
	uint8_t _moduleSize = 0;
	const uint8_t _whiteSpaceModules = 1;
	myLayout _layout;
	uint16_t _displayYPixelAmount = 0;
	uint16_t _displayXPixelAmount = 0;

	uint8_t _qrStartPosY = 0;
	uint8_t _qrStartPosX = 0;

	uint8_t** _moduleMatrix = NULL;
	
	static const uint8_t _alignmentArrayRowCount = 39; // should be 39 (versions: 2 - 40)
	static const uint8_t _alignmentArrayColCount = 7;
	//static const uint8_t _usedAlignmentRowsCount = _alignmentArrayColCount * _alignmentArrayColCount;

	// stores information (coordinates) which alignment patterns are used
	//uint8_t _usedAlignmentPatterns[_usedAlignmentRowsCount][2] = { { 0 } };
	
	// 2d Array stored as 1d array (columnCount = 2 [x,y coordinates])
	myArray _usedAlignmentPatternsMyArray = myArray();

	

#pragma endregion private_Variables

#pragma region private_Methods
	void setModulePropertiesAndMallocMatrix();
	void setModuleSize();
	void setQRStartPos();
	
	void setUsedAlignmentPatternArray();
	void validateAndSetThisAlignmentPattern(uint8_t x, uint8_t y);
	
	void setDataBytesInMatrix(uint8_t* finalStructuredQRbyteArray, uint16_t finalStructuredQRbyteArrayLength, uint8_t finalRemainderBitLength);

	void writeDataBitToModule(uint8_t bitValue, QRCodeDrawer::MatrixIndex& idx);

	boolean validateCurrentModuleIndex(const QRCodeDrawer::MatrixIndex& idx);

	void setPatternsSeperatorsAndDarkModuleToCorrectValues();

	void setFinderPatternsAndSeperatorsToCorrectValues();
	void setAlignmentPatternsToCorrectValues();
	void setTimingPatternsToCorrectValues();
	void setDarkModuleToOne();

	void dataMaskingAndEvaluation();

	void maskMatrix(uint8_t const maskNumber, uint8_t** tmpMatrix);

	uint16_t evaluateMatrix(uint8_t** const tmpMatrix);
	
	void setFormatAndVersionInfoAreasToCorrectValues(uint8_t const maskNumber);


	
	void setFormatInfoAreaToCorrectValue(uint8_t const maskNumber);

	void drawQRCodeToDisplay();

	

#pragma endregion private_Methods
};

#endif