#include "QRCodeBitGenerator.h"

// _remainderByte = 0; already set in QRCodeBitGenerator.h
// const uint8_t QRCodeBitGenerator::_remainderByte = 0;

#pragma region constructor
QRCodeBitGenerator::QRCodeBitGenerator() {}
QRCodeBitGenerator::QRCodeBitGenerator(const QRCodeBitGenerator& orig) {}

/// <summary>
/// Initializes a new instance of the <see cref="QRCodeBitGenerator"/> class.
/// </summary>
/// <param name="version">The QR Code Version.</param>
/// <param name="encMode">The encoding mode identifier.</param>
/// <param name="errCorrLvl">The error correction level enum.</param>
QRCodeBitGenerator::QRCodeBitGenerator(uint8_t version, encMode_enum encMode, ErrCorrCodeWordsInfo::errCorrLevel_enum errCorrLvl) {
	_version = version;
	_encMode = encMode;
	_errCorrLvl = errCorrLvl;
	
	setModuleAmountperSide();
	setEncModeByte();
	setCharCountIndBitArrayLength();

	setFinalRemainderBitLength();

	_ecCodeWordsInfo.setCodeWordsInfo(_version, _errCorrLvl);
}

#pragma endregion constructor

#pragma region destructor
QRCodeBitGenerator::~QRCodeBitGenerator() {
	Serial.println(F("~QRCodeBitGenerator START"));

	if (_encTextBitArrayLength > 0)
	{
		Serial.print(F("_encTextBitArray: "));
		Serial.println((uint16_t)_encTextBitArray);

		if (_encTextBitArray != NULL)
		{
			free(_encTextBitArray);
			_encTextBitArray = NULL;
			_encTextBitArrayLength = 0;
			Serial.println(F("free(_encTextBitArray) DONE"));
		}
	}
		
	if (_terminatorBitArrayLength > 0)
	{
		if (_terminatorBitArray != NULL)
		{
			free(_terminatorBitArray);
			_terminatorBitArray = NULL;
			_terminatorBitArrayLength = 0;
			Serial.println(F("free(_terminatorBitArray) DONE"));
		}
	}

	if (_addedBitForMultipleOf8ArrayLength > 0)
	{
		if (_addedBitForMultipleOf8Array != NULL)
		{
			free(_addedBitForMultipleOf8Array);
			_addedBitForMultipleOf8Array = NULL;
			_addedBitForMultipleOf8ArrayLength = 0;
			Serial.println(F("free(_addedBitForMultipleOf8Array) DONE"));
		}
	}
	
	if (_concatenatedBitArrayLength > 0)
	{
		if (_concatenatedBitArray != NULL)
		{
			free(_concatenatedBitArray);
			_concatenatedBitArray = NULL;
			_concatenatedBitArrayLength = 0;
			Serial.println(F("free(_concatenatedBitArray) DONE"));
		}
	}
		
	//free(_finalByteArray);
	//Serial.println(F("free(_finalByteArray) DONE"));
	Serial.println(F("~QRCodeBitGenerator DONE"));
}
#pragma endregion destructor

#pragma region getterSetter_Methods

/// <summary>
/// Gets the error code.
/// </summary>
/// <returns></returns>
uint8_t QRCodeBitGenerator::getErrorCode(){
	return _errorCode;
}
/// <summary>
/// Sets the error code.
/// 0...wrong encMode; 
/// 1...wrong version number; 
/// 2...wrong diff = neededBitLength - currentBitLength; 
/// 3...memory allocation gone wrong;
/// 4...wrong byte in bit array [not '0' || '1']
/// 5...optional bit array length too small for "getBinaryArrayOfDec" in myMath
/// </summary>
/// <param name="errorCode">The error code.</param>
void QRCodeBitGenerator::setErrorCode(uint8_t errorCode, char* additionalErrMsg = NULL){
	_errorCode = errorCode;
	Serial.print(F("Error with ErrorCode: "));
	Serial.print(errorCode);
	if (additionalErrMsg != NULL)
	{
		Serial.print(F(" -> \""));
		Serial.print(additionalErrMsg);
		Serial.print(F("\""));
	}
	Serial.println();
	//ToDo: somehow, via interrupt, abort procedur and display error...
	//ToDo: generalize (suggestive error codes (i.e.: 1x00... general 2x00 class specific) and outsource error handling to own class 
}

QRCodeBitGenerator::encMode_enum QRCodeBitGenerator::getEncMode(){
	return _encMode;
}
void QRCodeBitGenerator::setEncMode(QRCodeBitGenerator::encMode_enum encMode){
	_encMode = encMode;
}

uint8_t  QRCodeBitGenerator::getEncModeByte(){
	return _encModeByte;	
}
uint8_t  QRCodeBitGenerator::setModeByte(uint8_t encModeByte){
	_encModeByte = encModeByte;
}


uint8_t QRCodeBitGenerator::getPxlAmountperSide(){
	return _moduleAmountperSide;
}

uint8_t QRCodeBitGenerator::getCharCountIndLength(){
	return _charCountIndBitArrayLength;
}

#pragma endregion getterSetter_Methods

#pragma region public_Methods
/**
* final Bit Array [0 or 1] for drawing QR Code
* @return final Bit Array [0/1]
*/
uint8_t* QRCodeBitGenerator::getFinalStructuredQRbyteArrayOfText(uint8_t *text, uint8_t textLength){
	_charCountIndByte = textLength;
	
	
	encodeText(text, textLength);
	Serial.println(F("_encTextBitArray: "));
	myMath::print1dArray(_encTextBitArray, _encTextBitArrayLength);
		
	setRemainingArrays();
	
	concatenateBitArrays();
		
	/*
	Serial.print(F("_concatenatedBitArrayLength: "));
	Serial.println(_concatenatedBitArrayLength);

	Serial.print(F("_paddingByteArrayLength: "));
	Serial.println(_paddingByteArrayLength);

	Serial.println(F("_concatenatedBitArray: "));
	myMath::print1dArray(_concatenatedBitArray, _concatenatedBitArrayLength);


	Serial.print(F("_ecCodeWordsInfo.numberOfBlocksInGroup1(): "));
	Serial.println(_ecCodeWordsInfo.numberOfBlocksInGroup1());

	Serial.print(F("_ecCodeWordsInfo.numberOfBlocksInGroup2(): "));
	Serial.println(_ecCodeWordsInfo.numberOfBlocksInGroup2());
	
	Serial.print(F("_ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup1(): "));
	Serial.println(_ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup1());

	Serial.print(F("_ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup2(): "));
	Serial.println(_ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup2());*/
		
	_ecCodeWordsInfo.writeDataCodeWordsToAccordingBlock(_concatenatedBitArray, _concatenatedBitArrayLength, _paddingByteArrayLength);
	
	_ecCodeWordsInfo.printDataCodeWords();

	if (_concatenatedBitArrayLength > 0)
	{
		if (_concatenatedBitArray != NULL)
		{
			free(_concatenatedBitArray);
			_concatenatedBitArray = NULL;
			_concatenatedBitArrayLength = 0;
			Serial.println(F("free(_concatenatedBitArray) DONE"));
		}
	}

	
	//_ecCodeWordsInfo.dataCodeWordsGroup2()[1][8] = 16;
		
	_ecCodeWordsInfo.writeErrorCorrectionCodeWordsToAccordingBlock();
	
	_ecCodeWordsInfo.printErrCorrCodeWords();
		
	writeDataAndErrorCodeWordsToFinalByteArray();

	Serial.print(F("_finalByteArrayLenght: "));
	Serial.println(_finalByteArrayLenght);

	Serial.println(F("_finalByteArray: "));
	myMath::print1dArray(_finalByteArray, _finalByteArrayLenght);
	
	Serial.print(F("_finalRemainderBitLength: "));
	Serial.println(_finalRemainderBitLength);

	if (_finalByteArray == NULL)
	{
		return NULL;
	}
	else
	{
		return _finalByteArray;
	}
}


/// <summary>
/// returns the final length of the structured qr byte array
/// <para>Attention: getFinalStructuredQRbyteArrayOfText(uint8_t *text, uint8_t textLength) must be excecuted BEFORE</para>
/// </summary>
/// <returns></returns>
uint8_t QRCodeBitGenerator::getFinalStructuredQRbyteArrayLength(){
	// ToDo: create Class myArray, which also stores the length of the Array, so this information is also available for pointers
	return _finalByteArrayLenght;
}


uint8_t QRCodeBitGenerator::getFinalRemainderBitLength(){
	return _finalRemainderBitLength;
}


#pragma endregion public_Methods

#pragma region private_Methods

/// <summary>
/// Gets pixel amount per display side depending on version (1: 21px, 2: 25 px,... , 40: 177px)
/// </summary>
/// <param name="version">QR Code Version</param>
/// <returns>pixel amount</returns>
void QRCodeBitGenerator::setModuleAmountperSide(){
	_moduleAmountperSide = (_version - 1) * 4 + 21;
}

/// <summary>
/// Sets Binary Array of the Encoding Mode (always 4 Bytes, 5th Byte is for end-Char '\0' respectively NULL 
/// </summary>
/// <param name="encMode">The encoding mode.</param>
void QRCodeBitGenerator::setEncModeByte(){

	switch (_encMode){
	case QRCodeBitGenerator::em_NUMERIC:
		_encModeByte = 1;
		break;
	case QRCodeBitGenerator::em_ALPHANUMERIC:
		_encModeByte = 2;
		break;
	case QRCodeBitGenerator::em_BYTE:
		_encModeByte = 4;
		break;
	case QRCodeBitGenerator::em_KANJI:
		_encModeByte = 8;
		break;
	case QRCodeBitGenerator::em_ECI:
		_encModeByte = 7;
		break;
	default:
		setErrorCode(0);
		break;
	};
}

/// <summary>
/// defines length of the character count indicator bit array.
/// </summary>
void QRCodeBitGenerator::setCharCountIndBitArrayLength(){
	_charCountIndBitArrayLength = 0;

	switch (_encMode){
	case QRCodeBitGenerator::em_NUMERIC:
		_charCountIndBitArrayLength = 10;
		break;
	case QRCodeBitGenerator::em_ALPHANUMERIC:
		_charCountIndBitArrayLength = 9;
		break;
	case QRCodeBitGenerator::em_BYTE:
		_charCountIndBitArrayLength = 0;
		break;
	case QRCodeBitGenerator::em_KANJI:
		_charCountIndBitArrayLength = 8;
	default:
		setErrorCode(1);
	}

	if (_charCountIndBitArrayLength > 0){
		if (_version >= 1 && _version <= 9){
			_charCountIndBitArrayLength += 0;
		}
		else if (_version >= 10 && _version <= 26){
			_charCountIndBitArrayLength += 2;
		}
		else if (_version >= 27 && _version <= 40){
			_charCountIndBitArrayLength += 4;
		}
		else{
			setErrorCode(1);
		}
	}
	else{
		if (_version >= 1 && _version <= 9){
			_charCountIndBitArrayLength = 8;
		}
		else if (_version >= 10 && _version <= 26){
			_charCountIndBitArrayLength = 16;
		}
		else if (_version >= 27 && _version <= 40){
			_charCountIndBitArrayLength = 16;
		}
		else{
			setErrorCode(1);
		}
	}
}

/// <summary>
/// depending on version and error correction different encoding necessary
/// </summary>
/// <param name="text">The text to encode.</param>
/// <param name="textLength">Length of the text.</param>
void QRCodeBitGenerator::encodeText(uint8_t *text, uint8_t textLength){
	//Serial.println(F("encodeText(uint8_t *text, uint8_t textLength)"));
	switch (_encMode){
	case QRCodeBitGenerator::em_NUMERIC:
		break;
	case QRCodeBitGenerator::em_ALPHANUMERIC:
		break;
	case QRCodeBitGenerator::em_BYTE:
		encodeTextByteMode(text, textLength);
		break;
	case QRCodeBitGenerator::em_KANJI:
		break;
	case QRCodeBitGenerator::em_ECI:
		break;
	default:
		setErrorCode(0);
		break;
	}
}

/// <summary>
/// encodes *text Bytes into "_encTextBitArray" for byte mode
/// </summary>
/// <param name="text">The text.</param>
/// <param name="textLength">Length of the text.</param>
void QRCodeBitGenerator::encodeTextByteMode(uint8_t *text, uint8_t textLength){
	Serial.println(F("encodeTextByteMode(uint8_t *text, uint8_t textLength)"));

	_encTextBitArrayLength = textLength * 8;
	_encTextBitArray = (uint8_t*)malloc(_encTextBitArrayLength * sizeof(uint8_t));

	uint16_t dataIndex = 0;

	for (uint8_t textIndex = 0; textIndex < textLength; textIndex++)
	{
		uint8_t* tmpBitArray = myMath::getBinaryArrayOfDec(text[textIndex], 8);

		for (uint8_t bitArrIndex = 0; bitArrIndex < 8; bitArrIndex++)
		{
			_encTextBitArray[dataIndex] = tmpBitArray[bitArrIndex];
			dataIndex++;
		}
		free(tmpBitArray);
	}
}

/// <summary>
/// Sets the remaining arrays & lengths: _terminatorBitArrayLength, _addedBitForMultipleOf8ArrayLength , _paddingByteArrayLength
/// </summary>
void QRCodeBitGenerator::setRemainingArrays(){
	Serial.println(F("setRemainingArrays()"));
	
	uint16_t neededBitLength = _ecCodeWordsInfo.numberOfNeededBitArrayLength();
	uint16_t currentBitLength = _encModeBitLenght + _charCountIndBitArrayLength + _encTextBitArrayLength;

	uint16_t diff = neededBitLength - currentBitLength;
	
	// set _terminatorBitArrayLength (if necessary)
	if (diff >= 0 && diff <= 4){
		_terminatorBitArrayLength = diff;
	}
	else if (diff > 4){
		_terminatorBitArrayLength = 4;
	}
	else if (diff < 0){
		setErrorCode(2);
	}

	/*if (_terminatorBitArrayLength > 0)
	{
		_terminatorBitArray = (uint8_t *)malloc(_terminatorBitArrayLength);

		for (uint8_t i = 0; i < _terminatorBitArrayLength; i++)
		{
			_terminatorBitArray[i] = 0;
		}
	}*/
	currentBitLength += _terminatorBitArrayLength;
	
	diff = neededBitLength - currentBitLength;

	// add '0' bits to _addedBitForMultipleOf8ArrayLength to make the bit array length a multiple of 8 (if necessary)
	if (currentBitLength % 8 > 0)
	{
		_addedBitForMultipleOf8ArrayLength = 8 - (currentBitLength % 8);
		/*_addedBitForMultipleOf8Array = (uint8_t*)malloc(_addedBitForMultipleOf8ArrayLength);
		for (uint8_t i = 0; i < _addedBitForMultipleOf8ArrayLength; i++)
		{
			_addedBitForMultipleOf8Array[i] = 0;
		}*/
		currentBitLength += _addedBitForMultipleOf8ArrayLength;
		diff = neededBitLength - currentBitLength;
	}
	else if (currentBitLength % 8 == 0)
	{
		_addedBitForMultipleOf8ArrayLength = 0;
	}
	
	// set _padddingByteArrayLength, if bit array still smaller than required, padding bytes (236 respectively 17) are added alternately
	if (diff > 0 && diff < 8)
	{
		setErrorCode(2);
	}
	_paddingByteArrayLength = diff / 8;

	currentBitLength += _paddingByteArrayLength * 8;
	diff = neededBitLength - currentBitLength;
	// diff should be 0 now!

}

/// <summary>
/// Concatenates the arrays or bytes: _encModeByte, _charCountIndByte, _encTextBitArray, _terminatorBitArrayLength, _addedBitForMultipleOf8ArrayLength to _concatenatedBitArray
/// (_paddingByte excluded -> with length and bytes (236, 17) we got all information we need);
/// </summary>
void QRCodeBitGenerator::concatenateBitArrays(){
	Serial.println(F("concatenateBitArrays()"));
	
	_concatenatedBitArrayLength = _ecCodeWordsInfo.numberOfNeededBitArrayLength() - ((uint16_t)_paddingByteArrayLength * 8);
	Serial.print(F("_concatenatedBitArrayLength: "));
	Serial.println(_concatenatedBitArrayLength);

	// ToDo: Replace Calloc with Malloc;
	// -> just for debugging (calloc is redundant as all values will be set)
	//_concatenatedBitArray = (uint8_t*)malloc(_concatenatedBitArrayLength * sizeof(uint8_t));
	_concatenatedBitArray = (uint8_t*)calloc(_concatenatedBitArrayLength, sizeof(uint8_t));

	if (_concatenatedBitArray == NULL)
	{
		setErrorCode(3);
		Serial.println(F("_concatenatedBitArray == NULL"));
	}
	
	Serial.println(F("_concatenatedBitArray:"));
	myMath::print1dArray(_concatenatedBitArray, _concatenatedBitArrayLength);
	
	uint16_t concatIndex = 0;

	Serial.print(F("_encModeByte: "));
	Serial.println(_encModeByte);
	Serial.print(F("_encModeBitLenght: "));
	Serial.println(_encModeBitLenght);
	
	// _encModeByte
	uint8_t* tmpEncModeBitArray = myMath::getBinaryArrayOfDec(_encModeByte, _encModeBitLenght);

	for (uint8_t modeIndex = 0; modeIndex < _encModeBitLenght; modeIndex++){
		_concatenatedBitArray[concatIndex] = tmpEncModeBitArray[modeIndex];
		concatIndex++;
	}
	free(tmpEncModeBitArray);

	Serial.println(F("_concatenatedBitArray:"));
	myMath::print1dArray(_concatenatedBitArray, _concatenatedBitArrayLength);
	

	Serial.print(F("_charCountIndByte: "));
	Serial.println(_charCountIndByte);
	Serial.print(F("_charCountIndBitArrayLength: "));
	Serial.println(_charCountIndBitArrayLength);
	
	// _charCountIndByte
	uint8_t* tmpCharCountIndBitArray = myMath::getBinaryArrayOfDec(_charCountIndByte, _charCountIndBitArrayLength);

	for (uint8_t charCountIndex = 0; charCountIndex < _charCountIndBitArrayLength; charCountIndex++){
		_concatenatedBitArray[concatIndex] = tmpCharCountIndBitArray[charCountIndex];
		concatIndex++;
	}
	free(tmpCharCountIndBitArray);

	Serial.println(F("_concatenatedBitArray:"));
	myMath::print1dArray(_concatenatedBitArray, _concatenatedBitArrayLength);


	
	Serial.print(F("_encTextBitArrayLength: "));
	Serial.println(_encTextBitArrayLength);
	Serial.print(F("concatIndex: "));
	Serial.println(concatIndex);

	
	// _encTextBitArray
	for (uint16_t encDataIndex = 0; encDataIndex < _encTextBitArrayLength; encDataIndex++)
	{
		_concatenatedBitArray[concatIndex] = _encTextBitArray[encDataIndex];
		concatIndex++;
	}
	
	if (_encTextBitArrayLength > 0)
	{
		Serial.print(F("_encTextBitArray: "));
		Serial.println((uint16_t)_encTextBitArray);

		if (_encTextBitArray != NULL)
		{
			free(_encTextBitArray);
			Serial.println(F("free(_encTextBitArray) DONE"));
			_encTextBitArrayLength = 0;
			_encTextBitArray = NULL;
		}
	}
	
	Serial.println(F("_concatenatedBitArray:"));
	myMath::print1dArray(_concatenatedBitArray, _concatenatedBitArrayLength);


	Serial.print(F("_terminatorBitArrayLength: "));
	Serial.println(_terminatorBitArrayLength);
	Serial.print(F("concatIndex: "));
	Serial.println(concatIndex);
	
	// _terminatorBitArrayLength
	for (uint8_t terminatorIndex = 0; terminatorIndex < _terminatorBitArrayLength; terminatorIndex++)
	{
		_concatenatedBitArray[concatIndex] = 0; //_terminatorBitArray[terminatorIndex];
		concatIndex++;
	}
	
	Serial.println(F("_concatenatedBitArray:"));
	myMath::print1dArray(_concatenatedBitArray, _concatenatedBitArrayLength);

	Serial.print(F("_addedBitForMultipleOf8ArrayLength: "));
	Serial.println(_addedBitForMultipleOf8ArrayLength);
	Serial.print(F("concatIndex: "));
	Serial.println(concatIndex);

	// _addedBitForMultipleOf8ArrayLength
	for (uint8_t multipleOf8Index = 0; multipleOf8Index < _addedBitForMultipleOf8ArrayLength; multipleOf8Index++)
	{
		_concatenatedBitArray[concatIndex] = 0; //_addedBitForMultipleOf8Array[multipleOf8Index];
		concatIndex++;
	}
	

	Serial.print(F("_concatenatedBitArrayLength: "));
	Serial.println(_concatenatedBitArrayLength);

	Serial.print(F("concatIndex: "));
	Serial.println(concatIndex);
}


/// <summary>
/// Writes the data and error codewords to final byte array. (also sets the ramainder bit length)
/// </summary>
void QRCodeBitGenerator::writeDataAndErrorCodeWordsToFinalByteArray(){
	Serial.println(F("writeDataAndErrorCodeWordsToFinalByteArray()"));
	// data & error codewords are interleaved (if more than 1 data codeword blocks are available)
	// first: interleaved through blocks of data codewords
	// afterwards:  interleaved through blocks of error correction codewords
	// -> result: byte after byte in _finalByteArray

	_finalByteArrayLenght = _ecCodeWordsInfo.numberOfDataCodeWordsTotal() + _ecCodeWordsInfo.numberOfErrCorrCodeWordsTotal();
	//_finalByteArray = (uint8_t*)malloc(sizeof(uint8_t) * _finalByteArrayLenght);
	_finalByteArray = (uint8_t*)calloc(_finalByteArrayLenght, sizeof(uint8_t));

	uint8_t finalIndex = 0;
	uint8_t maxWordCount = (_ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup1() >= _ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup2()) ?
							_ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup1() : _ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup2();
	uint8_t maxGroupCount = _ecCodeWordsInfo._maxGroupCount;

	// dataCodeWords
	for (uint8_t wordIndex = 0; wordIndex < maxWordCount; wordIndex++){
		for (uint8_t groupIndex = 0; groupIndex < maxGroupCount; groupIndex++){

			uint8_t curGrpBlockCount = (groupIndex == 0) ? _ecCodeWordsInfo.numberOfBlocksInGroup1() : _ecCodeWordsInfo.numberOfBlocksInGroup2();
			uint8_t curGrpWordCount = (groupIndex == 0) ? _ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup1() : _ecCodeWordsInfo.numberOfCodeWordsInBlocksOfGroup2();
			uint8_t** dataCodeWords = (groupIndex == 0) ? _ecCodeWordsInfo.dataCodeWordsGroup1() : _ecCodeWordsInfo.dataCodeWordsGroup2();
			
			if (wordIndex < curGrpWordCount){
				for (uint8_t blockIndex = 0; blockIndex < curGrpBlockCount; blockIndex++){
					_finalByteArray[finalIndex] = dataCodeWords[blockIndex][wordIndex];
					finalIndex++;
				}
			}
		}
	}

	// errorCodeWords
	for (uint8_t wordIndex = 0; wordIndex < _ecCodeWordsInfo.numberOfErrCorrCodeWordsPerBlock(); wordIndex++){
		for (uint8_t groupIndex = 0; groupIndex < maxGroupCount; groupIndex++){
			uint8_t curGrpBlockCount = (groupIndex == 0) ? _ecCodeWordsInfo.numberOfBlocksInGroup1() : _ecCodeWordsInfo.numberOfBlocksInGroup2();
			uint8_t** errorCodeWords = (groupIndex == 0) ? _ecCodeWordsInfo.errCorrCodeWordsGroup1() : _ecCodeWordsInfo.errCorrCodeWordsGroup2();

			for (uint8_t blockIndex = 0; blockIndex < curGrpBlockCount; blockIndex++){
				_finalByteArray[finalIndex] = errorCodeWords[blockIndex][wordIndex];
				finalIndex++;
			}
		}
	}
	// _finalRemainderBitLength already set in Constructor
}

void QRCodeBitGenerator::setFinalRemainderBitLength(){

	if (_version >= 2 && _version <= 6)
	{
		_finalRemainderBitLength = 7;
	}
	else if ( (_version >= 14 && _version <= 20) || (_version >= 28 && _version <= 34) )
	{
		_finalRemainderBitLength = 3;
	}
	else if (_version >=21 && _version <= 27 )
	{
		_finalRemainderBitLength = 4;
	}
	else
	{
		_finalRemainderBitLength = 0;
	}
}


#pragma endregion private_Methods


