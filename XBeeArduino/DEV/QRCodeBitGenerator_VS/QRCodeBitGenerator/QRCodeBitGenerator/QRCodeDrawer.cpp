#include "QRCodeDrawer.h"

#pragma region constructor  

QRCodeDrawer::QRCodeDrawer()
{
}
/*
QRCodeDrawer::QRCodeDrawer(uint8_t version){
	_version = version;

	next steps:
	also pass size of display and wanted starting position of QR Code(perhaps do it via layout enum: left, centred, right, ...)

	setModulePropertiesAndMallocMatrix();
}

QRCodeDrawer::QRCodeDrawer(uint8_t version, Layout layout){
	_version = version;
	_layout = layout;

	setModulePropertiesAndMallocMatrix();
}

QRCodeDrawer::QRCodeDrawer(uint8_t version, Layout::horizontal horLayout, Layout::vertical vertLayout){
	_version = version;
	_layout.horizontal = horLayout;
	_layout.vertical = vertLayout;

	setModulePropertiesAndMallocMatrix();
}*/


/// <summary>
/// Initializes a new instance of the <see cref="QRCodeDrawer"/> class.
/// </summary>
/// <param name="version">The version.</param>
/// <param name="layout">The layout (i.e. hor: left;  vert: middle)</param>
/// <param name="displayYPixelAmount">The available rows pixel amount of the display (i.e. 128x160)</param>
/// <param name="displayXPixelAmount">The available columns pixel amount of the display (i.e. 128x160)</param>
/// <param name="moduleSize">Size of the module.</param>
QRCodeDrawer::QRCodeDrawer(uint8_t version, ErrCorrCodeWordsInfo::errCorrLevel_enum errCorrLvl, myLayout layout, uint16_t displayXPixelAmount, uint16_t displayYPixelAmount, uint8_t moduleSize){
	Serial.println(F("QRCodeDrawer(uint8_t version, myLayout layout, uint16_t displayYPixelAmount, uint16_t displayXPixelAmount, uint8_t moduleSize)"));
	_version = version;
	_errCorrLvl = errCorrLvl;
	_layout = layout;
	_displayYPixelAmount = displayYPixelAmount;
	_displayXPixelAmount = displayXPixelAmount;
	_moduleSize = moduleSize;
	//ToDo: maybe check if enough pixels are available
	setModulePropertiesAndMallocMatrix();
	setQRStartPos();

	/*Serial.print(F("layout: "));
	Serial.print(_layout._horizontal);
	Serial.print(F(" | "));
	Serial.println(_layout._vertical);

	Serial.print(F("_version: "));
	Serial.println(_version);

	Serial.print(F("_moduleAmountPerSide: "));
	Serial.println(_moduleAmountPerSide);

	Serial.print(F("_moduleSize: "));
	Serial.println(_moduleSize);

	Serial.print(F("_whiteSpaceModules: "));
	Serial.println(_whiteSpaceModules);
	
	Serial.print(F("_displayPixelAmount: "));
	Serial.print(_displayYPixelAmount);
	Serial.print(F(" | "));
	Serial.println(_displayXPixelAmount);

	Serial.print(F("_qrStartPos: "));
	Serial.print(_qrStartPosY);
	Serial.print(F(" | "));
	Serial.println(_qrStartPosX);*/
}

#pragma endregion constructor  


#pragma region destructor
QRCodeDrawer::~QRCodeDrawer(){
	myMath::free2dArray(_moduleMatrix, _moduleAmountPerSide);
}
#pragma endregion destructor



#pragma region public_Methods

void QRCodeDrawer::drawQRCode(uint8_t* finalStructuredQRbyteArray, uint16_t finalStructuredQRbyteArrayLength, uint8_t finalRemainderBitLength){
	Serial.println(F("drawQRCode()"));
	
		
	uint16_t lowestPenalty = 65535;
	uint8_t lowestPenaltyMaskNumber = 0;
	
	//at this point it's possible to allocate 173 bytes + 2 bytes (data + length)
	//myArray testArray = myArray(173);

	//working: 184
	//uint8_t* tmptArray = (uint8_t*)malloc(184 * sizeof(uint8_t));

	//if (tmptArray == NULL)
	//{
	//	Serial.println(F("Error: NOT successful"));
	//}
	//else
	//{
	//	Serial.println(F("Successful"));
	//}

	//free(tmptArray);
	//


	setUsedAlignmentPatternArray();

	for (uint8_t maskNumber = 0; maskNumber < 1; maskNumber++)
	{		
		Serial.print(F("maskNumber: "));
		Serial.println(maskNumber);

		// clear _moduleMatrix (set values to 0)
		for (uint8_t row = 0; row < _moduleAmountPerSide; row++)
		{
			for (uint8_t col = 0; col < _moduleAmountPerSide; col++)
			{
				_moduleMatrix[row][col] = 0;
			}
		}

		//setUsedAlignmentPatternArray();
		//Serial.println(F("setUsedAlignmentPatternArray() DONE"));

		setDataBytesInMatrix(finalStructuredQRbyteArray, finalStructuredQRbyteArrayLength, finalRemainderBitLength);
		Serial.println(F("setDataBytesInMatrix() DONE"));
	
		setPatternsSeperatorsAndDarkModuleToCorrectValues();
		Serial.println(F("setPatternsSeperatorsAndDarkModuleToCorrectValues() DONE"));
		// info: reserved Areas have to be 0 when evaluating

		//myMath::print2dArray(_moduleMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
	
		maskMatrix(maskNumber, _moduleMatrix);
		
		//myMath::print2dArray(_moduleMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
		
		uint16_t curPenaltyScore = evaluateMatrix(_moduleMatrix);
		if (curPenaltyScore <= lowestPenalty)
		{
			lowestPenalty = curPenaltyScore;
			lowestPenaltyMaskNumber = maskNumber;
		}
	}
	
	Serial.print(F("lowestPenaltyMaskNumber: "));
	Serial.println(lowestPenaltyMaskNumber);
	Serial.print(F("lowestPenalty: "));
	Serial.println(lowestPenalty);

	//maskMatrix(lowestPenaltyMaskNumber, _moduleMatrix);

	myMath::print2dArray(_moduleMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
	
	setFormatAndVersionInfoAreasToCorrectValues(lowestPenaltyMaskNumber);
	
	myMath::print2dArray(_moduleMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
	
	drawQRCodeToDisplay();
}

#pragma endregion public_Methods



#pragma region private_Methods

/// <summary>
/// <para>sets _moduleAmountPerSide (module per display side) depending on version (1: 21 modules, 2: 25 modules,... , 40: 177 modules)</para>
/// <para>- if _moduleSize = 0 (not set via constructor), function setModulSize() will be called</para>
/// <para>- callocs _moduleMatrix according to calculated module amount per side</para>
/// </summary>
/// <param name="version">QR Code Version</param>
/// <returns>pixel amount</returns>
void QRCodeDrawer::setModulePropertiesAndMallocMatrix(){
	Serial.println(F("setModulePropertiesAndMallocMatrix()"));
	_moduleAmountPerSide = (_version - 1) * 4 + 21;
	
	Serial.print(F("_moduleAmountPerSide: "));
	Serial.println(_moduleAmountPerSide);

	if (_moduleSize == 0)
	{
		setModuleSize();
	}
		
	_moduleMatrix = (uint8_t**)myMath::calloc2dArray(_moduleAmountPerSide, _moduleAmountPerSide);
	myMath::print2dArray(_moduleMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
}

/// <summary>
/// sets/calculates the size of one module.
/// </summary>
void QRCodeDrawer::setModuleSize(){
	Serial.println(F("setModuleSize()"));
	uint16_t minPxl = (_displayYPixelAmount <= _displayXPixelAmount) ? _displayYPixelAmount : _displayXPixelAmount;
	_moduleSize = minPxl / (_moduleAmountPerSide + (_whiteSpaceModules * 2));
	Serial.print(F("_moduleSize: "));
	Serial.println(_moduleSize);
}

/// <summary>
/// sets the starting position (coordinates) of the qr code
/// </summary>
void QRCodeDrawer::setQRStartPos(){
	Serial.println(F("setQRStartPos()"));

	uint16_t qrCodePxlAmountPerSide = _moduleAmountPerSide * _moduleSize; //(_moduleAmountPerSide + _whiteSpaceModules * 2) * _moduleSize;
	Serial.print(F("qrCodePxlAmountPerSide: "));
	Serial.println(qrCodePxlAmountPerSide);

	switch (_layout._vertical){
	case myLayout::top:
		_qrStartPosY = 0 + (_whiteSpaceModules * _moduleSize);
		break;
	case myLayout::middle:
		_qrStartPosY = (_displayYPixelAmount - qrCodePxlAmountPerSide) / 2;  // (_displayXPixelAmount - qrCodePxlAmountPerSide) / 2;
		break;
	case myLayout::bottom:
		_qrStartPosY = _displayYPixelAmount - qrCodePxlAmountPerSide - (_whiteSpaceModules * _moduleSize);
		break;
	default:
		//ToDo: ErrorHandling
		Serial.println(F("Error: wrong Layout..."));
		break;
	};
	
	switch (_layout._horizontal){
	case myLayout::left:
		_qrStartPosX = 0 + (_whiteSpaceModules * _moduleSize);
		break;
	case myLayout::centred:
		_qrStartPosX =   (_displayXPixelAmount - qrCodePxlAmountPerSide) / 2;
		break;
	case myLayout::right:
		_qrStartPosX = _displayXPixelAmount - qrCodePxlAmountPerSide - (_whiteSpaceModules * _moduleSize);
		break;
	default:
		//ToDo: ErrorHandling
		Serial.println(F("Error: wrong Layout..."));
		break;
	};

	Serial.print(F("_displayXPixelAmount: "));
	Serial.println(_displayXPixelAmount);
	Serial.print(F("_displayYPixelAmount: "));
	Serial.println(_displayYPixelAmount);

	Serial.print(F("qrCodePxlAmountPerSide: "));
	Serial.println(qrCodePxlAmountPerSide);

	Serial.print(F("_qrStartPosX: "));
	Serial.println(_qrStartPosX);
	Serial.print(F("_qrStartPosY: "));
	Serial.println(_qrStartPosY);


}


/// <summary>
/// <para>Sets the alignment patterns to 1</para>
/// <para>- an alignment pattern, consists of a 5x5 module black square, an inner 3x3 module white square, and a single black module in the center.</para>
/// <para>- the array defines the coordinates for the black module in the middle</para>
/// <para>- the numbers are to be used as BOTH row and column coordinates.</para>
/// <para>- i.e., version 2 has the numbers 6 and 18. This means that the CENTER MODULES of the alignment patterns are to be placed at:</para>
/// <para>--> (6, 6), (6, 18), (18, 6) and (18, 18). </para>
/// </summary>
void QRCodeDrawer::setUsedAlignmentPatternArray(){
	Serial.println(F("setUsedAlignmentPatternArray()"));
	// these are the coordinates for the black module in the middle
	
	/*
	uint8_t alignmentArray[_alignmentArrayRowCount][_alignmentArrayColCount] = {
		{ 6, 18, 0, 0, 0, 0, 0, }, // version 2
		{ 6, 22, 0, 0, 0, 0, 0, }, // version 3
		{ 6, 26, 0, 0, 0, 0, 0, }, // ...
		{ 6, 30, 0, 0, 0, 0, 0, },
		{ 6, 34, 0, 0, 0, 0, 0, },
		{ 6, 22, 38, 0, 0, 0, 0, },
		{ 6, 24, 42, 0, 0, 0, 0, },
		{ 6, 26, 46, 0, 0, 0, 0, },
		{ 6, 28, 50, 0, 0, 0, 0, },
		{ 6, 30, 54, 0, 0, 0, 0, },
		{ 6, 32, 58, 0, 0, 0, 0, },
		{ 6, 34, 62, 0, 0, 0, 0, },
		{ 6, 26, 46, 66, 0, 0, 0, },
		{ 6, 26, 48, 70, 0, 0, 0, },
		{ 6, 26, 50, 74, 0, 0, 0, },
		{ 6, 30, 54, 78, 0, 0, 0, },
		{ 6, 30, 56, 82, 0, 0, 0, },
		{ 6, 30, 58, 86, 0, 0, 0, },
		{ 6, 34, 62, 90, 0, 0, 0, },
		{ 6, 28, 50, 72, 94, 0, 0, },
		{ 6, 26, 50, 74, 98, 0, 0, },
		{ 6, 30, 54, 78, 102, 0, 0, },
		{ 6, 28, 54, 80, 106, 0, 0, },
		{ 6, 32, 58, 84, 110, 0, 0, },
		{ 6, 30, 58, 86, 114, 0, 0, },
		{ 6, 34, 62, 90, 118, 0, 0, },
		{ 6, 26, 50, 74, 98, 122, 0, },
		{ 6, 30, 54, 78, 102, 126, 0, },
		{ 6, 26, 52, 78, 104, 130, 0, },
		{ 6, 30, 56, 82, 108, 134, 0, },
		{ 6, 34, 60, 86, 112, 138, 0, },
		{ 6, 30, 58, 86, 114, 142, 0, },
		{ 6, 34, 62, 90, 118, 146, 0, },
		{ 6, 30, 54, 78, 102, 126, 150, },
		{ 6, 24, 50, 76, 102, 128, 154, },
		{ 6, 28, 54, 80, 106, 132, 158, },
		{ 6, 32, 58, 84, 110, 136, 162, }, // ...
		{ 6, 26, 54, 82, 110, 138, 166, }, // version 39
		{ 6, 30, 58, 86, 114, 142, 170, }, // version 40
	};
	
	if (_version > 1)
	{
		for (uint8_t i = 0; i < 7; i++)
		{
			uint8_t row = alignmentArray[_version - 2][i];
			for (uint8_t j = 0; j < 7; j++)
			{
				uint8_t col = alignmentArray[_version - 2][j];
				if (row > 0 && col > 0)
				{
					// ToDo: remember alignment patterns
					validateAndSetThisAlignmentPattern(row, col);
					Serial.print(F("after validateAndSetThisAlignmentPattern(row="));
					Serial.print(row);
					Serial.print(F(", col="));
					Serial.print(col);
					Serial.println(F(")"));
				}

			}
		}
	}*/

	if (_version > 1)
	{
		for (uint8_t i = 0; i < 7; i++)
		{
			uint8_t row = EEPROM.read(((_version - 2) * 7 + i));
			for (uint8_t j = 0; j < 7; j++)
			{
				uint8_t col = EEPROM.read((_version - 2) * 7 + j);
				if (row > 0 && col > 0)
				{
					validateAndSetThisAlignmentPattern(row, col);
					Serial.print(F("row: "));
					Serial.print(row);
					Serial.print(F(" | col: "));
					Serial.println(col);
				}

			}
		}
	}

	Serial.println("_usedAlignmentPatternsMyArray:");
	_usedAlignmentPatternsMyArray.printMyArray();
}


/// <summary>
/// <para>fills the array _usedAlignmentPatterns with according information (coordinates of centre (black) module)</para>
/// <para>therefore, it checks if one of the alignment pattern modules would overlap one of the finder patterns or separators</para>
/// <para>-> if not overlapping: this coordinates are being used as alignment pattern</para>
/// </summary>
/// <param name="centreRowIndex">The centre xpos.</param>
/// <param name="centreColIndex">The centre ypos.</param>
void QRCodeDrawer::validateAndSetThisAlignmentPattern(uint8_t centreRowIndex, uint8_t centreColIndex){
	Serial.println(F("validateAndSetThisAlignmentPattern(uint8_t centreRowIndex, uint8_t centreColIndex)"));
	// parameters are the coordinates of the centre (black) module
	// alignment patterns: 5x5 modules
	uint8_t alignmentStartRow = centreRowIndex - 2;
	uint8_t alignmentStartCol = centreColIndex - 2;
	
	boolean isUsed = false;

	//start from top left corner of this alignment pattern
	for (uint8_t row = alignmentStartRow; row < alignmentStartRow+5; row++)
	{
		for (uint8_t col = alignmentStartCol; col < alignmentStartCol+5; col++)
		{
			// fyi: maxRowIndex & maxColIndex are not used for this Index
			QRCodeDrawer::MatrixIndex tmpIdx(row, col, _moduleAmountPerSide - 1, _moduleAmountPerSide - 1);

			// validate every module/coordinate of this alignment pattern
			// if validation passes (returns true) this module is not in use -> isUsed = false;
			isUsed = (validateCurrentModuleIndex(tmpIdx) == true) ? false : true;
			
			if (isUsed == true)
				break;
		}
		if (isUsed == true)
			break;
	}
	// passed alignment pattern are added to the _usedAlignmentPatterns array
	if (isUsed == false)
	{
		//for (uint8_t i = 0; i < _usedAlignmentRowsCount; i++)
		//{
		//	if (_usedAlignmentPatterns[i][0] == 0 || _usedAlignmentPatterns[i][1] == 0)
		//	{
		//		_usedAlignmentPatterns[i][0] = centreRowIndex;
		//		_usedAlignmentPatterns[i][1] = centreColIndex;
		//		break;
		//	}
		//}

		//import to add always both indexes
		_usedAlignmentPatternsMyArray.increase(centreRowIndex);
		_usedAlignmentPatternsMyArray.increase(centreColIndex);
	}
}


/// <summary>
/// <para>Sets the data bytes in the matrix (values 0 or 1)</para>
/// <para>can be set independently from patterns, seperator and reserved areas</para>
/// </summary>
/// <param name="finalStructuredQRbyteArray">The final structured q rbyte array.</param>
/// <param name="finalRemainderBitLength">Final length of the remainder bit.</param>
void QRCodeDrawer::setDataBytesInMatrix(uint8_t* finalStructuredQRbyteArray, uint16_t finalStructuredQRbyteArrayLength, uint8_t finalRemainderBitLength){
	Serial.println(F("setDataBytesInMatrix()"));

	uint8_t lastModuleIndex = _moduleAmountPerSide - 1;
	QRCodeDrawer::MatrixIndex moduleIndex = QRCodeDrawer::MatrixIndex(lastModuleIndex, lastModuleIndex, lastModuleIndex, lastModuleIndex);

	for (uint8_t byteIndex = 0; byteIndex < finalStructuredQRbyteArrayLength; byteIndex++)
	{
		//Serial.print(F("byte: "));
		//Serial.println(finalStructuredQRbyteArray[byteIndex]);

		uint8_t* bitArray = myMath::getBinaryArrayOfDec(finalStructuredQRbyteArray[byteIndex], 8);

		for (uint8_t bitIndex = 0; bitIndex < 8; bitIndex++)
		{
			writeDataBitToModule(bitArray[bitIndex], moduleIndex);
		}
		free(bitArray);
	}
	/*Serial.println(F("set finalStructuredQRbyteArrayLength in Matrix DONE"));
	Serial.print(F("current Index: "));
	Serial.print(F("row: "));
	Serial.print(moduleIndex.rowIndex);
	Serial.print(F(" col: "));
	Serial.println(moduleIndex.colIndex);*/
	
	for (uint8_t i = 0; i < finalRemainderBitLength; i++)
	{
		writeDataBitToModule(0, moduleIndex);
	}
	//Serial.println(F("set finalRemainderBitLength in Matrix DONE"));
}


/// <summary>
/// writes the bit value to the matrix according to the module index
/// </summary>
/// <param name="bitValue">The bit value.</param>
/// <param name="idx">The index.</param>
void QRCodeDrawer::writeDataBitToModule(uint8_t bitValue, QRCodeDrawer::MatrixIndex& idx){
	//Serial.println(F("writeDataBitToModule()"));
	if (validateCurrentModuleIndex(idx) == true)
	{
		
		Serial.print(F("bitValue="));
		Serial.print(bitValue);
		Serial.print(F(" |    row : "));
		Serial.print(idx.rowIndex);
		Serial.print(F(" | col: "));
		Serial.println(idx.colIndex);

		_moduleMatrix[idx.rowIndex][idx.colIndex] = bitValue;
		idx.increaseMatrixIndex();
	}
	else
	{
		idx.increaseMatrixIndex();
		writeDataBitToModule(bitValue, idx);
	}
}


/// <summary>
/// validates whether the current module (index) is used by any patterns, seperators or reserved areas
/// <para>returns "true"  if idx is not being used</para>
/// <para>returns "false" if idx is already used -> index has to be increased (i.e. idx.increaseMatrixIndex())</para>
/// </summary>
/// <param name="idx">The index.</param>
/// <returns></returns>
boolean QRCodeDrawer::validateCurrentModuleIndex(const QRCodeDrawer::MatrixIndex& idx){
	boolean idxOkFlag = true;
	// finder patterns, seperators or reserved areas for the format information
	// top-left
	if (idx.rowIndex < 9 && idx.colIndex < 9)
	{
		idxOkFlag = false;
		return idxOkFlag;
	}
	// top-right
	if (idx.rowIndex < 9 && idx.colIndex > _moduleAmountPerSide - 1 - 8)
	{
		idxOkFlag = false;
		return idxOkFlag;
	}
	// bottom-left
	if (idx.rowIndex > _moduleAmountPerSide - 1 - 8 && idx.colIndex < 9)
	{
		idxOkFlag = false;
		return idxOkFlag;
	}

	// timing patterns
	if (idx.colIndex == 6 || idx.rowIndex == 6)
	{
		idxOkFlag = false;
		return idxOkFlag;
	}

	// dark module
	if ((idx.rowIndex == (4 * _version) + 9) && idx.colIndex == 8)
	{
		idxOkFlag = false;
		return idxOkFlag;
	}
	
	// alignment patterns
	// divide through 2 because it's a 2D array:
	// accessing arr[x][y] -> uint8_t tmp = arr[x * colCount + y]
	for (uint16_t i = 0; i < (_usedAlignmentPatternsMyArray.length/2); i++)
	{
		//if (_usedAlignmentPatternsMyArray.data[i][0] == 0 || _usedAlignmentPatterns[i][1] == 0)
		if (_usedAlignmentPatternsMyArray.data[i * 2 + 0] == 0 || _usedAlignmentPatternsMyArray.data[i * 2 + 1] == 0)
		{
			Serial.println(F("(_usedAlignmentPatternsMyArray.data[i * 2 + 0] == 0 || _usedAlignmentPatternsMyArray.data[i * 2 + 1] == 0)"));
			break;
		}
		else
		{
			uint8_t alPatRowStart = _usedAlignmentPatternsMyArray.data[i * 2 + 0] - 2;
			uint8_t alPatColStart = _usedAlignmentPatternsMyArray.data[i * 2 + 1] - 2;

			for (uint8_t patternRow = alPatRowStart; patternRow < alPatRowStart + 5; patternRow++)
			{
				for (uint8_t patternCol = alPatColStart; patternCol < alPatColStart + 5; patternCol++)
				{
					if (idx.rowIndex == patternRow && idx.colIndex == patternCol)
					{
						idxOkFlag = false;
						return idxOkFlag;
						break;
					}
				}
				if (idxOkFlag == false)
					break;
			}
		}
	}
	/*
	for (uint8_t i = 0; i < 49; i++)
	{
		if (_usedAlignmentPatterns[i][0] == 0 || _usedAlignmentPatterns[i][1] == 0)
		{
			break;
		}
		else
		{
			uint8_t alPatRowStart = _usedAlignmentPatterns[i][0] - 2;
			uint8_t alPatColStart = _usedAlignmentPatterns[i][1] - 2;

			for (uint8_t patternRow = alPatRowStart; patternRow < alPatRowStart + 5; patternRow++)
			{
				for (uint8_t patternCol = alPatColStart; patternCol < alPatColStart + 5; patternCol++)
				{
					if (idx.rowIndex == patternRow && idx.colIndex == patternCol)
					{
						idxOkFlag = false;
						return idxOkFlag;
						break;
					}
				}
				if (idxOkFlag == false)
					break;
			}
		}
	}
	*/
	// reserved version information areas if _version >= 7...
	if (_version >= 7)
	{
		// botom-left
		if ((idx.rowIndex < _moduleAmountPerSide - 1 - 7 && idx.rowIndex > _moduleAmountPerSide - 1 - 11) && idx.colIndex < 6)
		{
			idxOkFlag = false;
			return idxOkFlag;
		}
		// top-right
		if (idx.rowIndex < 6 && (idx.colIndex < _moduleAmountPerSide - 1 - 7 && idx.colIndex > _moduleAmountPerSide - 1 - 11))
		{
			idxOkFlag = false;
			return idxOkFlag;
		}
	}
	return idxOkFlag;
}

/// <summary>
/// <para>sets the patterns, seperators and reserved areas to correct values</para>
/// <para>can be set independently from data byte values</para>
/// </summary>
void QRCodeDrawer::setPatternsSeperatorsAndDarkModuleToCorrectValues(){
	Serial.println(F("setPatternsSeperatorsAndDarkModuleToCorrectValues()"));
	setFinderPatternsAndSeperatorsToCorrectValues();
	setAlignmentPatternsToCorrectValues();
	setTimingPatternsToCorrectValues();

	setDarkModuleToOne();
	//reserved Areas have to be 0 when evaluating
}


/// <summary>
/// sets the values of finder patterns and seperators correctly (blocks of 1's and 0's).
/// </summary>
void QRCodeDrawer::setFinderPatternsAndSeperatorsToCorrectValues(){
	Serial.println(F("setFinderPatternsAndSeperatorsToCorrectValues()"));
	//finder pattern & seperator (example for _moduleAmountPerSide = 29)

	// first reset finder patterns & seperators to 0
	for (uint8_t r = 0; r < 8; r++)
	{
		for (uint8_t c = 0; c < 8; c++)
		{
			_moduleMatrix[r][c] = 0; // top-left
			_moduleMatrix[_moduleAmountPerSide - 1 - r][c] = 0; // bottom-left
			_moduleMatrix[r][_moduleAmountPerSide - 1 - c] = 0; // top-right
		}
	}

	// set designated modules to one
	for (uint8_t i = 0; i < 8; i++)
	{
		//top-left finder pattern
		if (i < 7)
		{
			// top-left finder pattern
			_moduleMatrix[0][i] = 1;
			_moduleMatrix[6][i] = 1;
			_moduleMatrix[i][0] = 1;
			_moduleMatrix[i][6] = 1;
						
			// bottom-left finder pattern
			_moduleMatrix[_moduleAmountPerSide-1][i] = 1; // i.e. rowIndex: 28
			_moduleMatrix[_moduleAmountPerSide - 1 - 6][i] = 1; // i.e. rowIndex: 22
			_moduleMatrix[_moduleAmountPerSide - 1 - i][0] = 1;
			_moduleMatrix[_moduleAmountPerSide - 1 - i][6] = 1;

			// top-right finder pattern
			_moduleMatrix[0][_moduleAmountPerSide - 1 - i] = 1;
			_moduleMatrix[6][_moduleAmountPerSide - 1 - i] = 1;
			_moduleMatrix[i][_moduleAmountPerSide - 1] = 1;
			_moduleMatrix[i][_moduleAmountPerSide - 1 - 6] = 1;

		}
	}

	// finder pattern: set black block of 9 (3x3) in the middle
	for (uint8_t r = 0; r < 3; r++)
	{
		for (uint8_t c = 0; c < 3; c++)
		{
			_moduleMatrix[r+2][c+2] = 1; // top-left
			_moduleMatrix[_moduleAmountPerSide - 1 - 2 - r][c + 2] = 1; // bottom-left
			_moduleMatrix[r + 2][_moduleAmountPerSide - 1 - 2 - c] = 1; // top-right
		}
	}
}

/// <summary>
/// sets the values of the alignment patterns correctly (blocks of 1's and 0's).
/// </summary>
void QRCodeDrawer::setAlignmentPatternsToCorrectValues(){
	Serial.println(F("setAlignmentPatternsToCorrectValues()"));
	for (uint8_t i = 0; i < 49; i++)
		for (uint8_t i = 0; i < (_usedAlignmentPatternsMyArray.length/2); i++)
	{
		//uint8_t centreRowIndex = _usedAlignmentPatterns[i][0];
		//uint8_t centreColIndex = _usedAlignmentPatterns[i][1];

		uint8_t centreRowIndex = _usedAlignmentPatternsMyArray.data[i * 2 + 0];
		uint8_t centreColIndex = _usedAlignmentPatternsMyArray.data[i * 2 + 1];

		// if values in _usedAlignmentPatterns are = 0, no more alignment patterns are used
		if(centreRowIndex == 0 || centreColIndex == 0)
			break;

		/*Serial.print(F("centreRowIndex: "));
		Serial.print(centreRowIndex);
		Serial.print(F(" | centreColIndex: "));
		Serial.println(centreColIndex);*/

		// reset values of current alignment pattern to 0
		for (uint8_t r = 0; r < 5; r++)
		{
			for (uint8_t c = 0; c < 5; c++)
			{
				_moduleMatrix[centreRowIndex - 2 + r][centreColIndex - 2 + c] = 0;
			}	
		}

		// set centre module to 1
		_moduleMatrix[centreRowIndex][centreColIndex] = 1;

		// set "outer" module to 1
		for (uint8_t j = 0; j < 5; j++)
		{
			_moduleMatrix[centreRowIndex - 2][centreColIndex - 2 + j] = 1;
			_moduleMatrix[centreRowIndex + 2][centreColIndex - 2 + j] = 1;

			_moduleMatrix[centreRowIndex - 2 + j][centreColIndex - 2] = 1;
			_moduleMatrix[centreRowIndex - 2 + j][centreColIndex + 2] = 1;
		}
	}
}

/// <summary>
/// sets the value of the timing patterns correctly (alternating 1 and 0).
/// </summary>
void QRCodeDrawer::setTimingPatternsToCorrectValues(){

	// colomn 6 respectively row 6 between the sperators
	for (uint8_t i = 8; i < _moduleAmountPerSide - 1 - 7; i++)
	{
		_moduleMatrix[i][6] = (i % 2 == 0) ? 1 : 0;
		_moduleMatrix[6][i] = (i % 2 == 0) ? 1 : 0;
	}
}

void QRCodeDrawer::setDarkModuleToOne(){
	Serial.println(F("setDarkModuleToOne()"));
	// dark module -> coordinates are always: ([(4 * _version) + 9], 8)
	_moduleMatrix[(4 * _version) + 9][8] = 1;
}

void QRCodeDrawer::dataMaskingAndEvaluation(){
	Serial.println(F("dataMaskingAndEvaluation()"));
	
	uint16_t lowestPenalty = 0;
	uint8_t lowestPenaltyMaskNumber = 0;
	
	//uint8_t** tmpMatrix = (uint8_t**)myMath::calloc2dArray(_moduleAmountPerSide, _moduleAmountPerSide);
	uint8_t* tmpMatrix = (uint8_t*)myMath::calloc2dArrayAs1dArray(_moduleAmountPerSide, _moduleAmountPerSide);

	myMath::print1dArrayAs2dArray(tmpMatrix, _moduleAmountPerSide, _moduleAmountPerSide);

	for (uint8_t row = 0; row < _moduleAmountPerSide; row++)
	{
		for (uint8_t col = 0; col < _moduleAmountPerSide; col++)
		{
			//tmpMatrix[row*_moduleAmountPerSide + col] = _moduleMatrix[row][col];
			//tmpMatrix[row][col] = 9; // _moduleMatrix[row][col];
		}
	}
	myMath::print1dArrayAs2dArray(tmpMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
	/*
	for (uint8_t maskNumber = 0; maskNumber < 8; maskNumber++)
	{
		maskMatrix(maskNumber, tmpMatrix);
		myMath::print2dArray(tmpMatrix, _moduleAmountPerSide, _moduleAmountPerSide);
	}
	*/

	//myMath::free2dArray(tmpMatrix, _moduleAmountPerSide);
	
	free(tmpMatrix);
	Serial.println(F("dataMaskingAndEvaluation() DONE"));
}


void QRCodeDrawer::maskMatrix(uint8_t const maskNumber, uint8_t** tmpMatrix){
	Serial.print(F("maskMatrix() -> maskNumber: "));
	Serial.println(maskNumber);
	uint8_t lastModuleIndex = _moduleAmountPerSide - 1;
	// 	if the formula below is true for a given row/column coordinate, switch the bit at that coordinate: 0->1 | 1->0
	QRCodeDrawer::MatrixIndex tmpIdx(lastModuleIndex, lastModuleIndex, lastModuleIndex, lastModuleIndex);
	
	boolean stepOutFlag = false;
	switch (maskNumber)
	{
	case 0: //(row + column) mod 2 == 0
		do
		{
			if ((validateCurrentModuleIndex(tmpIdx) == true) && ((tmpIdx.rowIndex + tmpIdx.colIndex) % 2 == 0))
			{
				tmpMatrix[tmpIdx.rowIndex][tmpIdx.colIndex] = !(tmpMatrix[tmpIdx.rowIndex][tmpIdx.colIndex]);
			}

			if (tmpIdx.rowIndex == lastModuleIndex  && tmpIdx.colIndex == 0)
			{
				stepOutFlag = true;
			}
			tmpIdx.increaseMatrixIndex();

		} while (stepOutFlag == false);
		break;
	default:
		break;
	}
}


/// <summary>
/// <para>evaluates the matrix and returns its penalty score</para>
/// <para>the Four Penalty Rules:</para>
/// <para>the first rule gives the QR code a penalty for each group of five or more same - colored modules in a row(or column).</para>
/// <para>the second rule gives the QR code a penalty for each 2x2 area of same - colored modules in the matrix.</para>
/// <para>the third rule gives the QR code a large penalty if there are patterns that look similar to the finder patterns.</para>
/// <para>the fourth rule gives the QR code a penalty if more than half of the modules are dark or light, with a larger penalty for a larger difference.</para>
/// </summary>
/// <returns></returns>
uint16_t QRCodeDrawer::evaluateMatrix(uint8_t** const matrix){
	Serial.println(F("evaluateMatrix(uint8_t** const matrix)"));

	myMath::print2dArray(matrix, _moduleAmountPerSide, _moduleAmountPerSide);

	uint16_t horizontalScore = 0;
	uint16_t verticalScore = 0;
	uint16_t penaltyScore = 0;

	// first rule
	for (uint8_t idx1 = 0; idx1 < _moduleAmountPerSide; idx1++)
	{
		uint8_t rowPrevValue = 0;
		uint8_t rowScore = 0;
		uint8_t rowConsecutivelyCount = 0;

		uint8_t colPrevValue = 0;
		uint8_t colScore = 0;
		uint8_t colConsecutivelyCount = 0;

		for (uint8_t idx2 = 0; idx2 < _moduleAmountPerSide; idx2++)
		{
			//Serial.print(F("idx2: "));
			//Serial.println(idx2);
			//Serial.print(F("   rowPrevValue: "));
			//Serial.println(rowPrevValue);
			//Serial.print(F("   rowConsecutivelyCount: "));
			//Serial.println(rowConsecutivelyCount);
			//Serial.print(F("   rowScore: "));
			//Serial.println(rowScore);

			// counting  row's penalty score
			if (rowConsecutivelyCount == 0)
			{
				rowConsecutivelyCount = 1;
			}
			else
			{
				if (rowPrevValue == matrix[idx1][idx2])
				{
					rowConsecutivelyCount++;

					if (idx2 + 1 == _moduleAmountPerSide)
					{
						if (rowConsecutivelyCount >= 5)
						{
							rowScore += rowConsecutivelyCount - 2;
						}
						rowConsecutivelyCount = 1;
					}
				}
				else
				{
					if (rowConsecutivelyCount >= 5)
					{
						rowScore += rowConsecutivelyCount - 2;
					}
					rowConsecutivelyCount = 1;
				}
			}
			rowPrevValue = matrix[idx1][idx2];

			// counting  columns's penalty score
			if (colConsecutivelyCount == 0)
			{
				colConsecutivelyCount = 1;
			}
			else
			{
				if (colPrevValue == matrix[idx2][idx1])
				{
					colConsecutivelyCount++;

					if (idx2 + 1 == _moduleAmountPerSide)
					{
						if (colConsecutivelyCount >= 5)
						{
							colScore += colConsecutivelyCount - 2;
						}
						colConsecutivelyCount = 1;
					}
				}
				else
				{
					if (colConsecutivelyCount >= 5)
					{
						colScore += colConsecutivelyCount - 2;
					}
					colConsecutivelyCount = 1;
				}
			}
			colPrevValue = matrix[idx2][idx1];


		}

		/*Serial.print(F("Index "));
		Serial.println(idx1);
		Serial.print(F("rowscore: "));
		Serial.println(rowScore);
		Serial.print(F("colscore: "));
		Serial.println(colScore);*/

		horizontalScore += rowScore;
		verticalScore += colScore;
	}

	/*Serial.print(F("horizontalScore: "));
	Serial.println(horizontalScore);
	Serial.print(F("verticalScore: "));
	Serial.println(verticalScore);*/

	penaltyScore = horizontalScore + verticalScore;

	//ToDo: add other penalty scores from other 3 evaluation processes
	

	/*Serial.print(F("returned penalty Score:"));
	Serial.println(penaltyScore);*/
	return penaltyScore;
}



/// <summary>
/// <para>Sets the format and version (if _version >=7) information areas to correct values.</para>
/// <para>calls:</para>
/// <para>- setFormatInfoAreaToCorrectValue(maskNumber);</para>
/// </summary>
/// <param name="maskNumber">The mask number.</param>
void QRCodeDrawer::setFormatAndVersionInfoAreasToCorrectValues(uint8_t const maskNumber){
	Serial.println(F("setFormatAndVersionInfoAreasToCorrectValues()"));
	setFormatInfoAreaToCorrectValue(maskNumber);

	//ToDo: set reserved version information area when _version >=7
	// areas (2x 6x3 blocks) for version information have already been reserved (modules stay 0) earlier when _version >= 7
	if (_version >= 7)
	{
		uint8_t startIndex = _moduleAmountPerSide - 11;
		for (uint8_t indexUntil3 = startIndex; indexUntil3 < startIndex + 3; indexUntil3++)
		{
			for (uint8_t indexUntil6 = 0; indexUntil6 < 6; indexUntil6++)
			{
				_moduleMatrix[indexUntil3][indexUntil6] = 1;
				_moduleMatrix[indexUntil6][indexUntil3] = 1;
			}
		}
	}
}

void QRCodeDrawer::setFormatInfoAreaToCorrectValue(uint8_t const maskNumber){
	Serial.println(F("setFormatInfoAreaToCorrectValue()"));

	uint16_t formatInfoByte;
	uint8_t formatInfoLenght = 15;
	uint8_t* formatInfoBitArray;

	// format information bit string
	// ToDo: calculate formatInfoByte
	if (_errCorrLvl == ErrCorrCodeWordsInfo::ecl_L && maskNumber == 0)
	{
		formatInfoByte = 30660; //BIN: 111011111000100
	}
	else if (_errCorrLvl == ErrCorrCodeWordsInfo::ecl_Q && maskNumber == 0)
	{
		formatInfoByte = 13663;

	}

	formatInfoBitArray = myMath::getBinaryArrayOfDec(formatInfoByte, formatInfoLenght);

	Serial.println(F("formatInfoBitArray; 0->15:"));
	for (uint8_t i = 0; i < 15; i++)
	{
		Serial.print(formatInfoBitArray[i]);
		if (i + 1 == 15)
			Serial.println();
	}

	myMath::switchArrValues(formatInfoBitArray, formatInfoLenght);
	
	Serial.println(F("formatInfoBitArray; 0->15:"));
	for (uint8_t i = 0; i < 15; i++)
	{
		Serial.print(formatInfoBitArray[i]);
		if (i + 1 == 15)
			Serial.println();
	}
	

	for (int8_t i = 0; i < formatInfoLenght; i++)
	{
		if (i <= 7)
		{
			// row 8 right part of format information
			_moduleMatrix[8][_moduleAmountPerSide - 1 - i] = formatInfoBitArray[i];
			// col 8 top part of format information
			_moduleMatrix[(i <= 5) ? i : i + 1][8] = formatInfoBitArray[i];
		}

		if (i>=8)
		{
			// row 8 left part of format information
			_moduleMatrix[8][(i == 8) ? 7 : formatInfoLenght - 1 - i] = formatInfoBitArray[i];
			// col 8 bottom part of format information
			_moduleMatrix[_moduleAmountPerSide - 1 - (formatInfoLenght - 1 - i)][8] = formatInfoBitArray[i];
		}
	}

	free(formatInfoBitArray);
}



void QRCodeDrawer::drawQRCodeToDisplay(){
	Serial.println(F("drawQRCodeToDisplay(): "));
	Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);
	tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab
	tft.fillScreen(ST7735_WHITE);

	Serial.print(F("_qrStartPosY: "));
	Serial.println(_qrStartPosY);
	Serial.print(F("_qrStartPosX: "));
	Serial.println(_qrStartPosX);
	Serial.print(F("_moduleSize: "));
	Serial.println(_moduleSize);

	for (uint8_t row = 0; row < _moduleAmountPerSide; row++)
	{
		uint8_t curPosY = _qrStartPosY + (row * _moduleSize);

		for (uint8_t col = 0; col < _moduleAmountPerSide; col++)
		{
			uint8_t curPosX = _qrStartPosX + (col * _moduleSize);

			tft.fillRect(curPosX, curPosY, _moduleSize, _moduleSize, (_moduleMatrix[row][col] == 1) ? ST7735_BLACK : ST7735_WHITE);
		}
	}

	
}



#pragma endregion private_Methods