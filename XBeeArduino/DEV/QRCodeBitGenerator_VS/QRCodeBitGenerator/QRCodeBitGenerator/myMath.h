#ifndef myMath_H
#define	myMath_H

#if ARDUINO >= 100
#include "Arduino.h"
//#include "Print.h"
#else
#include "WProgram.h"
#endif

#pragma once
class myMath
{
public:
	myMath();
	~myMath();

	static uint8_t* uint8_tcpy(uint8_t *dest, uint8_t *source);

	static uint8_t  uint8_ArrLength(uint8_t *array);

	/// <summary>
	/// <para>gets the binary array of a decimal number</para>
	/// <para>CAUTION: the returned array has to be freed by the user as this function allocates the necessary memory!</para>
	/// <para>(only returns neccessary amount of bits by default, if a specific array lenght is needed: set the optArrLength)</para>
	/// <para>if optArrLength is bigger than the needed array length for the decimal  number, the remaining bytes are padded to the left and filled with value '0'</para>
	/// <para>if optArrLength is smaller than the needed array length for the decimal number, the optArrLength will be ignored and an array with the needed length is returned (i.e. 5 --> 1001)</para>
	/// </summary>
	/// <param name="decimalNumber">The decimal number.</param>
	/// <param name="optArrLength">optional array length (used for '0' padding the remaining bytes (MSB)).</param>
	/// <returns></returns>
	static uint8_t* getBinaryArrayOfDec(uint16_t const decimalNumber, uint8_t const optArrLength);
	static void switchArrValues(uint8_t* arr, uint8_t const length);

	static void** malloc2dArray(uint8_t const rowCount, uint8_t const colCount);
	static void** calloc2dArray(uint8_t const rowCount, uint8_t const colCount);
	
	static void free1dArray(uint8_t* array, uint8_t length);
	static void free2dArray(uint8_t** array, uint8_t rowCount);

	static void* malloc2dArrayAs1dArray(uint8_t const rowCount, uint8_t colCount);
	static void* calloc2dArrayAs1dArray(uint8_t const rowCount, uint8_t colCount);
		
	
	static int16_t myIntExp(int16_t const base, const int16_t exp);

	static void print1dArray(uint8_t* const array1D, uint16_t const count);

	static void print2dArray(uint8_t** const array2D, uint8_t const rowCount, uint8_t const colCount);

	static void print1dArrayAs2dArray(uint8_t* const array1D, uint8_t const rowCount, uint8_t const colCount);

};

#endif