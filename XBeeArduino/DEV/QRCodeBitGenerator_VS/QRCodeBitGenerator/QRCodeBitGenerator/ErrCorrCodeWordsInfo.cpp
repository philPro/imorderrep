#include "ErrCorrCodeWordsInfo.h"

ErrCorrCodeWordsInfo::ErrCorrCodeWordsInfo()
{
}


ErrCorrCodeWordsInfo::~ErrCorrCodeWordsInfo()
{

	myMath::free2dArray(_dataCodeWordsGroup1, _numberOfBlocksInGroup1);
	myMath::free2dArray(_errCorrCodeWordsGroup1, _numberOfBlocksInGroup1);
	Serial.println(F("free2dArray(_dataCodeWordsGroup1) & free2dArray(_errCorrCodeWordsGroup1); DONE"));

	myMath::free2dArray(_dataCodeWordsGroup2, _numberOfBlocksInGroup2);
	myMath::free2dArray(_errCorrCodeWordsGroup2, _numberOfBlocksInGroup2);
	Serial.println(F("free2dArray(_dataCodeWordsGroup2) & free2dArray(_errCorrCodeWordsGroup2); DONE"));
}

uint8_t ErrCorrCodeWordsInfo::numberOfBlocksInGroup1(){
	return _numberOfBlocksInGroup1;
}

uint8_t ErrCorrCodeWordsInfo::numberOfCodeWordsInBlocksOfGroup1(){
	return _numberOfDataCodeWordsInBlocksOfGroup1;
}


uint8_t ErrCorrCodeWordsInfo::numberOfBlocksInGroup2(){
	return _numberOfBlocksInGroup2;
}


uint8_t** ErrCorrCodeWordsInfo::dataCodeWordsGroup1(){
	return _dataCodeWordsGroup1;
}

uint8_t** ErrCorrCodeWordsInfo::dataCodeWordsGroup2(){
	return _dataCodeWordsGroup2;
}

uint8_t** ErrCorrCodeWordsInfo::errCorrCodeWordsGroup1(){
	return _errCorrCodeWordsGroup1;
}

uint8_t** ErrCorrCodeWordsInfo::errCorrCodeWordsGroup2(){
	return _errCorrCodeWordsGroup2;
}

uint8_t ErrCorrCodeWordsInfo::numberOfCodeWordsInBlocksOfGroup2(){
	return _numberOfDataCodeWordsInBlocksOfGroup2;
}

uint8_t ErrCorrCodeWordsInfo::numberOfDataCodeWordsTotal(){
	return _numberOfDataCodeWordsTotal;
}

uint16_t ErrCorrCodeWordsInfo::numberOfNeededBitArrayLength(){
	return _numberOfNeededBitArrayLength;
}

uint8_t ErrCorrCodeWordsInfo::numberOfErrCorrCodeWordsPerBlock(){
	return _numberOfErrCorrCodeWordsPerBlock;

}
uint16_t ErrCorrCodeWordsInfo::numberOfErrCorrCodeWordsTotal(){
	return _numberOfErrCorrCodeWordsTotal;
}

/// <summary>
/// Sets the code words information of a QR-Code bit generator
/// </summary>
/// <param name="version">The version.</param>
/// <param name="errCorrLvl">The error corr level.</param>
void ErrCorrCodeWordsInfo::setCodeWordsInfo(uint8_t version, errCorrLevel_enum errCorrLvl){
	//ToDo: perhaps include more versions/ErrorCorrectionLevel (data from fixed table)
	
	if (version == 2 && errCorrLvl == ecl_L){ // max 32 byte text
		_numberOfBlocksInGroup1 = 1;
		_numberOfDataCodeWordsInBlocksOfGroup1 = 34;
		_numberOfBlocksInGroup2 = 0;
		_numberOfDataCodeWordsInBlocksOfGroup2 = 0;
		_numberOfErrCorrCodeWordsPerBlock = 10;
	}
	else if (version == 3 && errCorrLvl == ecl_L){ // max 53 byte text
		_numberOfBlocksInGroup1 = 1;
		_numberOfDataCodeWordsInBlocksOfGroup1 = 55;
		_numberOfBlocksInGroup2 = 0;
		_numberOfDataCodeWordsInBlocksOfGroup2 = 0;
		_numberOfErrCorrCodeWordsPerBlock = 15;
	}
	else if (version == 5 && errCorrLvl == ecl_Q) // max 60 byte text
	{
		_numberOfBlocksInGroup1 = 2;
		_numberOfDataCodeWordsInBlocksOfGroup1 = 15;
		_numberOfBlocksInGroup2 = 2;
		_numberOfDataCodeWordsInBlocksOfGroup2 = 16;
		_numberOfErrCorrCodeWordsPerBlock = 18;
	}
	else
	{
		//ToDo:setError
		Serial.println(F("wrong version or error correction level"));
	}

	_numberOfDataCodeWordsTotal = (_numberOfBlocksInGroup1 * _numberOfDataCodeWordsInBlocksOfGroup1) + (_numberOfBlocksInGroup2 * _numberOfDataCodeWordsInBlocksOfGroup2);
	_numberOfNeededBitArrayLength = _numberOfDataCodeWordsTotal * 8;

	_numberOfErrCorrCodeWordsTotal = (_numberOfBlocksInGroup1 + _numberOfBlocksInGroup2) * _numberOfErrCorrCodeWordsPerBlock;

	_dataCodeWordsGroup1 = (uint8_t**)myMath::malloc2dArray(_numberOfBlocksInGroup1, _numberOfDataCodeWordsInBlocksOfGroup1);
	_dataCodeWordsGroup2 = (uint8_t**)myMath::malloc2dArray(_numberOfBlocksInGroup2, _numberOfDataCodeWordsInBlocksOfGroup2);

	_errCorrCodeWordsGroup1 = (uint8_t**)myMath::malloc2dArray(_numberOfBlocksInGroup1, _numberOfErrCorrCodeWordsPerBlock);
	_errCorrCodeWordsGroup2 = (uint8_t**)myMath::malloc2dArray(_numberOfBlocksInGroup2, _numberOfErrCorrCodeWordsPerBlock);

}


/// <summary>
/// distributes the encoded data (concatenated bit array and padding bytes) to according group/block/dataCodeWord.
/// </summary>
/// <param name="concatenatedBitArray">The concatenated bit array.</param>
/// <param name="concatenatedBitArraysLength">Length of the concatenated bit arrays.</param>
/// <param name="paddingByteArrayLength">Length of the padding byte array.</param>
void ErrCorrCodeWordsInfo::writeDataCodeWordsToAccordingBlock(uint8_t* concatenatedBitArray, uint16_t concatenatedBitArraysLength, uint8_t paddingByteArrayLength){
	// defines current index/position where to store next byte
	ErrCorrCodeWordsInfo::CodeWordIndexes* indexes = new ErrCorrCodeWordsInfo::CodeWordIndexes(0, 0, 0);
	// current byte to be stored in one of the code word blocks
	uint8_t curByte = 0;

	// concatenated bit array {0100000 10011011 ...} -> iterate through array
	for (uint16_t concBitIndex = 0; concBitIndex < concatenatedBitArraysLength; concBitIndex++)
	{
		// build byte of next 8 bits
		if (((concBitIndex + 1) % 8) == 0)
		{
			// after adding the 7th bit the byte is complete and can be written to the appropriate data code word block
			curByte += concatenatedBitArray[concBitIndex]; // ((concatenatedBitArray[concBitIndex] == '1') ? 1 : 0);
			writeByteIntoDataCodeWordBlock(curByte, indexes);
			curByte = 0;
		}
		else
		{
			// bits have to be shifted to their actual 2^n value (i.e.: '10001000': the second '1' (@ index 4) = 2^3 = 8
			curByte += (concatenatedBitArray[concBitIndex]) << (7 - (concBitIndex % 8)); // ((concatenatedBitArray[concBitIndex] == '1') ? 1 : 0) << (7 - (concBitIndex % 8));
		}
	}

	// padding bytes (236, 17) -> via paddingbyteArraylength, we know how much bytes to add alternately
	for (uint8_t i = 0; i < paddingByteArrayLength; i++){
		if (i % 2 == 0){
			writeByteIntoDataCodeWordBlock(236, indexes);
		}
		else{
			writeByteIntoDataCodeWordBlock(17, indexes);
		}
	}
	delete indexes;
}

/// <summary>
/// Writes the byte into the according group/block/dataCodeWord
/// </summary>
/// <param name="curByte">The current byte.</param>
/// <param name="indexes">The indexes.</param>
void ErrCorrCodeWordsInfo::writeByteIntoDataCodeWordBlock(uint8_t curByte, ErrCorrCodeWordsInfo::CodeWordIndexes* indexes){
	if (indexes->groupIndex >= _maxGroupCount)
	{
		//ToDo: setError
	}
	uint8_t** dataCodeWordsGroup = (indexes->groupIndex == 0) ? _dataCodeWordsGroup1 : _dataCodeWordsGroup2;
	
	uint8_t numberOfBlocks = (indexes->groupIndex == 0) ? _numberOfBlocksInGroup1 : _numberOfBlocksInGroup2;
	uint8_t numberOfCodeWords = (indexes->groupIndex == 0) ? _numberOfDataCodeWordsInBlocksOfGroup1 : _numberOfDataCodeWordsInBlocksOfGroup2;

	dataCodeWordsGroup[indexes->blockIndex][indexes->wordIndex] = curByte;

	// increase index (if i.e. wordIndex exceeds it range, next index (blockIndex) has to be increased)
	indexes->wordIndex++;

	if (indexes->wordIndex ==numberOfCodeWords)
	{
		indexes->wordIndex = 0;
		indexes->blockIndex++;
	}
	if (indexes->blockIndex == numberOfBlocks)
	{
		indexes->blockIndex = 0;
		indexes->groupIndex++;
	}
}


/// <summary>
/// prints the data codewords.
/// </summary>
void ErrCorrCodeWordsInfo::printDataCodeWords(){
	
	Serial.println(F("\nDataCodeWord[GROUP][BLOCK][WORD]:"));
	for (uint8_t groupIndex = 0; groupIndex < _maxGroupCount; groupIndex++)
	{
		Serial.print(F("groupIndex:"));
		Serial.println(groupIndex);
		uint8_t** dataCodeWordsGroup = (groupIndex == 0) ? _dataCodeWordsGroup1 : _dataCodeWordsGroup2;

		uint8_t numberOfBlocks = (groupIndex == 0) ? _numberOfBlocksInGroup1 : _numberOfBlocksInGroup2;
		uint8_t numberOfCodeWords = (groupIndex == 0) ? _numberOfDataCodeWordsInBlocksOfGroup1 : _numberOfDataCodeWordsInBlocksOfGroup2;
		for (uint8_t blockIndex = 0; blockIndex < numberOfBlocks; blockIndex++)
		{
			for (uint8_t wordIndex = 0; wordIndex < numberOfCodeWords; wordIndex++)
			{
				Serial.print(F("DataCodeWord["));
				Serial.print(groupIndex);
				Serial.print(F("]["));
				Serial.print(blockIndex);
				Serial.print(F("]["));
				Serial.print(wordIndex);
				Serial.print(F("]: "));
				Serial.println(dataCodeWordsGroup[blockIndex][wordIndex]);
			}
		}
	}
	Serial.println();
}


/// <summary>
/// Writes the error correction code words to according block.
/// </summary>
void ErrCorrCodeWordsInfo::writeErrorCorrectionCodeWordsToAccordingBlock(){
	Serial.println(F("writeErrorCorrectionCodeWordsToAccordingBlock()"));
	QRPolynom generatorPolynom(1);
	generatorPolynom = createGeneratorPolynomial();
	
	
	Serial.println(F("generatorPolynom"));
	QRPolynom::printQRPolynom(generatorPolynom);
	

	// create error correction code words for every group
	for (uint8_t groupIndex = 0; groupIndex < _maxGroupCount; groupIndex++)
	{
		Serial.print(F("groupIndex: "));
		Serial.println(groupIndex);
	
		// depending on group, different errCorrCodeWordsGroup, dataCodeWordsGroup, numberOfBlocksInGroup, numberOfDataCodeWordsInBlocks
		uint8_t** errCorrCodeWordsGroup = (groupIndex == 0) ? _errCorrCodeWordsGroup1 : _errCorrCodeWordsGroup2;
		uint8_t** dataCodeWordsGroup = (groupIndex == 0) ? _dataCodeWordsGroup1 : _dataCodeWordsGroup2;
	
		uint8_t numberOfBlocksInGroup = (groupIndex == 0) ? _numberOfBlocksInGroup1 : _numberOfBlocksInGroup2;
		uint8_t numberOfDataCodeWordsInBlocks = (groupIndex == 0) ? _numberOfDataCodeWordsInBlocksOfGroup1 : _numberOfDataCodeWordsInBlocksOfGroup2;

		/*
		Serial.print(F("numberOfBlocksInGroup: "));
		Serial.println(numberOfBlocksInGroup);
		Serial.print(F("numberOfDataCodeWordsInBlocks: "));
		Serial.println(numberOfDataCodeWordsInBlocks);
		*/

	for (uint8_t blockIndex = 0; blockIndex < numberOfBlocksInGroup; blockIndex++)
		{
			Serial.print(F("blockIndex: "));
			Serial.println(blockIndex);
			// original message polynomial has to be generated for every block
			// -> the data codewords build the (alpha) coefficients
			QRPolynom messagePolynom = QRPolynom(numberOfDataCodeWordsInBlocks, QRPolynom::integer);
	
			for (uint8_t wordIndex = 0; wordIndex < numberOfDataCodeWordsInBlocks; wordIndex++)
			{
				messagePolynom.monoms[wordIndex].a = dataCodeWordsGroup[blockIndex][wordIndex];
				messagePolynom.monoms[wordIndex].x = (numberOfDataCodeWordsInBlocks - (wordIndex + 1)) + _numberOfErrCorrCodeWordsPerBlock;
				//  + _numberOfErrCorrCodeWordsPerBlock ... in order to ensure exp of the lead term doesn't becom too small
			}
			// at this point the messagePolynom for the current block is ready for calculation, a values are stored as integer form
			// at this point the tmpgeneratorPolynom is ready for calculation, a values are stored as alpha notation form

			
			Serial.println("messagePolynom");
			QRPolynom::printQRPolynom(messagePolynom);
			Serial.println("generatorPolynom");
			QRPolynom::printQRPolynom(generatorPolynom);
			
			Serial.print(F("numberOfDataCodeWordsInBlocks: "));
			Serial.println(numberOfDataCodeWordsInBlocks);
					
			
			// n division steps; n = number of terms in message polynomial = number for data code words in blocks of GroupX
			for (uint8_t n = 1; n <= numberOfDataCodeWordsInBlocks; n++)
			{
				
				Serial.print(F("division steps; step n = "));
				Serial.println(n);
				

				// the lead term of the tmp generator polynomial must have the same (x) exponent
				uint8_t diff = messagePolynom.monoms[0].x - generatorPolynom.monoms[0].x;
				//Serial.print(F("diff = "));
				//Serial.println(diff);
	
				QRPolynom tmpGeneratorPolynom = QRPolynom(1);
				tmpGeneratorPolynom = QRPolynom::multiplyPolynoms(generatorPolynom, QRPolynom(0, diff, QRPolynom::alpha));

				//Serial.println(F("tmpGeneratorPolynom"));
				//QRPolynom::printQRPolynom(tmpGeneratorPolynom);
	
				// step a) multiply tmp generator polynom by lead term of result polynomial (message polynomial in step 1) -> only a value
				tmpGeneratorPolynom = QRPolynom::multiplyPolynoms(tmpGeneratorPolynom, QRPolynom(messagePolynom.monoms[0].a, 0, messagePolynom.notation));
									
				//QRPolynom::changeNotation(tmpGeneratorPolynom, QRPolynom::integer);
	
				/*
				Serial.println(F("messagePolynom"));
				QRPolynom::printQRPolynom(messagePolynom);
				Serial.println(F("tmpGeneratorPolynom"));
				QRPolynom::printQRPolynom(tmpGeneratorPolynom);
				*/
					
				// step b) xor result from step a (new generator polynomial) with result from previous loop result (new message polynomial)
				messagePolynom = QRPolynom::xorPolynoms(messagePolynom, tmpGeneratorPolynom);
								
				Serial.print(F("n = "));
				Serial.print(n);
				Serial.println(F(" | messagePolynom after xorPolynoms(messagePolynom, tmpGeneratorPolynom)"));
				QRPolynom::printQRPolynom(messagePolynom);

				boolean leadTermDiscarded = QRPolynom::discardLead0Term(messagePolynom);
				if (leadTermDiscarded == true)
				{
					n++;
					Serial.print(F("n = "));
					Serial.print(n);
					Serial.println(F(" | messagePolynom after discardLead0Term(messagePolynom)"));
					QRPolynom::printQRPolynom(messagePolynom);
				}
				
				
			} //for (uint8_t n = 1; n <= numberOfDataCodeWordsInBlocks; n++)
	
			if (messagePolynom.monomCount != _numberOfErrCorrCodeWordsPerBlock)
			{
				//ToDo: setError
				Serial.println(F("messagePolynom.monomCount != _numberOfErrCorrCodeWordsPerBlock"));
				Serial.print(F("   -> wrong number of final messagePolynom.monomCount for block "));
				Serial.println(blockIndex);
			}
			else
			{
				//Serial.println(F("write alpha values from final messagePolynom to according wordIndexes of _errCorrCodeWordsGroup"));
				// write alpha values from final messagePolynom to according wordIndexes of _errCorrCodeWordsGroup (group 1 respectively 2)
				for (uint8_t wordIndex = 0; wordIndex < _numberOfErrCorrCodeWordsPerBlock; wordIndex++)
				{
					errCorrCodeWordsGroup[blockIndex][wordIndex] = messagePolynom.monoms[wordIndex].a;
				}
			}
		} //for (uint8_t blockIndex = 0; blockIndex < numberOfBlocksInGroup; blockIndex++)
	} // for (uint8_t groupIndex = 0; groupIndex < _maxGroupCount; groupIndex++)
}

QRPolynom ErrCorrCodeWordsInfo::createGeneratorPolynomial(){
	Serial.println(F("createGeneratorPolynomial()"));
	// Base Polynomial (a^0 * x^1  +  a^n * x^0), n ... multiplication step
	// -> n = _numberOfErrCorrCodeWordsPerBlock - 1; i.e. _numberOfErrCorrCodeWordsPerBlock = 15 --> n = 14

	QRPolynom basePolynom = QRPolynom(0, 1, 0, 0);
	
	
	// for 15 error correction code words 14 multiplication steps are needed -> 1 to 14
	for (uint8_t n = 1; n < _numberOfErrCorrCodeWordsPerBlock; n++)
	{
		
		//Serial.print(F("\nloop in createGeneratorPolynomial: n="));
		//Serial.print(n);
		//Serial.print(F("; _numberOfErrCorrCodeWordsPerBlock="));
		//Serial.println(_numberOfErrCorrCodeWordsPerBlock);
				
		QRPolynom multiplyPolynom = QRPolynom(0, 1, n, 0);

		basePolynom = QRPolynom::multiplyPolynoms(basePolynom, multiplyPolynom);

		//QRPolynom::checkAndModExponents(basePolynom);
		//QRPolynom::combineLikeTerms(basePolynom);
		/*
		Serial.println(F("basePolynom after combineLikeTerms(basePolynom);"));
		QRPolynom::printQRPolynom(basePolynom);
		*/
	}
	return basePolynom;
}


/// <summary>
/// prints the error correction codewords
/// </summary>
void ErrCorrCodeWordsInfo::printErrCorrCodeWords(){
		Serial.println(F("ErrorCorrection CodeWords[GROUP][BLOCK][ErrCorrWORD]:"));
	for (uint8_t groupIndex = 0; groupIndex < _maxGroupCount; groupIndex++)
	{
		// depending on group, different errCorrCodeWordsGroup, dataCodeWordsGroup, numberOfBlocksInGroup, numberOfDataCodeWordsInBlocks
		uint8_t** errCorrCodeWordsGroup = (groupIndex == 0) ? _errCorrCodeWordsGroup1 : _errCorrCodeWordsGroup2;

		uint8_t numberOfBlocksInGroup = (groupIndex == 0) ? _numberOfBlocksInGroup1 : _numberOfBlocksInGroup2;
		uint8_t numberOfDataCodeWordsInBlocks = (groupIndex == 0) ? _numberOfDataCodeWordsInBlocksOfGroup1 : _numberOfDataCodeWordsInBlocksOfGroup2;
		//_numberOfErrCorrCodeWordsPerBlock

		for (uint8_t blockIndex = 0; blockIndex < numberOfBlocksInGroup; blockIndex++)
		{
			for (uint8_t wordIndex = 0; wordIndex < _numberOfErrCorrCodeWordsPerBlock; wordIndex++)
			{
				Serial.print(F("ErrorCorrection CodeWords["));
				Serial.print(groupIndex);
				Serial.print(F("]["));
				Serial.print(blockIndex);
				Serial.print(F("]["));
				Serial.print(wordIndex);
				Serial.print(F("]: "));
				Serial.println(errCorrCodeWordsGroup[blockIndex][wordIndex]);
			}
		}
	}
	Serial.println();
}