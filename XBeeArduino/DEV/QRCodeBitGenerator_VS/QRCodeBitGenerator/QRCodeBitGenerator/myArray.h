#ifndef myArray_H
#define	myArray_H

#include "myMath.h"

#if ARDUINO >= 100
#include "Arduino.h"
//#include "Print.h"
#else
#include "WProgram.h"
#endif

#pragma once
class myArray
{
public:
	myArray();
	myArray(uint16_t length);
	~myArray();

#pragma region  public_Variables

	void increase(uint8_t const dataByte);
	
	void freeMyArray();

	void printMyArray();

#pragma region  public_Variables
	uint16_t length = 0;
	uint8_t* data = NULL;
#pragma endregion public_Variables

private:
};
#endif