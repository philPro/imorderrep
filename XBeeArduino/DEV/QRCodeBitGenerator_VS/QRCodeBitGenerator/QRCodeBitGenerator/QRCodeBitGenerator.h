#pragma once

#ifndef QRCODEBITGENERATOR_H
#define	QRCODEBITGENERATOR_H

//#include <cstdlib>
////#include <iostream>
//#include <stdint.h>

//#include "Arduino.h"
#if ARDUINO >= 100
#include "Arduino.h"
//#include "Print.h"
#else
#include "WProgram.h"
#endif

#include "myMath.h"
#include "ErrCorrCodeWordsInfo.h"

class QRCodeBitGenerator
{
public:

	//enum errCorrLevel_enum {
	//	ecl_L = 0,
	//	ecl_M = 1,
	//	ecl_Q = 2,
	//	ecl_H = 3
	//};
	enum encMode_enum {
		em_NUMERIC = 0,
		em_ALPHANUMERIC = 1,
		em_BYTE = 2,
		em_KANJI = 3,
		em_ECI = 4
	};

#pragma region constructor  
	QRCodeBitGenerator();
	QRCodeBitGenerator(const QRCodeBitGenerator& orig);
	QRCodeBitGenerator(uint8_t version, encMode_enum encMode, ErrCorrCodeWordsInfo::errCorrLevel_enum errorCorrectionLevel);

#pragma endregion constructor  


#pragma region destructor
	~QRCodeBitGenerator();
#pragma endregion destructor


#pragma region getterSetter_Methods
	
	uint8_t getErrorCode();
	void setErrorCode(uint8_t errorCode, char* additionalErrMsg);

	uint8_t getVersion();
	void setVersion(uint8_t version);

	encMode_enum getEncMode();
	void setEncMode(encMode_enum modeIndicatorId);

	ErrCorrCodeWordsInfo::errCorrLevel_enum getErrCorrLvl();
	void setErrorCorrectionLevel(ErrCorrCodeWordsInfo::errCorrLevel_enum errorCorrectionLevel);

	uint8_t getPxlAmountperSide();

	uint8_t getEncModeByte();
	uint8_t setModeByte(uint8_t encModeByte);

	uint8_t getCharCountIndLength();
		
#pragma endregion getterSetter_Methods
	
#pragma region public_Methods

	uint8_t* getFinalStructuredQRbyteArrayOfText(uint8_t *text, uint8_t textLength);
	uint8_t getFinalStructuredQRbyteArrayLength();
	uint8_t getFinalRemainderBitLength();
	
#pragma endregion public_Methods

private:
#pragma region private_Variables
	uint8_t _errorCode;

	uint8_t _version; // 1 - 40 (pixel Amount))
	uint8_t _moduleAmountperSide; // 21, 25, ..., 177

	ErrCorrCodeWordsInfo::errCorrLevel_enum _errCorrLvl; // L, M, Q, H

	encMode_enum _encMode; // 0 ... Numeric | 1 ... AlphaNumeric | 2 ... Byte | 3 ... Kanji | 4 ... ECI;
	
	uint8_t _encModeByte = 0;
	const uint8_t _encModeBitLenght = 4;

	uint8_t _charCountIndByte = 0;
	uint8_t _charCountIndBitArrayLength = 0;
	
	ErrCorrCodeWordsInfo _ecCodeWordsInfo;

	uint16_t _encTextBitArrayLength = 0;
	uint8_t *_encTextBitArray = NULL;

	//-> handled via TextBitArray as with different encoding Modes it is necessary to store the text bytes as bit
	// i.e. alphanumeric mode with text "HELLO":
	// H=17, E=14; -> (45 * 17) + 14 = 779
	// 11 - bit binary string: 779 -> 01100001011
	// L=21, L=21; -> (45 * 21) + 21 = 966 --> 01111000110
	// if encoding text length is odd, final character converted to a 6 - bit binary string:
	// O=24; -> 011000

	uint8_t _terminatorByte = 0;
	uint8_t* _terminatorBitArray = NULL;
	uint8_t _terminatorBitArrayLength = 0;

	uint8_t* _addedBitForMultipleOf8Array = NULL;
	uint8_t _addedBitForMultipleOf8ArrayLength = 0;

	uint8_t _paddingByteArrayLength = 0;

	uint8_t* _concatenatedBitArray = NULL;
	uint16_t _concatenatedBitArrayLength = 0;

	uint8_t _finalByteArrayLenght = 0;
	uint8_t* _finalByteArray = NULL;

	uint8_t _finalRemainderBitLength = 0;
	static const uint8_t _remainderByte = 0;

#pragma endregion private_Variables 

#pragma region private_Methods
	void setModuleAmountperSide();
	
	void setEncModeByte();

	void setCharCountIndBitArrayLength();
	
	void encodeText(uint8_t *text, uint8_t textLength);

	void encodeTextByteMode(uint8_t *text, uint8_t textLength);
		
	void setRemainingArrays();

	void concatenateBitArrays();

	void writeDataAndErrorCodeWordsToFinalByteArray();

	void setFinalRemainderBitLength();

#pragma endregion private_Methods



};

#endif