#include "QRPolynom.h"

#pragma region constructor
QRPolynom::QRPolynom()
{
	//Serial.println(F("constructor: QRPolynom()"));
}

QRPolynom::QRPolynom(uint8_t monomCount, alphaNotation notation){
	//Serial.println(F("constructor: QRPolynom(uint8_t monomCount)"));
	this->monomCount = monomCount;
	monoms = new QRMonom[monomCount]();

	if (monoms == NULL)
	{
		Serial.println(F("Error: QRPolynom(): monoms == NULL"));
		//ToDo: setError
	}
	this->notation = notation;
}

QRPolynom::QRPolynom(uint16_t a, uint16_t x, alphaNotation notation){
	//Serial.println(F("constructor: QRPolynom(uint16_t a, uint16_t x)"));
	monomCount = 1;
	monoms = new QRMonom[1]();
	if (monoms == NULL)
	{
		Serial.println(F("Error: QRPolynom(): monoms == NULL"));
		//ToDo: setError
	}
	monoms[0].a = a;
	monoms[0].x = x;
	this->notation = notation;
}

QRPolynom::QRPolynom(uint16_t a1, uint16_t x1, uint16_t a2, uint16_t x2, alphaNotation notation){
	//Serial.println(F("constructor: QRPolynom(uint16_t a1, uint16_t x1, uint16_t a2, uint16_t x2)"));
	monomCount = 2;
	monoms = new QRMonom[monomCount]();
	if (monoms == NULL)
	{
		Serial.println(F("Error: QRPolynom(): monoms == NULL"));
		//ToDo: setError
	}
	monoms[0].a = a1;
	monoms[0].x = x1;
	monoms[1].a = a2;
	monoms[1].x = x2;	
	this->notation = notation;
}

/// <summary>
/// copy constructor: initializes a new instance of the <see cref="QRPolynom"/> class.
/// <para>copy constructor - used for objects, which not yet exist</para>
/// </summary>
/// <param name="sourcePolynom">The source polynom.</param>
QRPolynom::QRPolynom(const QRPolynom& sourcePolynom)
{
	//Serial.println("constructor: QRPolynom(const QRPolynom& sourcePolynom)");
	monomCount = sourcePolynom.monomCount;
	notation = sourcePolynom.notation;

	if (sourcePolynom.monoms != NULL)
	{
		monoms = new QRMonom[monomCount];

		for (uint8_t i = 0; i < monomCount; i++)
		{
			monoms[i] = sourcePolynom.monoms[i];
		}
	}
	else
	{
		monoms = NULL;
	}
}
#pragma endregion constructor  

#pragma region destructor
QRPolynom::~QRPolynom()
{
	/*
	Serial.println(F("destructor: ~QRPolynom"));
	printQRPolynom(this);
	*/
	monomCount = 0;
	delete[] monoms;
	monoms = NULL;
}
#pragma endregion destructor

#pragma region public_Methods


QRPolynom& QRPolynom::getReference(){
	return *this;
}


/// <summary>
/// <para>overload of the assignment operator=</para>
/// <para>1. "deletes" momory of destination object</para>
/// <para>2. allocates memory and assignes value to destination object</para>
/// <para>source object stays untouched (=constant)</para>
/// </summary>
/// <param name="sourcePolynom">The source polynom.</param>
/// <returns></returns>
QRPolynom& QRPolynom::operator= (const QRPolynom& sourcePolynom)
{
	//Serial.println(F("operator=: QRPolynom::operator="));
	if (this == &sourcePolynom)
		return *this;

	// monomCount is no pointer, so can be shallow copied
	monomCount = sourcePolynom.monomCount;
	delete[] monoms;

	if (sourcePolynom.monoms != NULL)
	{
		monoms = new QRMonom[monomCount];
		for (uint8_t i = 0; i < monomCount; i++)
		{
			monoms[i] = sourcePolynom.monoms[i];
		}
	}
	else
	{
		monoms = NULL;
	}

	// return the existing object
	return *this;
}


void QRPolynom::saveDelete(QRPolynom* polynom){
	//Serial.println(F("QRPolynom::saveDelete"));

	delete polynom;
	polynom = NULL;
}


/// <summary>
/// Prints the QRPolynomial information (i.e. polynom.monomCount: 4 /next line: "a^0*x^2 + a^1*x^1 + a^0*x^1 + a^1*x^0")
/// </summary>
/// <param name="polynom">The polynom.</param>
void QRPolynom::printQRPolynom(const QRPolynom& polynom, boolean printCountFlag){
	Serial.println(F("QRPolynom::printQRPolynom(QRPolynom& polynom, boolean printCount)"));

	if (printCountFlag == true)
	{
		Serial.print(F("   polynom.monomCount: "));
		Serial.println(polynom.monomCount);
	}

	Serial.print(F("   polynom.notation: "));
	Serial.println(polynom.notation);

	Serial.print(F("   "));
	for (uint8_t i = 0; i < polynom.monomCount; i++)
	{
		if (polynom.notation == alpha)
		{
			Serial.print(F("a^"));
		}
		
		Serial.print(polynom.monoms[i].a);
		Serial.print(F("*x^"));
		Serial.print(polynom.monoms[i].x);
		(i + 1 < polynom.monomCount) ? Serial.print(F(" + ")) : Serial.println();
	}
	if (polynom.monomCount == 0)
		Serial.println();
}



/// <summary>
/// Prints the QRPolynomial information (i.e. polynom.monomCount: 4 /next line: "a^0*x^2 + a^1*x^1 + a^0*x^1 + a^1*x^0")
/// </summary>
/// <param name="polynom">The polynom.</param>
void QRPolynom::printQRPolynom(QRPolynom* polynom, boolean printCountFlag){
	Serial.println(F("QRPolynom::printQRPolynom(QRPolynom* polynom, boolean printCountFlag)"));

	if (polynom != NULL)
	{
		QRPolynom::printQRPolynom(*polynom, printCountFlag);
	}
	else
	{
		Serial.println(F("   polynom == NULL"));
	}
}


/// <summary>
/// Multiplies two polynoms and returns a polynom object.
/// (if needed a values a are converted to alpha notation before multiplying -> returned polynom always has alpha notation)
/// </summary>
/// <param name="polynom1">The polynom1.</param>
/// <param name="polynom2">The polynom2.</param>
/// <param name="n">The n.</param>
/// <returns></returns>
QRPolynom QRPolynom::multiplyPolynoms(const QRPolynom& polynom1, const QRPolynom& polynom2){
	/*Serial.println(F("multiplyPolynoms(polynom1, polynom2)"));*/
	
	// first multiplication:		(a0x1 + a0x0) by (a0x1 + a1x0)
	// first multiplication result: (a0x1 * a0x1) + (a0x0 * a0x1) + (a0x1 * a1x0) + (a0x0 * a1x0)
	// To multiply, you add the exponents together like this:
	// (a(0 + 0)x(1 + 1)) + (a(0 + 0)x(0 + 1)) + (a(0 + 1)x(1 + 0)) + (a(0 + 1)x(0 + 0))
	// After adding the exponents, this is the result :
	// a0x2 + a0x1 + a1x1 + a1x0 


	// multiply and add exponents, but like terms are not combined yet (check for exponents higher than 255 needed before)
	// i.e. (a^0x^1 + a^0x^0) by (a^0x^1 + a^1x^0)
	// -> (a^0x^1) * (a^0x^1) = a^0x^2
	// -> (a^0x^1) * (a^1x^0) = a^1x^1
	// -> (a^0x^0) * (a^0x^1) = a^0x^1
	// -> (a^0x^0) * (a^1x^0) = a^1x^0
	
	QRPolynom resultPolynom = QRPolynom(polynom1.monomCount * polynom2.monomCount, alpha);

	uint8_t resultTermIndex = 0;
	for (uint8_t pol1Index = 0; pol1Index < polynom1.monomCount; pol1Index++)
	{
		for (uint8_t pol2Index = 0; pol2Index < polynom2.monomCount; pol2Index++)
		{
			uint16_t pol1_a = (polynom1.notation == alpha) ? polynom1.monoms[pol1Index].a : QRPolynom::integerToAlphaExp(polynom1.monoms[pol1Index].a);
			uint16_t pol2_a = (polynom2.notation == alpha) ? polynom2.monoms[pol2Index].a : QRPolynom::integerToAlphaExp(polynom2.monoms[pol2Index].a);

			//resultPolynom.monoms[resultTermIndex].a = polynom1.monoms[pol1Index].a + polynom2.monoms[pol2Index].a;
			resultPolynom.monoms[resultTermIndex].a = pol1_a + pol2_a;
			resultPolynom.monoms[resultTermIndex].x = polynom1.monoms[pol1Index].x + polynom2.monoms[pol2Index].x;
			resultTermIndex++;
		}
	}

	checkAndModExponents(resultPolynom);
	combineLikeTerms(resultPolynom);

	return resultPolynom;
}


/// <summary>
/// Checks the exponents of a's (alpha) for higher values than 255 and if true applies modulo 255.
/// (i.e. a^257x^4 = a^(257 % 255)x^4 = a^2x^4)
/// </summary>
/// <param name="polynom">The polynom.</param>
void QRPolynom::checkAndModExponents(QRPolynom& polynom){
	//Serial.println(F("checkAndModExponents(QRPolynom& polynom)"));

	for (uint8_t i = 0; i < polynom.monomCount; i++)
	{
		if (polynom.monoms[i].a > 255)
		{
			polynom.monoms[i].a %= 255;
		}
	}
}


/// <summary>
/// Combines the like terms (i.e. a^0*x^1 + a^1*x^1);
/// 1. using alpha->integer conversion: 1*x^1 + 2*x^1
/// 2. XOR integers: (1^2)x^1 = 3*x^1
/// 3. using integer->alpha conversion: 3*x^1 -> a^25*x^1
/// </summary>
/// <param name="basePolynom">The base polynom.</param>
void QRPolynom::combineLikeTerms(QRPolynom& basePolynom){
	//Serial.println(F("combineLikeTerms(QRPolynom& basePolynom)"));
	uint8_t combineCount = 0;

	for (uint8_t i = 0; i < basePolynom.monomCount; i++)
	{
		for (uint8_t k = i + 1; k < basePolynom.monomCount; k++)
		{
			if (basePolynom.monoms[i].x == basePolynom.monoms[k].x)
			{
				combineCount++;
				break;
			}
		}
	}
	if (combineCount > 0)
	{
		/*Serial.print(F("combineCount: "));
		Serial.println(combineCount);*/

		QRPolynom combinedPolynom = QRPolynom(basePolynom.monomCount - combineCount, integer);
		// curCombIndex ... describes next "unused" index in combinedPolynom
		uint8_t curCombIndex = 0;
		
		for (uint8_t baseIndex = 0; baseIndex < basePolynom.monomCount; baseIndex++)
		{
			for (uint8_t combIndex = 0; combIndex < combinedPolynom.monomCount; combIndex++)
			{
				// go through polynoms
				// -> set/add value of a to combinedPolynom index
				// ---> either to already written value of x (same exp of x) or to curCombIndex (unused so far) -> raised if written to curCombIndex
				// ---> before adding, a value is converted to integer form
				if (combIndex == curCombIndex)
				{
					combinedPolynom.monoms[combIndex].x = basePolynom.monoms[baseIndex].x;
					combinedPolynom.monoms[combIndex].a = (basePolynom.notation == integer) ? basePolynom.monoms[baseIndex].a : QRPolynom::alphaExpToInteger(basePolynom.monoms[baseIndex].a);
					//combinedPolynom.monoms[combIndex].a = QRPolynom::alphaExpToInteger(basePolynom.monoms[baseIndex].a);
					curCombIndex++;
					break;
				}
				if (basePolynom.monoms[baseIndex].x == combinedPolynom.monoms[combIndex].x)
				{
					combinedPolynom.monoms[combIndex].a ^= (basePolynom.notation == integer) ? basePolynom.monoms[baseIndex].a : QRPolynom::alphaExpToInteger(basePolynom.monoms[baseIndex].a);
					//combinedPolynom.monoms[combIndex].a ^= QRPolynom::alphaExpToInteger(basePolynom.monoms[baseIndex].a);
					break;
				}

			}
		}
		// at this point the combining is is done, but a values are still stored as integer form -> have to be re-converted to a (alpha) form
		QRPolynom::changeNotation(combinedPolynom, alpha);

		//for (uint8_t i = 0; i < combinedPolynom.monomCount; i++)
		//{
		//	combinedPolynom.monoms[i].a = QRPolynom::integerToAlphaExp(combinedPolynom.monoms[i].a);
		//}
		//combinedPolynom.notation = alpha;
		
		basePolynom = combinedPolynom;
	}
	else //else: no combinable monoms available -> polynomial remains the same
	{
		//Serial.println(F("else: no combinable monoms available -> polynomial remains the same"));
	}
}


QRPolynom QRPolynom::xorPolynoms(QRPolynom& polynom1, QRPolynom& polynom2){
	//Serial.println(F("xorPolynoms(const QRPolynom& polynom1, const QRPolynom& polynom2)"));
	
	//QRPolynom tmpPol1 = (polynom1.monomCount >= polynom2.monomCount) ? QRPolynom(polynom1) : QRPolynom(polynom2);
	//QRPolynom tmpPol2 = (polynom1.monomCount >= polynom2.monomCount) ? QRPolynom(polynom2) : QRPolynom(polynom1);
		
	QRPolynom& tmpPol1 = (polynom1.monomCount >= polynom2.monomCount) ? polynom1 : polynom2;
	QRPolynom& tmpPol2 = (polynom1.monomCount >= polynom2.monomCount) ? polynom2 : polynom1;

	if (tmpPol1.notation == alpha)
		changeNotation(tmpPol1, integer);

	if (tmpPol2.notation == alpha)
		changeNotation(tmpPol2, integer);
	
	/*
	Serial.println(F("tmpPol1"));
	printQRPolynom(tmpPol1);
	Serial.println(F("tmpPol2"));
	printQRPolynom(tmpPol2);
	*/
		
	// restultPolynom: erstes Monom f�llt immer weg, da dessen Alpha nach dem Xor a=0 betr�gt.
	QRPolynom resultPolynom = QRPolynom((tmpPol1.monomCount - 1), integer);

	uint8_t curResultIndex = 0;
	
	for (uint8_t polIndex1 = 0; polIndex1 < tmpPol1.monomCount; polIndex1++)
	{
		for (uint8_t polIndex2 = 0; polIndex2 < tmpPol2.monomCount; polIndex2++)
		{
			if (tmpPol1.monoms[polIndex1].x == tmpPol2.monoms[polIndex2].x)
			{
				// if first xored value == 0, this value can be ignored (used for first term ONLY)
				if ((((tmpPol1.monoms[polIndex1].a ^ tmpPol2.monoms[polIndex2].a) == 0)) && polIndex1 == 0){
					break;
				}
				
				resultPolynom.monoms[curResultIndex].x = tmpPol1.monoms[polIndex1].x;
				resultPolynom.monoms[curResultIndex].a = (tmpPol1.monoms[polIndex1].a ^ tmpPol2.monoms[polIndex2].a);
				curResultIndex++;
				break;
			}
			if (polIndex2 == (tmpPol2.monomCount - 1))
			{
				resultPolynom.monoms[curResultIndex].x = tmpPol1.monoms[polIndex1].x;
				resultPolynom.monoms[curResultIndex].a = tmpPol1.monoms[polIndex1].a;
				curResultIndex++;
				break;
			}
		}
	}

	//QRPolynom::discardLead0Term(resultPolynom);
	
	return resultPolynom;
}


/// <summary>
/// <para>discards the lead term of the polynom if its a-value = 0</para>
/// <para>return true/false (if discard was performed: true; if discard wasn't performed: false</para>
/// </summary>
/// <param name="polynom">The polynom.</param>
/// <returns></returns>
boolean QRPolynom::discardLead0Term(QRPolynom& polynom){
	Serial.println(F("QRPolynom::discardLead0Term(QRPolynom& polynom)"));

	boolean leadTermDiscarded = false;

	if (polynom.monoms[0].a == 0)
	{
		Serial.println(F("leadTerm: a == 0 -> discard..."));
		//Serial.println(F("polynom.monoms[0].a == 0"));
		QRPolynom tmpPolynom = QRPolynom((polynom.monomCount - 1), polynom.notation);

		/*
		Serial.print(F("polynom.monomCount: "));
		Serial.println(polynom.monomCount);
		*/
		for (uint8_t i = 1; i < polynom.monomCount; i++)
		{
			tmpPolynom.monoms[(i - 1)].a = polynom.monoms[i].a;
			tmpPolynom.monoms[(i - 1)].x = polynom.monoms[i].x;
		}
		polynom = tmpPolynom;
		leadTermDiscarded = true;
	}
	else
	{
		Serial.println(F("leadTerm: a != 0"));
		leadTermDiscarded = false;
		/*
		Serial.println(F("discardLead0Term(QRPolynom& polynom): polynom.monoms[0].a != 0"));
		Serial.print(F("polynom.monoms[0].a = "));
		Serial.println(polynom.monoms[0].a);
		*/
	}

	return leadTermDiscarded;
}

/// <summary>
/// Changes the notation of the polynom / also does the appropriate conversion for a values.
/// </summary>
/// <param name="polynom">The polynom.</param>
void QRPolynom::changeNotation(QRPolynom& polynom, alphaNotation newNotation){
	if (polynom.notation == newNotation)
	{
		Serial.print(F("polynom already has '"));
		Serial.print(polynom.notation);
		Serial.println(F("' notation..."));
	}
	else
	{
		polynom.notation = newNotation;

		for (uint8_t i = 0; i < polynom.monomCount; i++)
		{
			polynom.monoms[i].a = (newNotation == alpha) ? integerToAlphaExp(polynom.monoms[i].a) : alphaExpToInteger(polynom.monoms[i].a);
		}
	}

}


/// <summary>
/// returns the integer value for alphas exponent notation
/// (i.e. a=5->32; a=7->128; a=8->29; a=13->135)
/// </summary>
/// <param name="a">a.</param>
/// <returns></returns>
uint8_t QRPolynom::alphaExpToInteger(uint8_t a){
	
	if (a < 8)
		return myMath::myIntExp(2, a);
	
	// higher values than 255 have to be "xored" with 285
	// first higher value: 2^8 = 256

	uint16_t times2 = a - 8;
	uint16_t result = 29; // 2^8 = 256 -> 256^285 (XOR) = 29
	for (uint8_t i = 0; i < times2; i++)
	{
		result *= 2;
		if (result > 255)
			result ^= 285; // XOR
	}
	return (uint8_t) result;
}

/// <summary>
/// returns the alpha exponent notation for the integer value (attention aInt must not be 0, as there is no alpha notation for that)
/// </summary>
/// <param name="aInt">a int.</param>
/// <returns></returns>
uint8_t QRPolynom::integerToAlphaExp(uint8_t aInt){
	if (aInt == 0)
	{
		//ToDo: setErrorCode
		return NULL;
	}
	
	for (uint8_t i = 0; i <= 255; i++)
	{
		if (aInt == QRPolynom::alphaExpToInteger(i))
		{
			return i;
		}
	}
}

#pragma endregion public_Methods