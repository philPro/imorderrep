#pragma once

#ifndef MyMemoryManager_H
#define	MyMemoryManager_H

#if ARDUINO >= 100
#include "Arduino.h"
//#include "Print.h"
#else
#include "WProgram.h"
#endif

class MyMemoryManager
{
private:
	static uint8_t _initialized;
	static uint8_t* _reservedArray;
	static uint16_t _firstAddress;
	static uint16_t _firstUnusedAddress;
	static uint16_t _lastAddress;

	static const uint8_t _firstIndex;
	static uint16_t _firstUnusedIndex;
	static uint16_t _lastUnusedIndex;
	static uint16_t _lastIndex;

public:
	MyMemoryManager();
	~MyMemoryManager();

	static void myMemInit(uint16_t size);
	static void* myMemMalloc(uint16_t size);
	static void myMemFree(uint8_t* arrStartPointer);
};

#endif