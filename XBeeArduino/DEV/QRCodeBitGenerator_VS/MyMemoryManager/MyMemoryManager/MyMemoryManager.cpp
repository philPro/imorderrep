#include "MyMemoryManager.h"

uint8_t MyMemoryManager::_initialized = false;
uint8_t* MyMemoryManager::_reservedArray = NULL;
uint16_t MyMemoryManager::_firstAddress = NULL;
uint16_t MyMemoryManager::_lastAddress = NULL;
const uint8_t MyMemoryManager::_firstIndex = 0;
uint16_t MyMemoryManager::_firstUnusedIndex = NULL;
uint16_t MyMemoryManager::_lastUnusedIndex = NULL;
uint16_t MyMemoryManager::_lastIndex = NULL;

MyMemoryManager::MyMemoryManager()
{
}


MyMemoryManager::~MyMemoryManager()
{
}


/// <summary>
/// reserves "size" bytes of memory, which can/should be used via "myMemMalloc" for new arrays
/// </summary>
/// <param name="size">The size.</param>
void MyMemoryManager::myMemInit(uint16_t size = 1024){
	
	_reservedArray = (uint8_t*)malloc(size);
	if (_reservedArray == NULL)
	{
		//ToDo: ErrorMessage & end init
		Serial.println(F("reserve Array Memory failed"));
		return;
	}

	for (uint16_t index = 0; index < size; index++)
	{
		if ((uint16_t)&_reservedArray[index] != ((uint16_t)&_reservedArray[0]) + index){
			//ToDo: ErrorMessage & end init
			Serial.print(index);
			Serial.print(F(": reserved Array not sequential: "));
			Serial.println(F("((uint16_t)&_reservedArray[index] != ((uint16_t)&_reservedArray[0]) + index)"));
			Serial.print(((uint16_t)&_reservedArray[index]));
			Serial.print(F(" != "));
			Serial.println(((uint16_t)&_reservedArray[0]) + index);

			return;
		}
	}
	Serial.println("init SUCCESSS");
	
	/* *_reservedArray: 124
	(uint16_t)&_reservedArray : 493
	(uint16_t)*&_reservedArray : 684
	(uint16_t)&_reservedArray[0] : 684
	* &_reservedArray[0] : 124 */
	
	_initialized = true;

	_firstUnusedIndex = 0;
	_firstUnusedAddress = &_reservedArray[0];
	_lastUnusedIndex = size - 1;
	_lastIndex = size - 1;

	_firstAddress = (uint16_t)&_reservedArray[0];
	_lastAddress = _firstAddress + size-1;
}



/// <summary>
/// allocates "size" bytes of memory in array which was reserved in "myMemInit";
/// fyi: over-allocation of sizeof(uint8_t*) takes place in order to save size of array pointers;
/// </summary>
/// <param name="size">The size.</param>
/// <returns></returns>
void* MyMemoryManager::myMemMalloc(uint16_t size){
	if (_initialized == false)
	{
		MyMemoryManager::myMemInit();
	}

	if (size == 0)
	{
		return NULL;
	}

	uint16_t requestedBytes = size + sizeof(uint8_t*);

	if ((requestedBytes - 1) > _lastIndex)
	{
		// not enough memory in reserved Array
		//ToDo: reallocate Memory in reserved Array
		return NULL;
	}
	
	uint16_t retPtrIndex = _firstUnusedIndex + sizeof(uint8_t*);
	Serial.print("size: ");
	Serial.println(size);

	_reservedArray[_firstUnusedIndex] = size % 255;
	_reservedArray[_firstUnusedIndex + 1] = (size < 256) ? 0 : 255;


	_firstUnusedIndex += requestedBytes;
	
	return &_reservedArray[retPtrIndex];
}

void MyMemoryManager::myMemFree(uint8_t* arrStartPointer){
	uint16_t size = arrStartPointer[-1] + arrStartPointer[-2];

	if (arrStartPointer[])
	{

	}

	for (uint8_t index = 0; index < size; index++)
	{
		arrStartPointer[index] = NULL;
	}
	arrStartPointer[-1] = NULL;
	arrStartPointer[-2] = NULL;
}