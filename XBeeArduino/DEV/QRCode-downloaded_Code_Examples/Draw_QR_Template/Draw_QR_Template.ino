//#include <TFT.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>

// For the breakout, you can use any 2 or 3 pins
// These pins will also work for the 1.8" TFT shield
#define TFT_CS     10
#define TFT_RST    9  // you can also connect this to the Arduino reset
                      // in which case, set this #define pin to 0!
#define TFT_DC     8

// Option 1 (recommended): must use the hardware SPI pins
// (for UNO thats sclk = 13 and sid = 11) and pin 10 must be
// an output. This is much faster - also required if you want
// to use the microSD card (see the image drawing example)
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS,  TFT_DC, TFT_RST);

// Option 2: use any pins but a little slower!
#define TFT_SCLK 13   // set these to be whatever pins you like!
#define TFT_MOSI 11   // set these to be whatever pins you like!
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_MOSI, TFT_SCLK, TFT_RST);
void setup(){
  tft.initR(INITR_BLACKTAB);   // initialize a ST7735S chip, black tab

  // Use this initializer (uncomment) if you're using a 1.44" TFT
  //tft.initR(INITR_144GREENTAB);   // initialize a ST7735S chip, black tab

  Serial.println("Initialized");

  uint16_t time = millis();
  tft.fillScreen(ST7735_BLACK);
  time = millis() - time;

  tft.fillScreen(ST7735_WHITE);
  delay(1000);
  
  drawTemplate();

}

void loop(){

}

void drawTemplate()
{

   //TOP RIGHT OUTER BOX START
  tft.fillRect(10,50,0,0,ST7735_BLACK);
  tft.fillRect(10,60,1,1,ST7735_BLACK);
  tft.fillRect(10,70,2,2,ST7735_BLACK);
  tft.fillRect(10,80,3,3,ST7735_BLACK);
  tft.fillRect(10,90,4,4,ST7735_BLACK);
  tft.fillRect(10,100,5,5,ST7735_BLACK);
  tft.fillRect(10,110,6,6,ST7735_BLACK);

//  tft.fillRect(20,50,10,10,ST7735_BLACK);
//  tft.fillRect(30,50,10,10,ST7735_BLACK);
//  tft.fillRect(40,50,10,10,ST7735_BLACK);
//  tft.fillRect(50,50,10,10,ST7735_BLACK);
//  tft.fillRect(60,50,10,10,ST7735_BLACK);
//  tft.fillRect(70,50,10,10,ST7735_BLACK);
//
//  tft.fillRect(70,60,10,10,ST7735_BLACK);
//  tft.fillRect(70,70,10,10,ST7735_BLACK);
//  tft.fillRect(70,80,10,10,ST7735_BLACK);
//  tft.fillRect(70,90,10,10,ST7735_BLACK);
//  tft.fillRect(70,100,10,10,ST7735_BLACK);
//  tft.fillRect(70,110,10,10,ST7735_BLACK);
//
//  tft.fillRect(60,110,10,10,ST7735_BLACK);
//  tft.fillRect(50,110,10,10,ST7735_BLACK);
//  tft.fillRect(40,110,10,10,ST7735_BLACK);  
//  tft.fillRect(30,110,10,10,ST7735_BLACK);
//  tft.fillRect(20,110,10,10,ST7735_BLACK);  
//  //TOP RIGHT OUTER BOX END
//  
//  //TOP RIGHT INNER BOX START
//  tft.fillRect(30,90,10,10,ST7735_BLACK);
//  tft.fillRect(30,80,10,10,ST7735_BLACK);
//  tft.fillRect(30,70,10,10,ST7735_BLACK);
//
//  tft.fillRect(40,90,10,10,ST7735_BLACK);
//  tft.fillRect(40,80,10,10,ST7735_BLACK);
//  tft.fillRect(40,70,10,10,ST7735_BLACK);
//
//  tft.fillRect(50,90,10,10,ST7735_BLACK);
//  tft.fillRect(50,80,10,10,ST7735_BLACK);
//  tft.fillRect(50,70,10,10,ST7735_BLACK);
//  //TOP RIGHT INNER BOX END
//  
  
//  //TOP LEFT CORNER OUTER BOX 
//  tft.fillRect(10,250,10,10,ST7735_BLACK);
//  tft.fillRect(10,240,10,10,ST7735_BLACK);
//  tft.fillRect(10,230,10,10,ST7735_BLACK);
//  tft.fillRect(10,220,10,10,ST7735_BLACK);
//  tft.fillRect(10,210,10,10,ST7735_BLACK);
//  tft.fillRect(10,200,10,10,ST7735_BLACK);
//  tft.fillRect(10,190,10,10,ST7735_BLACK);
//
//  
//  tft.fillRect(20,250,10,10,ST7735_BLACK);
//  tft.fillRect(30,250,10,10,ST7735_BLACK);
//  tft.fillRect(40,250,10,10,ST7735_BLACK);
//  tft.fillRect(50,250,10,10,ST7735_BLACK);
//  tft.fillRect(60,250,10,10,ST7735_BLACK);
//  tft.fillRect(70,250,10,10,ST7735_BLACK);
//
//  tft.fillRect(70,240,10,10,ST7735_BLACK);
//  tft.fillRect(70,230,10,10,ST7735_BLACK);
//  tft.fillRect(70,220,10,10,ST7735_BLACK);
//  tft.fillRect(70,210,10,10,ST7735_BLACK);
//  tft.fillRect(70,200,10,10,ST7735_BLACK);
//  tft.fillRect(70,190,10,10,ST7735_BLACK);
//  
//  tft.fillRect(60,190,10,10,ST7735_BLACK);
//  tft.fillRect(50,190,10,10,ST7735_BLACK);
//  tft.fillRect(40,190,10,10,ST7735_BLACK);
//  tft.fillRect(30,190,10,10,ST7735_BLACK);
//  tft.fillRect(20,190,10,10,ST7735_BLACK);
//  //TOP LEFT CORNER OUTER BOX END  
//  
//  //TOP LEFT CORNER INSIDE BOX START  
//  tft.fillRect(30,230,10,10,ST7735_BLACK);
//  tft.fillRect(30,220,10,10,ST7735_BLACK);
//  tft.fillRect(30,210,10,10,ST7735_BLACK);
//
//  tft.fillRect(40,230,10,10,ST7735_BLACK);
//  tft.fillRect(40,220,10,10,ST7735_BLACK);
//  tft.fillRect(40,210,10,10,ST7735_BLACK);
//
//  tft.fillRect(50,230,10,10,ST7735_BLACK);
//  tft.fillRect(50,220,10,10,ST7735_BLACK);
//  tft.fillRect(50,210,10,10,ST7735_BLACK);  
//  //TOP LEFT CORNER INSIDE BOX END
//  
//  //BOTTOM RIGHT OUTER BOX START
//  tft.fillRect(210,250,10,10,ST7735_BLACK);
//  tft.fillRect(200,250,10,10,ST7735_BLACK);
//  tft.fillRect(190,250,10,10,ST7735_BLACK);
//  tft.fillRect(180,250,10,10,ST7735_BLACK);
//  tft.fillRect(170,250,10,10,ST7735_BLACK);
//  tft.fillRect(160,250,10,10,ST7735_BLACK);
//  tft.fillRect(150,250,10,10,ST7735_BLACK);
//
//  tft.fillRect(210,240,10,10,ST7735_BLACK);
//  tft.fillRect(210,230,10,10,ST7735_BLACK);
//  tft.fillRect(210,220,10,10,ST7735_BLACK);
//  tft.fillRect(210,210,10,10,ST7735_BLACK);
//  tft.fillRect(210,200,10,10,ST7735_BLACK);
//  tft.fillRect(210,190,10,10,ST7735_BLACK);
//
//  tft.fillRect(200,190,10,10,ST7735_BLACK);
//  tft.fillRect(190,190,10,10,ST7735_BLACK);
//  tft.fillRect(180,190,10,10,ST7735_BLACK);
//  tft.fillRect(170,190,10,10,ST7735_BLACK);
//  tft.fillRect(160,190,10,10,ST7735_BLACK);
//  tft.fillRect(150,190,10,10,ST7735_BLACK);
//
//  tft.fillRect(150,200,10,10,ST7735_BLACK);
//  tft.fillRect(150,210,10,10,ST7735_BLACK);
//  tft.fillRect(150,220,10,10,ST7735_BLACK);
//  tft.fillRect(150,230,10,10,ST7735_BLACK);
//  tft.fillRect(150,240,10,10,ST7735_BLACK);
//   //BOTTOM RIGHT OUTER BOX END
//  
//  //BOTTOM RIGHT INNER BOX START
//  tft.fillRect(170,230,10,10,ST7735_BLACK);
//  tft.fillRect(170,220,10,10,ST7735_BLACK);
//  tft.fillRect(170,210,10,10,ST7735_BLACK);
//  
//  tft.fillRect(180,230,10,10,ST7735_BLACK);
//  tft.fillRect(180,220,10,10,ST7735_BLACK);
//  tft.fillRect(180,210,10,10,ST7735_BLACK);
//
//  tft.fillRect(190,230,10,10,ST7735_BLACK);
//  tft.fillRect(190,220,10,10,ST7735_BLACK);
//  tft.fillRect(190,210,10,10,ST7735_BLACK);
//  //BOTTOM RIGHT INNER BOX END
//
//  //Timing Pattern
//   tft.fillRect(70,170,10,10,ST7735_BLACK);
//   tft.fillRect(70,150,10,10,ST7735_BLACK);
//   tft.fillRect(70,130,10,10,ST7735_BLACK);
//
//   tft.fillRect(90,190,10,10,ST7735_BLACK);
//   tft.fillRect(110,190,10,10,ST7735_BLACK);
//   tft.fillRect(130,190,10,10,ST7735_BLACK);
//
//   tft.fillRect(140,170,10,10,ST7735_BLACK);
//   //end Timing Pattern
}
