#include <EEPROM.h>

void setup() {
  Serial.begin(115200);
  Serial.println("Setup START");
  
  Serial.println(EEPROM.read(((3 - 2) * 7 + 1)));
  static const uint8_t _alignmentArrayRowCount = 39;
  static const uint8_t _alignmentArrayColCount = 7;
  
  uint8_t alignmentArray[_alignmentArrayRowCount][_alignmentArrayColCount] = {
		{ 6, 18, 0, 0, 0, 0, 0, }, // version 2
		{ 6, 22, 0, 0, 0, 0, 0, }, // version 3
		{ 6, 26, 0, 0, 0, 0, 0, }, // ...
		{ 6, 30, 0, 0, 0, 0, 0, },
		{ 6, 34, 0, 0, 0, 0, 0, },
		{ 6, 22, 38, 0, 0, 0, 0, },
		{ 6, 24, 42, 0, 0, 0, 0, },
		{ 6, 26, 46, 0, 0, 0, 0, },
		{ 6, 28, 50, 0, 0, 0, 0, },
		{ 6, 30, 54, 0, 0, 0, 0, },
		{ 6, 32, 58, 0, 0, 0, 0, },
		{ 6, 34, 62, 0, 0, 0, 0, },
		{ 6, 26, 46, 66, 0, 0, 0, },
		{ 6, 26, 48, 70, 0, 0, 0, },
		{ 6, 26, 50, 74, 0, 0, 0, },
		{ 6, 30, 54, 78, 0, 0, 0, },
		{ 6, 30, 56, 82, 0, 0, 0, },
		{ 6, 30, 58, 86, 0, 0, 0, },
		{ 6, 34, 62, 90, 0, 0, 0, },
		{ 6, 28, 50, 72, 94, 0, 0, },
		{ 6, 26, 50, 74, 98, 0, 0, },
		{ 6, 30, 54, 78, 102, 0, 0, },
		{ 6, 28, 54, 80, 106, 0, 0, },
		{ 6, 32, 58, 84, 110, 0, 0, },
		{ 6, 30, 58, 86, 114, 0, 0, },
		{ 6, 34, 62, 90, 118, 0, 0, },
		{ 6, 26, 50, 74, 98, 122, 0, },
		{ 6, 30, 54, 78, 102, 126, 0, },
		{ 6, 26, 52, 78, 104, 130, 0, },
		{ 6, 30, 56, 82, 108, 134, 0, },
		{ 6, 34, 60, 86, 112, 138, 0, },
		{ 6, 30, 58, 86, 114, 142, 0, },
		{ 6, 34, 62, 90, 118, 146, 0, },
		{ 6, 30, 54, 78, 102, 126, 150, },
		{ 6, 24, 50, 76, 102, 128, 154, },
		{ 6, 28, 54, 80, 106, 132, 158, },
		{ 6, 32, 58, 84, 110, 136, 162, }, // ...
		{ 6, 26, 54, 82, 110, 138, 166, }, // version 39
		{ 6, 30, 58, 86, 114, 142, 170, }, // version 40
	};

  uint16_t eepromIndex = 0;
  
  
  
  
  
  for(uint8_t row = 0; row < _alignmentArrayRowCount; row++){
    for(uint8_t col = 0; col < _alignmentArrayColCount; col++){
      //EEPROM.write(eepromIndex, alignmentArray[row][col]);
      Serial.print(alignmentArray[row][col]);
      (row+1 == _alignmentArrayRowCount && col+1 == _alignmentArrayColCount) ? Serial.println() : Serial.print(", ");
      eepromIndex++;
    }
  }
  
  for(uint8_t row = 0; row < _alignmentArrayRowCount; row++){
    for(uint8_t col = 0; col < _alignmentArrayColCount; col++){
      Serial.print(alignmentArray[row][col]);
      (row+1 == _alignmentArrayRowCount && col+1 == _alignmentArrayColCount) ? Serial.println() : Serial.print(", ");
    }
  }
  
  for(uint8_t row = 0; row < _alignmentArrayRowCount; row++){
    for(uint8_t col = 0; col < _alignmentArrayColCount; col++){
      Serial.print(EEPROM.read(row*_alignmentArrayColCount + col));
      (row+1 == _alignmentArrayRowCount && col+1 == _alignmentArrayColCount) ? Serial.println() : Serial.print(", ");
    }
  }
  
  
  // put your setup code here, to run once:
  Serial.println("Setup END");
}

void loop() {
  // put your main code here, to run repeatedly:

}
