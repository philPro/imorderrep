unit DataModul.Meeting;

interface

uses
  System.Classes, System.SysUtils, ZConnection,
  ZDataset, Vcl.Forms, Data.DB, System.UITypes, Spring.Container, Spring.Services,
  DataModul.Interfaces, DataModul.Errors;

type
  TDataModulMeetings = class(TInterfacedObject, IDataModulMeetings)
  private
    FActiveMeetingsList: TInterfaceList;
    FHistoryMeetingsList: TInterfaceList;
    FDBConnection: IDataModulCore;
  public
    constructor Create;
    destructor Destroy; override;
    function LoadData: Boolean;
    function GetActiveItem(aIndex: Integer): IDataModulMeeting;
    function GetHistoryItem(aIndex: Integer): IDataModulMeeting;
    function GetActiveCount: Integer;
    function GetHistoryCount: Integer;
    procedure SetDataModulCore(aDataModulCore: IDataModulCore);
    property ActiveCount: Integer read GetActiveCount;
    property HistoryCount: Integer read GetHistoryCount;
    property DataModulCore: IDataModulCore write SetDataModulCore;
  end;

implementation

{ TDataModulMeetings }

constructor TDataModulMeetings.Create;
begin
  inherited Create;
  FActiveMeetingsList := TInterfaceList.Create;
  FHistoryMeetingsList := TInterfaceList.Create;
end;

destructor TDataModulMeetings.Destroy;
begin
  FreeAndNil(FActiveMeetingsList);
  FreeAndNil(FHistoryMeetingsList);
  inherited;
end;

function TDataModulMeetings.GetActiveCount: Integer;
begin
  Result := FActiveMeetingsList.Count;
end;

function TDataModulMeetings.GetActiveItem(aIndex: Integer): IDataModulMeeting;
begin
  Result := IDataModulMeeting(FActiveMeetingsList.Items[aIndex]);
end;

function TDataModulMeetings.GetHistoryCount: Integer;
begin
  Result := FHistoryMeetingsList.Count;
end;

function TDataModulMeetings.GetHistoryItem(aIndex: Integer): IDataModulMeeting;
begin
  Result := IDataModulMeeting(FHistoryMeetingsList.Items[aIndex]);
end;

function TDataModulMeetings.LoadData: Boolean;
var
  ListQuery: TZQuery;
  TIQuery: TZQuery;
  QRQuery: TZQuery;
  iTIID: Integer;
  iQRID: Integer;
  Meeting: IDataModulMeeting;
begin

  Result := False;
  if not FDBConnection.ConnectToDB then
    raise EDataModulError.Create('Database not connected!');

  FActiveMeetingsList.Clear;
  FHistoryMeetingsList.Clear;
  Screen.Cursor := crHourGlass;

  ListQuery := TZQuery.Create(nil);
  TIQuery := TZQuery.Create(nil);
  QRQuery := TZQuery.Create(nil);
  ListQuery.Connection := FDBConnection.GetSession^;
  TIQuery.Connection := ListQuery.Connection;
  QRQuery.Connection := ListQuery.Connection;
  try
    ListQuery.SQL.Add('SELECT * FROM ' + TABLE_MEETING);
    try
      ListQuery.Active := True;
      if ListQuery.IsEmpty then Exit;

      ListQuery.First;
      while not ListQuery.Eof do
      begin
        TIQuery.SQL.Clear;
        QRQuery.SQL.Clear;
        Meeting := ServiceLocator.GetService<IDataModulMeeting>;
        Meeting.ID := ListQuery.FieldByName('M_ID').AsInteger;
        Meeting.BeginDate := ListQuery.FieldByName('BEGIN_DATE').AsDateTime;
        Meeting.EndDate := ListQuery.FieldByName('END_DATE').AsDateTime;
        Meeting.Participants := ListQuery.FieldByName('PARTICIPANTS').AsInteger;
        Meeting.BillSum := ListQuery.FieldByName('BILL_SUM').AsFloat;
        iTIID := ListQuery.FieldByName('T_ID').AsInteger;
        iQRID := ListQuery.FieldByName('ACTIVE_QR_CODE').AsInteger;
        { TI DATA }
        TIQuery.SQL.Clear;
        TIQuery.SQL.Add('SELECT * FROM ' + TABLE_TABLE_IDENTIFIERS + ' WHERE T_ID = ' + IntToStr(iTIID));
        TIQuery.Active := True;
        Meeting.TableIdentifier.ID := TIQuery.FieldByName('T_ID').AsInteger;
        Meeting.TableIdentifier.RID := TIQuery.FieldByName('R_ID').AsInteger;
        Meeting.TableIdentifier.Name := TIQuery.FieldByName('NAME').AsString;
        Meeting.TableIdentifier.TableNumber := TIQuery.FieldByName('TABLE_NUMBER').AsString;
        Meeting.TableIdentifier.XBeeHexID := TIQuery.FieldByName('XBeeHexID').AsString;
        Meeting.TableIdentifier.Connected := False;
          { QR-Code from TI is not needed here }
        { QR CODE }
        if iQRID > -1 then
        begin
          QRQuery.SQL.Clear;
          QRQuery.SQL.Add('SELECT * FROM ' + TABLE_QR_CODE + ' WHERE idt_qr_code = ' + IntToStr(iQRID));
          QRQuery.Active := True;
          Meeting.ActiveQRCode.ID := QRQuery.FieldByName('idt_qr_code').AsInteger;
          Meeting.ActiveQRCode.QRText := QRQuery.FieldByName('qr_code').AsString;
          Meeting.ActiveQRCode.PIN := QRQuery.FieldByName('pin').AsString;
          FActiveMeetingsList.Add(Meeting);
        end else
        begin
          FHistoryMeetingsList.Add(Meeting);
        end;
        ListQuery.Next;
      end;
    except
      on E: Exception do
      begin
        raise EDataModulError.Create('Couldn''t get Meetings from DB! Error: ' + sLineBreak + E.Message);
      end;
    end;
  finally
    ListQuery.Free;
    TIQuery.Free;
    QRQuery.Free;
    Screen.Cursor := crDefault;
  end;

  Result := True;
end;

procedure TDataModulMeetings.SetDataModulCore(aDataModulCore: IDataModulCore);
begin
  FDBConnection := aDataModulCore;
end;

initialization
  GlobalContainer.RegisterType<TDataModulMeetings>.Implements<IDataModulMeetings>('DataModulMeetings');

end.
