unit Communication.Protocol;

interface

uses
  System.SysUtils, System.StrUtils, Spring.Services, Spring.Container,
  Communication.Interfaces, DataModul.Interfaces;

type
  TCommunicationProtocol = class(TInterfacedObject, ICommunicationProtocol)
  private
    FGUID: String;
    FReqMessage: TRequest;
    FRepMessage: TReply;
    FTableID: IDataModulTableIdentifier;
    FTimeStamp: TDateTime;
  public
    procedure SetGUID(aGUID: String);
    function GetGUID: String;
    procedure SetReqMessage(aReq: TRequest);
    function GetReqMessage: TRequest;
    procedure SetRepMessage(aRep: TReply);
    function GetRepMessage: TReply;
    procedure SetTableID(aTableID: IDataModulTableIdentifier);
    function GetTableID: IDataModulTableIdentifier;
    procedure SetTimeStamp(aTimeStamp: TDateTime);
    function GetTimeStamp: TDateTime;
    function ToSendString: String;
    procedure FromSendString(const aString: String);
    property GUID: String read GetGUID write SetGUID;
    property ReqMessage: TRequest read GetReqMessage write SetReqMessage;
    property RepMessage: TReply read GetRepMessage write SetRepMessage;
    property TableID: IDataModulTableIdentifier read GetTableID write SetTableID;
    property TimeStamp: TDateTime read GetTimeStamp write SetTimeStamp;
  end;

implementation

{ TCommunicationProtocol }

procedure TCommunicationProtocol.FromSendString(const aString: String);
var
  iStartPos: Integer;
  iStopPos: Integer;
begin
  { Message-Aufbau }
  { START_CHAR  GUID | REQUEST | REPLY | XBeeHexID | QRCode | QR-Pin  END_CHAR }

  { GUID }
  iStartPos := 2;
  iStopPos := Pos(SEPARATOR, aString);
  GUID := Copy(aString, iStartPos, iStopPos - iStartPos);

  { Request }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aString, iStartPos);
  ReqMessage := TRequest(StrToInt(Copy(aString, iStartPos, iStopPos - iStartPos)));

  { Reply }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aString, iStartPos);
  RepMessage := TReply(StrToInt(Copy(aString, iStartPos, iStopPos - iStartPos)));

  if not Assigned(TableID) then
    TableID := ServiceLocator.GetService<IDataModulTableIdentifier>;
  { XBeeHex }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aString, iStartPos);
  TableId.XBeeHexID := Copy(aString, iStartPos, iStopPos - iStartPos);

  { QRCode }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(SEPARATOR, aString, iStartPos);
  TableID.QRCode.QRText := Copy(aString, iStartPos, iStopPos - iStartPos);

  { QRPin }
  iStartPos := iStopPos + 1;
  iStopPos := PosEx(END_CHAR, aString, iStartPos);
  TableID.QRCode.PIN := Copy(aString, iStartPos, iStopPos - iStartPos);
end;

function TCommunicationProtocol.ToSendString: String;
begin
  Result := START_CHAR + GUID + SEPARATOR +   { GUID }
    IntToStr(Ord(ReqMessage)) + SEPARATOR +   { Request }
    IntToStr(Ord(RepMessage)) + SEPARATOR +   { Reply }
    TableID.XBeeHexID + SEPARATOR +           { XBeeHexID }
    TableID.QRCode.QRText + SEPARATOR +       { QRCode }
    TableID.QRCode.PIN + END_CHAR;            { QR-Pin }
end;

function TCommunicationProtocol.GetGUID: String;
begin
  Result := FGUID;
end;

function TCommunicationProtocol.GetRepMessage: TReply;
begin
  Result := FRepMessage;
end;

function TCommunicationProtocol.GetReqMessage: TRequest;
begin
  Result := FReqMessage;
end;

function TCommunicationProtocol.GetTableID: IDataModulTableIdentifier;
begin
  Result := FTableID;
end;

function TCommunicationProtocol.GetTimeStamp: TDateTime;
begin
  Result := FTimeStamp;
end;

procedure TCommunicationProtocol.SetGUID(aGUID: String);
begin
  FGUID := aGUID;
end;

procedure TCommunicationProtocol.SetRepMessage(aRep: TReply);
begin
  FRepMessage := aRep;
end;

procedure TCommunicationProtocol.SetReqMessage(aReq: TRequest);
begin
  FReqMessage := aReq;
end;

procedure TCommunicationProtocol.SetTableID(
  aTableID: IDataModulTableIdentifier);
begin
  FTableID := aTableID;
end;

procedure TCommunicationProtocol.SetTimeStamp(aTimeStamp: TDateTime);
begin
  FTimeStamp := aTimeStamp;
end;

initialization
  GlobalContainer.RegisterType<TCommunicationProtocol>.Implements<ICommunicationProtocol>('CommunicationProtocol');

end.
