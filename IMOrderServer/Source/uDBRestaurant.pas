unit uDBRestaurant;

{------------------------------------------------------------------------------}
interface
{------------------------------------------------------------------------------}

uses
  Generics.Collections, ZAbstractConnection, ZConnection, Vcl.Forms,
  System.UITypes, System.SysUtils, ZAbstractRODataset, ZAbstractDataset,
  ZDataset;

type
{------------------------------------------------------------------------------}
  TArticleCategorie = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FiFKID: Integer;
    FsName: String;
    FbMainCategory: Boolean;
  end;

{------------------------------------------------------------------------------}
  TArticleCategorieList = class(TObjectList<TArticleCategorie>);
{------------------------------------------------------------------------------}

{------------------------------------------------------------------------------}
  TArticle = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FsName: String;
    FdCost: Double;
    FArticleCategories: TArticleCategorieList;
  end;

{------------------------------------------------------------------------------}
  TArticleList = class(TObjectList<TArticle>);
{------------------------------------------------------------------------------}

{------------------------------------------------------------------------------}
  TRestaurant = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FsName: String;
    FsStreet: String;
    FsCity: String;
    FiPLZ: Integer;
    FiTableCount: Integer;
  end;

{------------------------------------------------------------------------------}
  TQRCode = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FsQRCode: String;
    FsPIN: String;
  end;

{------------------------------------------------------------------------------}
  TTableIdentifier = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FsName: String;
    FiRID: Integer;
    FsTableNumber: String;
    FQRCode: TQRCode;
  end;

{------------------------------------------------------------------------------}
  TTableIdentifierList = class(TObjectList<TTableIdentifier>);
{------------------------------------------------------------------------------}

{------------------------------------------------------------------------------}
  TMeeting = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FTableIdentifier: TTableIdentifier;
    FdtBeginDate: TDateTime;
    FdtEndDate: TDateTime;
    FiParticipants: Integer;
    FdBillSum: Double;
  end;

{------------------------------------------------------------------------------}
  TMeetingList = class(TObjectList<TMeeting>);
{------------------------------------------------------------------------------}

{------------------------------------------------------------------------------}
  TUser = class
{------------------------------------------------------------------------------}
    FiID: Integer;
    FsUniqueID: String;
    FsForename: String;
    FsLastname: String;
    FbAgeTeenProofed: Boolean;
    FbAgeAdultProofed: Boolean;
    FdtBirthday: TDateTime;
    FTableIdentifier: TTableIdentifier;
  end;

{------------------------------------------------------------------------------}
  TUserList = class(TObjectList<TUser>);
{------------------------------------------------------------------------------}

{##############################################################################}

{------------------------------------------------------------------------------}
  TDBConnection = record
{------------------------------------------------------------------------------}
    FsDatabase: String;
    FsHostname: String;
    FsLibraryLocation: String;
    FiPort: Integer;
    FsUser: String;
    FsPassword: String;
  end;

{##############################################################################}

{------------------------------------------------------------------------------}
  TDBRestaurant = class
{------------------------------------------------------------------------------}
  private const
    TABLE_ARTICLE_CATEGORY = 't_article_category';
    TABLE_TABLE_IDENTIFIERS = 't_table_identifiers';
    TABLE_ARTICLES = 't_articles';
    TABLE_USERS = 't_users';
  private
    FDBConnection: TDBConnection;
    FIMOrderSession: TZConnection;
    FbDataInitialized: Boolean;
    { Daten halten }
    FolArticleCategorieList: TArticleCategorieList;
    FolArticleList: TArticleList;
    FolTableList: TTableIdentifierList;
    FolUserList: TUserList;
    { Daten holen }
    function GetRestaurant(var aRestaurant: TRestaurant; var aErrorCode: Integer): Boolean;
    function GetTableIdentifierList(var aTableIDList: TTableIdentifierList; var aErrorCode): Boolean;
    function GetTableIdentifier(var aTableID: TTableIdentifier; var aErrorCode: Integer): Boolean;
    function GetMeetingList(var aMeetingList: TMeetingList; var aErrorCode: Integer): Boolean;
    function GetMeeting(var aMetting: TMeeting; var aErrorCode: Integer): Boolean;
    function GetUserList(var aUserList: TUserList; var aErrorCode: Integer): Boolean;
    function GetArticleCategoryList(var aArticleCatList: TArticleCategorieList; var aErrorCode: Integer): Boolean;
    function GetArticleCategory(var aArticleCat: TArticleCategorie; var aErrorCode: Integer): Boolean;
    function GetArticleList(var aArticleList: TArticleList; var aErrorCode: Integer): Boolean;
  public const
    dbERR_NOT_CONNECTED = 1;
  public
    constructor Create;
    destructor Destroy; override;
    function ConnectToDB(aDBSettings: TDBConnection): Boolean;
    procedure DisconnectDB;
    function LoadAllData: Boolean;
    property ArticleCategorieList: TArticleCategorieList read FolArticleCategorieList write FolArticleCategorieList;
  end;

{------------------------------------------------------------------------------}
  TDBHelper = class helper for TDBRestaurant
{------------------------------------------------------------------------------}
    class function DBIntToBool(aVal: Integer): Boolean;
  end;

{------------------------------------------------------------------------------}
implementation
{------------------------------------------------------------------------------}

{ TDBRestaurant }
{------------------------------------------------------------------------------}
constructor TDBRestaurant.Create;
{------------------------------------------------------------------------------}
begin
  inherited Create;
  FIMOrderSession := TZConnection.Create(nil);

  FolArticleCategorieList := TArticleCategorieList.Create;
  FolArticleList := TArticleList.Create;
  FolTableList := TTableIdentifierList.Create;
  FolUserList := TUserList.Create;

  FbDataInitialized := False;
end;

{------------------------------------------------------------------------------}
destructor TDBRestaurant.Destroy;
{------------------------------------------------------------------------------}
begin
  if FIMOrderSession.Connected then DisconnectDB;
  FreeAndNil(FIMOrderSession);

  FolArticleCategorieList.Free;
  FolArticleList.Free;
  FolTableList.Free;
  FolUserList.Free;

  inherited Destroy;
end;

{------------------------------------------------------------------------------}
function TDBRestaurant.ConnectToDB(aDBSettings: TDBConnection): Boolean;
{------------------------------------------------------------------------------}
begin
  Screen.Cursor := crHourGlass;
  FDBConnection := aDBSettings;

  try
    FIMOrderSession.Database := FDBConnection.FsDatabase;
    FIMOrderSession.HostName := FDBConnection.FsHostname;
    FIMOrderSession.LibraryLocation := FDBConnection.FsLibraryLocation;
    FIMOrderSession.Port := FDBConnection.FiPort;
    FIMOrderSession.User := FDBConnection.FsUser;
    FIMOrderSession.Password := FDBConnection.FsPassword;
    FIMOrderSession.Protocol := 'mysql';

    FIMOrderSession.Connect;
  finally
    Screen.Cursor := crDefault;
  end;

  Result := FIMOrderSession.Connected;
end;

{------------------------------------------------------------------------------}
procedure TDBRestaurant.DisconnectDB;
{------------------------------------------------------------------------------}
begin
  if FIMOrderSession.Connected then FIMOrderSession.Disconnect;
end;

{------------------------------------------------------------------------------}
function TDBRestaurant.LoadAllData: Boolean;
{------------------------------------------------------------------------------}
var
  iError: Integer;
begin
  Result := GetArticleCategoryList(FolArticleCategorieList, iError);
  Result := Result AND GetTableIdentifierList(FolTableList, iError);
  Result := Result AND GetArticleList(FolArticleList, iError);
  Result := Result AND GetUserList(FolUserList, iError);

  if Result then FbDataInitialized := True;
end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetArticleCategory(var aArticleCat: TArticleCategorie;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetArticleCategoryList(
  var aArticleCatList: TArticleCategorieList; var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
var
  ListQuery: TZQuery;
  iCatIdx: Integer;
begin
  Result := False;
  if not FIMOrderSession.Connected then
  begin
    aErrorCode := dbERR_NOT_CONNECTED;
    Exit;
  end;

  ListQuery := TZQuery.Create(nil);
  ListQuery.Connection := FIMOrderSession;
  try
    ListQuery.SQL.Add('SELECT * FROM ' + TABLE_ARTICLE_CATEGORY + ' ORDER BY AC_ID');
    aArticleCatList.Clear;
    try
      ListQuery.Active := True;
      if ListQuery.IsEmpty then Exit;

      ListQuery.First;
      iCatIdx := 0;
      while not ListQuery.Eof do
      begin
        aArticleCatList.Add(TArticleCategorie.Create);
        aArticleCatList.Items[iCatIdx].FiID := ListQuery.FieldByName('AC_ID').AsInteger;
        aArticleCatList.Items[iCatIdx].FiFKID := ListQuery.FieldByName('AC_ID_FK').AsInteger;
        aArticleCatList.Items[iCatIdx].FsName := ListQuery.FieldByName('CATEGORY_NAME').AsString;
        aArticleCatList.Items[iCatIdx].FbMainCategory := TDBRestaurant.DBIntToBool(ListQuery.FieldByName('IS_MAIN_CATEGORY').AsInteger);
        Inc(iCatIdx);
        ListQuery.Next;
      end;

      Result := True;
    except
      Result := False;
    end;
  finally
    ListQuery.Free;
  end;
end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetArticleList(var aArticleList: TArticleList;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
var
  ListQuery: TZQuery;
  ListQueryCat: TZQuery;
  iArtIdx: Integer;
begin
  Result := False;
  if not FIMOrderSession.Connected then
  begin
    aErrorCode := dbERR_NOT_CONNECTED;
    Exit;
  end;

  ListQuery := TZQuery.Create(nil);
  ListQueryCat := TZQuery.Create(nil);
  ListQuery.Connection := FIMOrderSession;
  try
    ListQuery.SQL.Add('SELECT * FROM ' + TABLE_ARTICLES);
    aArticleList.Clear;
    try
      ListQuery.Active := True;
      if ListQuery.IsEmpty then Exit;

      ListQuery.First;
      iArtIdx := 0;
      while not ListQuery.Eof do
      begin
        aArticleList.Add(TArticle.Create);
        aArticleList.Items[iArtIdx].FiID := ListQuery.FieldByName('A_ID').AsInteger;
        aArticleList.Items[iArtIdx].FsName := ListQuery.FieldByName('NAME').AsString;
        aArticleList.Items[iArtIdx].FdCost := ListQuery.FieldByName('COST').AsFloat;
        Inc(iArtIdx);
        ListQuery.Next;
      end;

      Result := True;
    except
      Result := False;
    end;
  finally
    ListQuery.Free;
    ListQueryCat.Free;
  end;

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetMeeting(var aMetting: TMeeting;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetMeetingList(var aMeetingList: TMeetingList;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetRestaurant(var aRestaurant: TRestaurant;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetTableIdentifier(var aTableID: TTableIdentifier;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetTableIdentifierList(
  var aTableIDList: TTableIdentifierList; var aErrorCode): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{------------------------------------------------------------------------------}
function TDBRestaurant.GetUserList(var aUserList: TUserList;
  var aErrorCode: Integer): Boolean;
{------------------------------------------------------------------------------}
begin

end;

{ TDBHelper }
{------------------------------------------------------------------------------}
class function TDBHelper.DBIntToBool(aVal: Integer): Boolean;
{------------------------------------------------------------------------------}
begin
  Result := False;
  if aVal = 1 then Result := True;
end;

end.
