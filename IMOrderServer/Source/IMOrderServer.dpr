program IMOrderServer;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  Spring.Container,
  uFrmMainServer in 'uFrmMainServer.pas' {FrmMainServer},
  Communication.Interfaces in 'Communication.Interfaces.pas',
  Communication.TableIdentifier in 'Communication.TableIdentifier.pas',
  DataModul.Core in 'DataModul.Core.pas',
  DataModul.TableIdentifier in 'DataModul.TableIdentifier.pas',
  Communication.Serial.Arduino in 'Communication.Serial.Arduino.pas',
  Communication.Errors in 'Communication.Errors.pas',
  DataModul.Errors in 'DataModul.Errors.pas',
  DataModul.Interfaces in 'DataModul.Interfaces.pas',
  DataModul.TableIdentifier.Item in 'DataModul.TableIdentifier.Item.pas',
  DataModul.TableIdentifier.QRCode in 'DataModul.TableIdentifier.QRCode.pas',
  DataModul.Meeting in 'DataModul.Meeting.pas',
  DataModul.Meeting.Item in 'DataModul.Meeting.Item.pas',
  Communication.Core in 'Communication.Core.pas',
  IMOServer.Settings.Interfaces in 'IMOServer.Settings.Interfaces.pas',
  IMOServer.Settings.Global in 'IMOServer.Settings.Global.pas',
  Communication.TCP.Indy in 'Communication.TCP.Indy.pas',
  Communication.Protocol in 'Communication.Protocol.pas';

{$R *.res}

begin
  GlobalContainer.Build;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMainServer, FrmMainServer);
  Application.Run;
end.
