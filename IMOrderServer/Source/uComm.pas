(*******************************************************************************
*                                                                              *
*  filename:       uComm.pas                                                   *
*  author:         lukas singer lukas.singer@swarovski.com                     *
*  creationdate:   2013/10/24                                                  *
*                                                                              *
*  purpose:        communication between control panel and master computer     *
*                  file exchange, status requests, ...                         *
*                                                                              *
*  protocoll:                                                                  *
*    header:       recvtype|hostname|timestamp|requestid|size|#EndOfLine       *
       |:          seperator                                                   *
*      recvtype:   string representing a enum f.e.: rtStringRequest            *
*      name:       computername of the sender f.e.: dswb1000                   *
*      timestamp:  formated datetime format: YYYYMMDDHHNNSSZZZ                 *
*      requestid:  for use in application purpose                              *
*      size:       depending on recvtype size represents the bytecount of the  *
*                  following stream or the line count of the following text.   *
*      #EndOfLine: string terminator (use const sLineBreak or writeln)         *
*    data:         the user data, type is speciefied in header                 *
*                                                                              *
*******************************************************************************)

unit uComm;

interface

uses
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdCustomTCPServer,
  IdTCPServer, IdContext, Classes, SysUtils, SyncObjs, TypInfo;

type
  TRecvType=(
    rtRequestReply,  //size=0
    rtStreamRequest, //size=0
    rtStringRequest, //size=0
    rtStatusRequest, //size=0
    rtReply,         //size=0
    rtStream,        //size=answerstream.size
    rtString,        //size=stringlist.count
    rtStatus         //size=1  (eine zeile mit status als string ('0' bis IntToStr(MAXINTEGER)))
  );

type
  TCommHeader=record
    RecvType:TRecvType;
    CompName:string;
    TimeStamp:TDateTime;//timestamp of send
    RequestID:Cardinal; //wird ignoriert wenn recvtype keine request ist
    Size:Int64;         //die gr��e des streams oder die anzahl der zeilen (siehe recvtype)
    function ToString:string;
    procedure FromString(const AString:string);
  end;

type
  TRecvStreamRequestEvent = procedure(ARequestId:Cardinal;var AOutStream:TStream) of object;
  TRecvStringRequestEvent = procedure(ARequestId:Cardinal;var AOutString:TStrings) of object;
  TRecvStatusRequestEvent = procedure(ARequestId:Cardinal;var AAnswerId:Cardinal) of object;
  TRecvStreamEvent = procedure(AStream:TStream) of object;
  TRecvStringEvent = procedure(AString:TStrings) of object;
  TRecvStatusEvent = procedure(AStatus:Cardinal) of object;

type
  TCommunication = class(TComponent)
  private
    FIndyClient:TIdTCPClient;
    FIndyServer:TIdTCPServer;
    FOnStreamRequest:TRecvStreamRequestEvent;
    FOnStringRequest:TRecvStringRequestEvent;
    FOnStatusRequest:TRecvStatusRequestEvent;
    FOnStreamRecv:TRecvStreamEvent;
    FOnStringRecv:TRecvStringEvent;
    FOnStatusRecv:TRecvStatusEvent;
    FCreateDumpFile: Boolean;
    FDumpPath:string;
    FOwnName:string;
    procedure IndyServerExecute(AContext: TIdContext);
    procedure Dump(const ADumpTxt:string;ADumpString:TStrings=nil;ADumpStream:TStream=nil);
    function  GetListenPort:Word;
  public
    constructor Create(AOwner:TComponent);override;
    destructor  Destroy;override;
    function    Activate(const APort:Word):Boolean;
    function    RequestStream(const AHost:string;const APort:Word;AReqID:Cardinal;var AStream:TStream;const ATimeOut:Cardinal=0):Boolean;
    function    RequestString(const AHost:string;const APort:Word;AReqID:Cardinal;var AString:TStrings;const ATimeOut:Cardinal=0):Boolean;
    function    RequestStatus(const AHost:string;const APort:Word;AReqID:Cardinal;var AStatusID:Cardinal;const ATimeOut:Cardinal=0):Boolean;
    procedure   Deactivate;
    function    IsActive(const AHost:string;const APort:Word;const ATimeOut:Cardinal=0):Boolean;
    property    OnStreamRequest:TRecvStreamRequestEvent read FOnStreamRequest write FOnStreamRequest;
    property    OnStringRequest:TRecvStringRequestEvent read FOnStringRequest write FOnStringRequest;
    property    OnStatusRequest:TRecvStatusRequestEvent read FOnStatusRequest write FOnStatusRequest;
    property    OnStreamRecv:TRecvStreamEvent read FOnStreamRecv write FOnStreamRecv;
    property    OnStringRecv:TRecvStringEvent read FOnStringRecv write FOnStringRecv;
    property    OnStatusRecv:TRecvStatusEvent read FOnStatusRecv write FOnStatusRecv;
    property    CreateDumpFile: Boolean read FCreateDumpFile write FCreateDumpFile;
    property    DumpPath: string read FDumpPath write FDumpPath;
    property    ListenPort:Word read GetListenPort;
  end;

implementation

const _SEPERATOR_ = string('|');

var commCS:TCriticalSection;

function strTok(var s:string):string;
begin
  result:=Copy(s,1,Pos(_SEPERATOR_,s)-1);
  s:=Copy(s,Pos(_SEPERATOR_,s)+1,Length(s));
end;

{ TCommunication }

constructor TCommunication.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FOnStreamRequest:=nil;
  FOnStringRequest:=nil;
  FOnStatusRequest:=nil;
  FOnStreamRecv:=nil;
  FOnStringRecv:=nil;
  FOnStatusRecv:=nil;
  FCreateDumpFile:=false;
  FDumpPath:='';
  FOwnName:=GetEnvironmentVariable('COMPUTERNAME');
  FIndyClient:=TIdTCPClient.Create(nil);
  FIndyServer:=TIdTCPServer.Create(nil);
  FIndyServer.OnExecute:=IndyServerExecute;
end;

destructor TCommunication.Destroy;
begin
  if FIndyClient.Connected then FIndyClient.Disconnect;
  if FIndyServer.Active    then FIndyServer.Active:=false;
  if Assigned(FIndyClient) then FreeAndNil(FIndyClient);
  if Assigned(FIndyServer) then FreeAndNil(FIndyServer);
  inherited Destroy;
end;

procedure TCommunication.Dump(const ADumpTxt:string;ADumpString:TStrings=nil;ADumpStream:TStream=nil);
var fs   : TFileStream;
    fn   : string;
    sHlp : string;
    bHlp : Boolean;
    dtHlp: TDateTime;
begin
  if not FCreateDumpFile then Exit;
  TThread.CreateAnonymousThread
  (procedure
    function prepare(const AString:string):string;inline;
    begin
      result:=AString;
      while Pos('  ',result)>0 do result:=StringReplace(result,'  ',' ',[rfReplaceAll]);
      while Pos(sLineBreak+sLineBreak,result)>0 do result:=StringReplace(result,sLineBreak+sLineBreak,sLineBreak,[rfReplaceAll]);
    end;
  begin
    try
      if not Assigned(commCS) then Exit;
      commCS.Enter;
      fs:=nil;
      sHlp:='';
      fn:=IncludeTrailingPathDelimiter(FDumpPath)+FormatDateTime('YYYYMMDD',Now)+'.comm.dump';
      if FileExists(fn) then begin
        dtHlp:=Now+EncodeTime(0,0,0,500);
        repeat
          try
            fs:=TFileStream.Create(fn, fmOpenWrite or fmShareDenyWrite);
            bHlp:=true;
          except
            bHlp:=false;
          end;
        until bHlp or (Now>dtHlp);
      end else begin
        fs:=TFileStream.Create(fn,fmCreate or fmShareDenyWrite);
        bHlp:=true;
      end;
      if bHlp then begin
        fs.Position:=fs.Size;
        sHlp:='\\ --- '+FormatDateTime('HH:NN:SS(ZZZ)',Now)+' --- '+FOwnName+' --- //'+sLineBreak+prepare(ADumpTxt)+sLineBreak;
        fs.WriteBuffer(Pointer(sHlp)^, Length(sHlp)*SizeOf(sHlp[1]));
        if Assigned(ADumpStream) then begin
          ADumpStream.Position:=0;
          sHlp:='STREAM:'+sLineBreak;
          fs.WriteBuffer(Pointer(sHlp)^, Length(sHlp)*SizeOf(sHlp[1]));
          fs.CopyFrom(ADumpStream,ADumpStream.Size);
          sHlp:=sLineBreak;
          fs.WriteBuffer(Pointer(sHlp)^, Length(sHlp)*SizeOf(sHlp[1]));
        end;
        if Assigned(ADumpString) then begin
          sHlp:='STRING:'+sLineBreak+ADumpString.Text;
          fs.WriteBuffer(Pointer(sHlp)^, Length(sHlp)*SizeOf(sHlp[1]));
          sHlp:=sLineBreak;
          fs.WriteBuffer(Pointer(sHlp)^, Length(sHlp)*SizeOf(sHlp[1]));
        end;
      end;
    finally
      try
        if Assigned(fs) then FreeAndNil(fs);
      except end;
      commCS.Leave;
    end;
  end).Start;
end;

function TCommunication.GetListenPort:Word;
begin
  result:=FIndyServer.DefaultPort;
end;

function TCommunication.Activate(const APort:Word):Boolean;
begin
  try
    FIndyServer.DefaultPort:=APort;
    FIndyServer.Active:=true;
    result:=FIndyServer.Active;
  except
    result:=false;
  end;
  Dump('Activate( '+IntToStr(APort)+' ) := '+BoolToStr(result,true)+'; ');
end;

procedure TCommunication.Deactivate;
begin
  FIndyServer.Active:=false;
  Dump('Deactivate();');
end;

function TCommunication.IsActive(const AHost:string;const APort:Word;const ATimeOut:Cardinal): Boolean;
var h:TCommHeader;
begin
  result:=false;
  h.RecvType:=rtRequestReply;
  h.CompName:=FOwnName;
  h.TimeStamp:=Now;
  h.RequestID:=0;
  h.Size:=0;
  FIndyClient.Host:=AHost;
  FIndyClient.Port:=APort;
  FIndyClient.ConnectTimeout:=ATimeOut;
  FIndyClient.ReadTimeout:=ATimeOut;
  try
    try
      if FIndyClient.Connected then FIndyClient.Disconnect;
      FIndyClient.Connect;
      if FIndyClient.Connected then begin
        FIndyClient.Socket.WriteLn(h.ToString);
        Sleep(1);
        h.FromString(FIndyClient.Socket.ReadLn);
        if (h.RecvType<>rtReply)or
           (h.Size<>0) then raise Exception.Create('unexpected answer!');
        result:=true;
      end;
    except
      result:=false;
    end;
  finally
    if FIndyClient.Connected then FIndyClient.Disconnect;
    Dump('IsActive( "'+AHost+'", '+IntToStr(ATimeOut)+') := '+BoolToStr(result,true)+'; ')
  end;
end;

function TCommunication.RequestStatus(const AHost:string;const APort:Word;AReqID:Cardinal;var AStatusID:Cardinal;const ATimeOut:Cardinal):Boolean;
var h:TCommHeader;
begin
  result:=false;
  h.RecvType:=rtStatusRequest;
  h.CompName:=FOwnName;
  h.TimeStamp:=Now;
  h.RequestID:=AReqID;
  h.Size:=0;
  FIndyClient.Host:=AHost;
  FIndyClient.Port:=APort;
  FIndyClient.ConnectTimeout:=ATimeOut;
  FIndyClient.ReadTimeout:=ATimeOut;
  try
    if FIndyClient.Connected then FIndyClient.Disconnect;
    FIndyClient.Connect;
    if FIndyClient.Connected then begin
      FIndyClient.Socket.WriteLn(h.ToString);
      Sleep(1);
      h.FromString(FIndyClient.Socket.ReadLn);
      if (h.RecvType<>rtStatus)or
         (h.Size<>1) then raise Exception.Create('unexpected answer!');
      AStatusID:=StrToInt(FIndyClient.Socket.ReadLn);
      result:=true
    end;
  finally
    Dump('RequestStatus( "'+AHost+'", '+IntToStr(AReqID)+', [out]'+IntToStr(AStatusID)+', '+IntToStr(ATimeOut)+') := '+BoolToStr(result,true)+'; ');
    if FIndyClient.Connected then FIndyClient.Disconnect;
  end;
end;

function TCommunication.RequestStream(const AHost:string;const APort:Word;AReqID:Cardinal;var AStream:TStream;const ATimeOut:Cardinal):Boolean;
var h:TCommHeader;
begin
  result:=false;
  h.RecvType:=rtStreamRequest;
  h.CompName:=FOwnName;
  h.TimeStamp:=Now;
  h.RequestID:=AReqID;
  h.Size:=0;
  FIndyClient.Host:=AHost;
  FIndyClient.Port:=APort;
  FIndyClient.ConnectTimeout:=ATimeOut;
  FIndyClient.ReadTimeout:=ATimeOut;
  try
    if FIndyClient.Connected then FIndyClient.Disconnect;
    FIndyClient.Connect;
    if FIndyClient.Connected then begin
      FIndyClient.Socket.WriteLn(h.ToString);
      Sleep(1);
      h.FromString(FIndyClient.Socket.ReadLn);
      if (h.RecvType<>rtStream) then raise Exception.Create('unexpected answer!');
      if not Assigned(AStream) then AStream:=TMemoryStream.Create;
      FIndyClient.Socket.ReadStream(AStream,h.Size);
      AStream.Position:=0;
      result:=true;
    end;
  finally
    Dump('RequestStream( "'+AHost+'", '+IntToStr(AReqID)+', [out,stream.size]'+IntToStr(AStream.Size)+', '+IntToStr(ATimeOut)+') := '+BoolToStr(result,true)+'; ',nil,AStream);
    if FIndyClient.Connected then FIndyClient.Disconnect;
  end;
end;

function TCommunication.RequestString(const AHost:string;const APort:Word;AReqID:Cardinal;var AString:TStrings;const ATimeOut:Cardinal):Boolean;
var h:TCommHeader;
    i:Integer;
begin
  result:=false;
  h.RecvType:=rtStringRequest;
  h.CompName:=FOwnName;
  h.TimeStamp:=Now;
  h.RequestID:=AReqID;
  h.Size:=0;
  FIndyClient.Host:=AHost;
  FIndyClient.Port:=APort;
  FIndyClient.ConnectTimeout:=ATimeOut;
  FIndyClient.ReadTimeout:=ATimeOut;
  try
    if FIndyClient.Connected then FIndyClient.Disconnect;
    FIndyClient.Connect;
    if FIndyClient.Connected then begin
      FIndyClient.Socket.WriteLn(h.ToString);
      Sleep(1);
      h.FromString(FIndyClient.Socket.ReadLn);
      if (h.RecvType<>rtString) then raise Exception.Create('unexpected answer!');
      if not Assigned(AString) then AString:=TStringList.Create;
      AString.Clear;
      for i:=0 to h.Size-1 do AString.Add(FIndyClient.Socket.ReadLn);
      result:=true;
    end;
  finally
    Dump('RequestString( "'+AHost+'", '+IntToStr(AReqID)+', [out,strings.count]'+IntToStr(AString.Count)+', '+IntToStr(ATimeOut)+') := '+BoolToStr(result,true)+'; '+sLineBreak+'String:',AString);
    if FIndyClient.Connected then FIndyClient.Disconnect;
  end;
end;

procedure TCommunication.IndyServerExecute(AContext: TIdContext);
var h:TCommHeader;
    strm:TStream;
    strng:TStrings;
    i:Integer;
    stat:Cardinal;
begin
  strm:=nil;
  strng:=nil;
  stat:=0;
  h.FromString('');

  h.FromString(AContext.Connection.Socket.ReadLn);
  Dump('IndyServerExecute recv header='+h.ToString);
  case h.RecvType of
    rtRequestReply: begin
      h.RecvType:=rtReply;
      h.CompName:=FOwnName;
      h.TimeStamp:=Now;
      h.Size:=0;
      AContext.Connection.Socket.WriteLn(h.ToString);
      Dump('IndyServerExecute transmit header='+h.ToString);
    end;
    rtStreamRequest: begin
      strm:=TMemoryStream.Create;
      if Assigned(FOnStreamRequest) then FOnStreamRequest(h.RequestID,strm);
      strm.Position:=0;
      h.RecvType:=rtStream;
      h.CompName:=FOwnName;
      h.TimeStamp:=Now;
      h.Size:=strm.Size;
      AContext.Connection.Socket.WriteLn(h.ToString);
      AContext.Connection.Socket.Write(strm,strm.Size,false);
      Dump('IndyServerExecute transmit header='+h.ToString);
    end;
    rtStringRequest: begin
      strng:=TStringList.Create;
      if Assigned(FOnStringRequest) then FOnStringRequest(h.RequestID,strng);
      h.RecvType:=rtString;
      h.CompName:=FOwnName;
      h.TimeStamp:=Now;
      h.Size:=strng.Count;
      AContext.Connection.Socket.WriteLn(h.ToString);
      for i:=0 to strng.Count-1 do AContext.Connection.Socket.WriteLn(strng.Strings[i]);
      Dump('IndyServerExecute transmit header='+h.ToString);
    end;
    rtStatusRequest: begin
      if Assigned(FOnStatusRequest) then FOnStatusRequest(h.RequestID,stat);
      h.RecvType:=rtStatus;
      h.CompName:=FOwnName;
      h.TimeStamp:=Now;
      h.Size:=1;
      AContext.Connection.Socket.WriteLn(h.ToString);
      AContext.Connection.Socket.WriteLn(IntToStr(stat));
      Dump('IndyServerExecute transmit header='+h.ToString);
    end;
    rtReply: begin
      //
    end;
    rtStream: begin
      strm:=TMemoryStream.Create;
      AContext.Connection.Socket.ReadStream(strm,h.Size,false);
      strm.Position:=0;
      if Assigned(FOnStreamRecv) then FOnStreamRecv(strm);
    end;
    rtString: begin
      strng:=TStringList.Create;
      for i:=0 to h.Size-1 do strng.Add(AContext.Connection.Socket.ReadLn);
      if Assigned(FOnStringRecv) then FOnStringRecv(strng);
    end;
    rtStatus: begin
      stat:=StrToIntDef(AContext.Connection.Socket.ReadLn,0);
      if Assigned(FOnStatusRecv) then FOnStatusRecv(stat);
    end;
  end;
  strm.Free;
  strng.Free;
end;


{ TCommHeader }

procedure TCommHeader.FromString(const AString: string);
var sHlp:string;
  function dt(const s:string):TDateTime;
  begin
    try
      result:=EncodeDate(
        StrToIntDef(Copy(s,1,4),1900), //YYYY
        StrToIntDef(Copy(s,5,2),1),    //MM
        StrToIntDef(Copy(s,7,2),1)     //DD
      )+EncodeTime(
        StrToIntDef(Copy(s,9,2),0),    //HH
        StrToIntDef(Copy(s,11,2),0),   //NN
        StrToIntDef(Copy(s,13,2),0),   //SS
        StrToIntDef(Copy(s,15,3),0)    //ZZZ
      );
    except
      result:=EncodeDate(1900,1,1)+EncodeTime(0,0,0,0);
    end;
  end;
begin
  sHlp:=AString;
  RecvType:=TRecvType(GetEnumValue(TypeInfo(TRecvType),strTok(sHlp)));
  CompName:=strTok(sHlp);
  TimeStamp:=dt(strTok(sHlp));
  RequestID:=StrToIntDef(strTok(sHlp),0);
  Size:=StrToInt64Def(strTok(sHlp),0);
end;

function TCommHeader.ToString: string;
begin
  result:=GetEnumName(TypeInfo(TRecvType),Integer(RecvType))+_SEPERATOR_+
          CompName+_SEPERATOR_+
          FormatDateTime('YYYYMMDDHHNNSSZZZ',TimeStamp)+_SEPERATOR_+
          IntToStr(RequestID)+_SEPERATOR_+
          IntToStr(Size)+_SEPERATOR_;
end;

initialization
  commCS:=TCriticalSection.Create;
finalization
  commCS.Free;
  commCS:=nil;

end.
