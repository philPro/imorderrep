unit DataModul.Core;

interface

uses
  ZAbstractConnection, ZConnection, Vcl.Forms, System.UITypes, System.SysUtils,
  ZAbstractRODataset, ZAbstractDataset, ZDataset, Generics.Collections, Dialogs,
  System.IniFiles, Spring.Container,
  DataModul.Errors, DataModul.Interfaces;

type
  TDBConnectionSettings = record
    FsDatabase: String;
    FsHostname: String;
    FsLibraryLocation: String;
    FiPort: Integer;
    FsUser: String;
    FsPassword: String;
  end;

type
  TDataModulCore = class(TInterfacedObject, IDataModulCore)
  private
    FDBConnection: TDBConnectionSettings;
    FIMOrderSession: TZConnection;
    function ReadDBSettings: TDBConnectionSettings;
  public
    constructor Create;
    destructor Destroy; override;
    function ConnectToDB: Boolean;
    procedure DisconnectDB;
    function Connected: Boolean;
    function GetSession: PZConnection;
  end;

implementation

constructor TDataModulCore.Create;
begin
  inherited Create;
  FIMOrderSession := TZConnection.Create(nil);
end;

destructor TDataModulCore.Destroy;
begin
  DisconnectDB;
  if Assigned(FIMOrderSession) then FIMOrderSession.Free;
  inherited;
end;

function TDataModulCore.ConnectToDB: Boolean;
begin
  if FIMOrderSession.Connected then
  begin
    Result := True;
    Exit;
  end;

  Screen.Cursor := crHourGlass;
  FDBConnection := ReadDBSettings;

  try
    FIMOrderSession.Database := FDBConnection.FsDatabase;
    FIMOrderSession.HostName := FDBConnection.FsHostname;
    FIMOrderSession.LibraryLocation := FDBConnection.FsLibraryLocation;
    FIMOrderSession.Port := FDBConnection.FiPort;
    FIMOrderSession.User := FDBConnection.FsUser;
    FIMOrderSession.Password := FDBConnection.FsPassword;
    FIMOrderSession.Protocol := 'mysql';

    FIMOrderSession.Connect;
  finally
    Screen.Cursor := crDefault;
  end;

  Result := FIMOrderSession.Connected;
end;

procedure TDataModulCore.DisconnectDB;
begin
  if FIMOrderSession.Connected then FIMOrderSession.Disconnect;
end;

function TDataModulCore.ReadDBSettings: TDBConnectionSettings;
const
  DB_SEC = 'DBCONNECTION';
var
  DBIni: TIniFile;
begin
  DBIni := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    Result.FsDatabase := DBIni.ReadString(DB_SEC, 'Database', 'im_order');
    Result.FsHostname := DBIni.ReadString(DB_SEC, 'Hostname', 'localhost');
    Result.FsLibraryLocation := DBIni.ReadString(DB_SEC, 'LibPath', 'C:\Program Files (x86)\MySQL\MySQL Workbench 6.0 CE\libmysql.dll');
    Result.FiPort := DBIni.ReadInteger(DB_SEC, 'Port', 3306);
    Result.FsUser := DBIni.ReadString(DB_SEC, 'User', 'root');
    Result.FsPassword := DBIni.ReadString(DB_SEC, 'Password', '(123-masterOf99!)');
  finally
    DBIni.Free;
  end;
end;

function TDataModulCore.Connected: Boolean;
begin
  Result := FIMOrderSession.Connected;
end;

function TDataModulCore.GetSession: PZConnection;
begin
  Result := @FIMOrderSession;
end;

initialization
  GlobalContainer.RegisterType<TDataModulCore>.Implements<IDataModulCore>('DataModulCore');


end.
