unit Communication.TCP.Indy;

interface
uses
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdCustomTCPServer,
  IdTCPServer, IdContext, Classes, System.SysUtils, System.SyncObjs, System.TypInfo,
  System.Generics.Collections, Spring.Services, Spring.Container,
  { Own libs }
  Communication.Interfaces, Communication.Errors, IMOServer.Settings.Interfaces;

type
  TContextData = class
  public
    XBeeHexID: String;
  end;

type
  TCommunicationTCPIndy = class(TInterfacedObject, ICommunicationDriver)
  private
    FIndyServer:TIdTCPServer;
    FEvMessageReceived: TMessageReceivedEvent;
    [Inject('SettingsGlobal')]
    FGlobalSettings: ISettingsGlobal;
    procedure IndyServerExecute(AContext: TIdContext);
    procedure IndyServerConnect(AContext: TIdContext);
    procedure IndyServerDisconnect(AContext: TIdContext);
    function GetContextByHexID(const aXBeeHexID: String; var aContext: TIdContext): Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    { ICommunicationDriver }
    function InitializeMaster: Boolean; //Slave
    procedure DisconnectMaster;
    function MasterConnected: Boolean;
    procedure SetMessageReceivedEvent(aMessageReceivedEvent: TMessageReceivedEvent);
    function  GetMessageReceivedEvent: TMessageReceivedEvent;
    procedure SendMessage(const aMessage: String);
    property  OnMessageReceived: TMessageReceivedEvent read GetMessageReceivedEvent write SetMessageReceivedEvent;
  end;

implementation

var commCS:TCriticalSection;

{ TCommunication }

constructor TCommunicationTCPIndy.Create;
begin
  inherited Create;
  FIndyServer := TIdTCPServer.Create(nil);
  FIndyServer.OnExecute := IndyServerExecute;
  FIndyServer.OnConnect := IndyServerConnect;
  FIndyServer.OnDisconnect := IndyServerDisconnect;
end;

destructor TCommunicationTCPIndy.Destroy;
begin
  if FIndyServer.Active then FIndyServer.Active:=False;
  if Assigned(FIndyServer) then FreeAndNil(FIndyServer);
  inherited Destroy;
end;

function TCommunicationTCPIndy.GetMessageReceivedEvent: TMessageReceivedEvent;
begin
  Result := FEvMessageReceived;
end;

procedure TCommunicationTCPIndy.IndyServerExecute(AContext: TIdContext);
var
  sClientMsg: String;
  CommProt: ICommunicationProtocol;
begin
  sClientMsg := AContext.Connection.Socket.ReadLn;

  if Length(sClientMsg) > 5 then
  begin
    if Pos('LOGON', sClientMsg) > 0 then
    begin
      sClientMsg := Copy(sClientMsg, 7, Length(sClientMsg));
      CommProt := ServiceLocator.GetService<ICommunicationProtocol>;
      CommProt.FromSendString(sClientMsg);
      TContextData(AContext.Data).XBeeHexID := CommProt.TableID.XBeeHexID;
      Exit;
    end;
    if Pos('DISCONNECT', sClientMsg) > 0 then
    begin
      AContext.Connection.Disconnect;
      Exit;
    end;
  end;

  if Assigned(FEvMessageReceived) then FEvMessageReceived(sClientMsg);
end;

procedure TCommunicationTCPIndy.IndyServerConnect(AContext: TIdContext);
begin
  AContext.Data := TContextData.Create;
end;

procedure TCommunicationTCPIndy.IndyServerDisconnect(AContext: TIdContext);
begin
  AContext.Data.Free;
  AContext.Data := nil;
end;

function TCommunicationTCPIndy.GetContextByHexID(const aXBeeHexID: String; var aContext: TIdContext): Boolean;
var
  I: Integer;
  List: TList;
begin
  Result := False;

  List := FIndyServer.Contexts.LockList;
  try
    for I := 0 to List.Count-1 do
    begin
      if TContextData(TIdContext(List.Items[I]).Data).XBeeHexID = aXBeeHexID then
      begin
        aContext := TIdContext(List.Items[I]);
        Result := True;
        Break;
      end;
    end;
  finally
    FIndyServer.Contexts.UnlockList;
  end;
end;

function TCommunicationTCPIndy.InitializeMaster: Boolean;
begin
  try
    FIndyServer.DefaultPort:=60001; // GetFromSettings
    FIndyServer.Active:=true;
    result:=FIndyServer.Active;
  except
    result:=false;
  end;
end;

procedure TCommunicationTCPIndy.DisconnectMaster;
begin
  FIndyServer.Active := False;
end;

function TCommunicationTCPIndy.MasterConnected: Boolean;
begin
  Result := FIndyServer.Active;
end;

procedure TCommunicationTCPIndy.SendMessage(const aMessage: String);
var
  CommProt: ICommunicationProtocol;
  HlpContext: TIdContext;
begin
  // Dieser INDY-Server ist jetzt im Prinzip der Master-Arduino welcher
  // mit den Tisch-Clients kommuniziert
  CommProt := ServiceLocator.GetService<ICommunicationProtocol>;

  CommProt.FromSendString(aMessage);
  if GetContextByHexID(CommProt.TableID.XBeeHexID, HlpContext) then
  begin
    HlpContext.Connection.Socket.WriteLn(CommProt.ToSendString);
  end else
  begin
    raise ECommunicationError.Create('TCP-Client with HEX-ID: ' + CommProt.TableID.XBeeHexID + ' not connected yet!');
  end;
end;

procedure TCommunicationTCPIndy.SetMessageReceivedEvent(
  aMessageReceivedEvent: TMessageReceivedEvent);
begin
  FEvMessageReceived := aMessageReceivedEvent;
end;

initialization
  commCS:=TCriticalSection.Create;
  GlobalContainer.RegisterType<TCommunicationTCPIndy>.Implements<ICommunicationDriver>('CommunicationTCPIndy').AsSingleton;

finalization
  commCS.Free;
  commCS:=nil;

end.
