unit Communication.Core;

interface

uses
  Spring.Services, Spring.Container, System.Classes, System.SysUtils,
  DataModul.Interfaces, Communication.Interfaces;

type
  TCommunicationCore = class(TInterfacedObject, ICommunicationCore)
  private
    [Inject('DataModulCore')]
    FDataModul: IDataModulCore;
    [Inject('CommunicationTIConn')]
    FTIConn: ICommunicationTIConn;
    [Inject('DataModulMeetings')]
    FMeetingList: IDataModulMeetings;
    [Inject('DataModulTableIdentifiers')]
    FTIList: IDataModulTableIdentifiers;
    procedure CheckTIsActive;
    procedure AssignQRCode(aQRCode: IDataModulQRCode);
    procedure TIConnectionStateChanged(aTableIdentifier: IDataModulTableIdentifier; aReply: TReply);
  public
    constructor Create;
    function Initialize: Boolean;
    procedure DeliverAllTIs;
    procedure SetAllActiveTI;
  end;

implementation

procedure TCommunicationCore.CheckTIsActive;
var
  iTIIdx: Integer;
  iMeetIdx: Integer;
  hlpTI: IDataModulTableIdentifier;
begin
  for iTIIdx := 0 to FTIList.Count-1 do
  begin
    hlpTI := FTIList.GetItem(iTIIdx);
    for iMeetIdx := 0 to FMeetingList.GetActiveCount-1 do
    begin
      if hlpTI.ID = FMeetingList.GetActiveItem(iMeetIdx).TableIdentifier.ID then
        hlpTI.Active := True;
    end;
  end;
end;

procedure TCommunicationCore.AssignQRCode(aQRCode: IDataModulQRCode);
begin
  aQRCode.ID := 0;
  aQRCode.QRText := FormatDateTime('c', Now);
  aQRCode.PIN := IntToStr(Random(9001) + 1000);
end;

constructor TCommunicationCore.Create;
begin
  Randomize;
end;

function TCommunicationCore.Initialize: Boolean;
begin
  Result := False;

  if FDataModul.ConnectToDB then
  begin
    FTIConn.OnConnectionStateChanged := TIConnectionStateChanged;
    FTIConn.InitializeConnection;

    FMeetingList.DataModulCore := FDataModul;
    FTIList.DataModulCore := FDataModul;

    Result := FMeetingList.LoadData;
    Result := Result AND FTIList.LoadData;

    if Result then CheckTIsActive;
  end;
end;

procedure TCommunicationCore.DeliverAllTIs;
var
  iTIIdx: Integer;
  hlpTI: IDataModulTableIdentifier;
begin
  for iTIIdx := 0 to FTIList.Count-1 do
  begin
    hlpTI := FTIList.GetItem(iTIIdx);
    if not hlpTI.Active then AssignQRCode(hlpTI.QRCode);

    FTIConn.SendQRCodeToTI(hlpTI);
  end;
end;

procedure TCommunicationCore.SetAllActiveTI;
var
  iMeetIdx: Integer;
begin
  for iMeetIdx := 0 to FMeetingList.GetActiveCount-1 do
  begin
      FTIConn.SendQRCodeToTI(FMeetingList.GetActiveItem(iMeetIdx).TableIdentifier);
  end;
end;

procedure TCommunicationCore.TIConnectionStateChanged(
  aTableIdentifier: IDataModulTableIdentifier; aReply: TReply);
begin
  if aReply in [repDataTransmitted] then
  begin
    // Save Data in DB
    FTIList.SaveData(aTableIdentifier);
  end;
end;

initialization
  GlobalContainer.RegisterType<TCommunicationCore>.Implements<ICommunicationCore>('CommunicationCore');

end.
