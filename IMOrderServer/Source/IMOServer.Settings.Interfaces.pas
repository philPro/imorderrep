unit IMOServer.Settings.Interfaces;

interface

type
  ISettingsGlobal = interface(IInterface)
    ['{B3704CB9-86FE-4727-844A-EB64EC972CEF}']
    function IsSimulationUsed: Boolean;
    procedure SetSimulationUsed(aValue: Boolean);
    function GetGlobSettingsFileName: String;
    procedure LoadSettings;
    procedure StoreSettings;
    property SimUsed: Boolean read IsSimulationUsed write SetSimulationUsed;
    property SettingsFileName: String read GetGlobSettingsFileName;
  end;

implementation

end.
