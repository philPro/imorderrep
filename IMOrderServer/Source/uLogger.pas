(******************************************************************************)
(*                                                                            *)
(*                                                                            *)
(*  Logger f�r die Aufzeichnung von �nderungen - wird in DB gespeichert       *)
(*                                                                            *)
(*                                                                            *)
(*       Programmierer: pHL                                     Mai 2013      *)
(*                                                                            *)
(*       Last Update: MAY-27, 2013                                            *)
(*                                                                            *)
(*******************************************************************************
                          revision history
(*******************************************************************************

27-05-2013  Implementierung
PP          ====================================================================

18-07-2013  Nur Loggen wenn nicht im Debug-Modus
PP          ====================================================================
            o RunningInDelphi hinzugef�gt
            o AddLogEntry angepasst
            o AddInstantLog hinzugef�gt

*******************************************************************************)
unit uLogger;

{------------------------------------------------------------------------------}
interface
{------------------------------------------------------------------------------}

uses
  Generics.Collections, Windows, SysUtils, Dialogs, Ora, Forms, Controls,
  uDBUniversal;

type
{------------------------------------------------------------------------------}
  TLogEntry = class
{------------------------------------------------------------------------------}
    ChangeText:   String;
    UserName:     String;
    Time:         TDateTime;
  end;

type
{ Singleton }
{------------------------------------------------------------------------------}
  TLogger = class sealed
{------------------------------------------------------------------------------}
  private
    FOraUniversal: TOraUniversal;
    FOraSessionLog: TOraSession;
    FolLogEntries: TObjectList<TLogEntry>;
    class var Logger: TLogger;
    class var AllowFree: Boolean;
    constructor Create;
    function RunningInDelphi: Boolean;
  public
    procedure AddLogEntry(parText: string; parUser: String);
    procedure SaveLogToDB;
    procedure AddInstantLog(parText: String; parUser: String);
    destructor Destroy; override;
    procedure FreeInstance; override;
    class function GetInstance: TLogger;
  end;

{------------------------------------------------------------------------------}
implementation
{------------------------------------------------------------------------------}

{------------------------------------------------------------------------------}
constructor TLogger.Create;
{------------------------------------------------------------------------------}
begin
  FolLogEntries := TObjectList<TLogEntry>.Create;
  FOraUniversal := TOraUniversal.Create;
end;

{------------------------------------------------------------------------------}
destructor TLogger.Destroy;
{------------------------------------------------------------------------------}
begin
  FreeAndNil(FolLogEntries);
  FreeAndNil(FOraUniversal);
end;

{------------------------------------------------------------------------------}
function TLogger.RunningInDelphi: Boolean;
{------------------------------------------------------------------------------}
begin
  Result := DebugHook <> 0;
end;

{------------------------------------------------------------------------------}
procedure TLogger.AddLogEntry(parText: string; parUser: string);
{------------------------------------------------------------------------------}
var
  hlpEntry: TLogEntry;
begin
  { Nicht loggen wenn debuggt wird }
  if RunningInDelphi then Exit;

  hlpEntry := TLogEntry.Create;

  hlpEntry.ChangeText := parText;
  hlpEntry.UserName := parUser;
  hlpEntry.Time := Now;

  if Assigned(FolLogEntries) then FolLogEntries.Add(hlpEntry)
  else hlpEntry.Free;
end;

{------------------------------------------------------------------------------}
class function TLogger.GetInstance: TLogger;
{------------------------------------------------------------------------------}
var
  Log: TLogger;
begin
  if not Assigned(Logger) then
  begin
    Log := TLogger.Create;
    if Assigned(InterlockedCompareExchangePointer(Pointer(Logger), Pointer(Log), nil)) then
      Log.Free;
  end;
  Result := Logger;
end;

{------------------------------------------------------------------------------}
procedure TLogger.FreeInstance;
{------------------------------------------------------------------------------}
begin
  if AllowFree then
    inherited FreeInstance;
end;

{------------------------------------------------------------------------------}
procedure TLogger.SaveLogToDB;
{------------------------------------------------------------------------------}
var
  OraQueryLook: TOraQuery;
  hlpEntry: TLogEntry;
  I: Integer;
begin
  if FOraUniversal.PingDB then
  begin
    if not FOraUniversal.OpenOraSession(FOraSessionLog) then Exit;
    Screen.Cursor := crSQLWait;
    try
      OraQueryLook := TOraQuery.Create(nil);
      try
        OraQueryLook.Session := FOraSessionLog;

        for I := 0 to FolLogEntries.Count-1 do
        begin
          hlpEntry := FolLogEntries.Items[I];
          OraQueryLook.SQL.Clear;
          OraQueryLook.SQL.Add('INSERT INTO T_SSV_LOG (ID, CHANGE, ' +
                                  'USERSHORT, LOGDATE) VALUES (' +
                                  '1, ''' + // AutoInc DB
                                  hlpEntry.ChangeText + ''', ''' +
                                  hlpEntry.UserName + ''', ' +
                                  'TO_DATE(''' + FormatDateTime('dd.mm.yyyy hh:nn:ss', hlpEntry.Time) + ''',''dd.mm.yyyy HH24:MI:SS'')' + ')');
          OraQueryLook.Execute;
        end;
        FolLogEntries.Clear;
      finally
        OraQueryLook.Close;
      end;
    finally
      FOraSessionLog.Close;
      Screen.Cursor := crDefault;
    end;
  end;
end;

{------------------------------------------------------------------------------}
procedure TLogger.AddInstantLog(parText: string; parUser: String);
{------------------------------------------------------------------------------}
var
  OraQueryLook: TOraQuery;
begin
  { Nicht loggen wenn debuggt wird }
  if RunningInDelphi then Exit;

  if FOraUniversal.PingDB then
  begin
    if not FOraUniversal.OpenOraSession(FOraSessionLog) then Exit;
    Screen.Cursor := crSQLWait;
    try
      OraQueryLook := TOraQuery.Create(nil);
      try
        OraQueryLook.Session := FOraSessionLog;

        OraQueryLook.SQL.Clear;
        OraQueryLook.SQL.Add('INSERT INTO T_SSV_LOG (ID, CHANGE, ' +
                             'USERSHORT, LOGDATE) VALUES (' +
                             '1, ''' + // AutoInc DB
                             parText + ''', ''' +
                             parUser + ''', ' +
                             'TO_DATE(''' + FormatDateTime('dd.mm.yyyy hh:nn:ss', Now) + ''',''dd.mm.yyyy HH24:MI:SS'')' + ')');
        OraQueryLook.Execute;
      finally
        OraQueryLook.Close;
      end;
    finally
      FOraSessionLog.Close;
      Screen.Cursor := crDefault;
    end;
  end;
end;

{ Lets test SVN :) }

{------------------------------------------------------------------------------}
initialization
  TLogger.AllowFree := False;

{------------------------------------------------------------------------------}
finalization
  TLogger.AllowFree := True;
  TLogger.Logger.Free;

end.
