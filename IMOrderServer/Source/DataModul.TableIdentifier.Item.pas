unit DataModul.TableIdentifier.Item;

interface

uses
  Spring.Container, Spring.Services,
  DataModul.Interfaces;

type
  TTableIdentifier = class(TInterfacedObject, IDataModulTableIdentifier)
  private
    FID: Integer;
    FName: String;
    FRID: Integer;
    FTableNumber: String;
    [Inject('DataModulQRCode')]
    FQRCode: IDataModulQRCode;
    FXBeeHexID: String;
    { For Server only }
    FConnected: Boolean;
    FActive: Boolean; // on this TI is an active Meeting
  public
    function GetID: Integer;
    function GetName: String;
    function GetRID: Integer;
    function GetTableNumber: String;
    function GetQRCode: IDataModulQRCode;
    function GetXBeeHexID: String;
    function GetConnected: Boolean;
    function GetActive: Boolean;
    procedure SetID(aID: Integer);
    procedure SetName(aName: String);
    procedure SetRID(aRID: Integer);
    procedure SetTableNumber(aTableNumber: String);
    procedure SetQRCode(aQRCode: IDataModulQRCode);
    procedure SetXBeeHexID(aXBeeID: String);
    procedure SetConnected(aConnected: Boolean);
    procedure SetActive(aActive: Boolean);
    property ID: Integer read GetID write SetID;
    property Name: String read GetName write SetName;
    property RID: Integer read GetRID write SetRID;
    property TableNumber: String read GetTableNumber write SetTableNumber;
    property QRCode: IDataModulQRCode read GetQRCode write SetQRCode;
    property XBeeHexID: String read GetXBeeHexID write SetXBeeHexID;
    property Connected: Boolean read GetConnected write SetConnected;
    property Active: Boolean read GetActive write SetActive;
  end;

implementation

{ TTableIdentifier }

function TTableIdentifier.GetConnected: Boolean;
begin
  Result := FConnected;
end;

function TTableIdentifier.GetActive: Boolean;
begin
  Result := FActive;
end;

function TTableIdentifier.GetID: Integer;
begin
  Result := FID;
end;

function TTableIdentifier.GetName: String;
begin
  Result := FName;
end;

function TTableIdentifier.GetQRCode: IDataModulQRCode;
begin
  Result := FQRCode;
end;

function TTableIdentifier.GetRID: Integer;
begin
  Result := FRID;
end;

function TTableIdentifier.GetTableNumber: String;
begin
  Result := FTableNumber;
end;

function TTableIdentifier.GetXBeeHexID: String;
begin
  Result := FXBeeHexID;
end;

procedure TTableIdentifier.SetConnected(aConnected: Boolean);
begin
  FConnected := aConnected;
end;

procedure TTableIdentifier.SetActive(aActive: Boolean);
begin
  FActive := aActive;
end;

procedure TTableIdentifier.SetID(aID: Integer);
begin
  FID := aID;
end;

procedure TTableIdentifier.SetName(aName: String);
begin
  FName := aName;
end;

procedure TTableIdentifier.SetQRCode(aQRCode: IDataModulQRCode);
begin
  FQRCode := aQRCode;
end;

procedure TTableIdentifier.SetRID(aRID: Integer);
begin
  FRID := aRID;
end;

procedure TTableIdentifier.SetTableNumber(aTableNumber: String);
begin
  FTableNumber := aTableNumber;
end;

procedure TTableIdentifier.SetXBeeHexID(aXBeeID: String);
begin
  FXBeeHexID := aXBeeID;
end;

initialization
  GlobalContainer.RegisterType<TTableIdentifier>.Implements<IDataModulTableIdentifier>('DataModulTableIdentifier');

end.
