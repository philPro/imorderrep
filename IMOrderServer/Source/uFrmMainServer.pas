unit uFrmMainServer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls,
  Spring.Services, Communication.Interfaces;

type
  TFrmMainServer = class(TForm)
    Button1: TButton;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private-Deklarationen }
    TIConn: ICommunicationCore;
  public
    { Public-Deklarationen }
  end;

var
  FrmMainServer: TFrmMainServer;

implementation

{$R *.dfm}

procedure TFrmMainServer.Button1Click(Sender: TObject);
begin
  TIConn := ServiceLocator.GetService<ICommunicationCore>('CommunicationCore');
  TIConn.Initialize;
end;

procedure TFrmMainServer.Button2Click(Sender: TObject);
begin
  TIConn.DeliverAllTIs;
end;

end.
