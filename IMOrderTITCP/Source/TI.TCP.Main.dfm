object TCPMainForm: TTCPMainForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'TCPMainForm'
  ClientHeight = 453
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControlMain: TPageControl
    Left = 0
    Top = 0
    Width = 325
    Height = 453
    ActivePage = TabSheetOverview
    Align = alClient
    TabOrder = 0
    object TabSheetOverview: TTabSheet
      Caption = #220'bersicht'
      object LabelLog: TLabel
        Left = 3
        Top = 200
        Width = 21
        Height = 13
        Caption = 'Log:'
      end
      object ButtonConnect: TButton
        Left = 48
        Top = 168
        Width = 209
        Height = 25
        Caption = 'Connect'
        TabOrder = 0
        OnClick = ButtonConnectClick
      end
      object panelStatus: TPanel
        Left = 3
        Top = 16
        Width = 311
        Height = 41
        BevelInner = bvLowered
        Caption = 'Status:'
        ParentBackground = False
        TabOrder = 1
      end
      object RichEditLog: TRichEdit
        Left = 0
        Top = 219
        Width = 317
        Height = 206
        Align = alBottom
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
    object TabSheetSettings: TTabSheet
      Caption = 'Einstellungen'
      ImageIndex = 1
      object LabelXBeeHexID: TLabel
        Left = 16
        Top = 19
        Width = 62
        Height = 13
        Caption = 'XBee-HexID:'
      end
      object EditXBeeHexID: TEdit
        Left = 120
        Top = 16
        Width = 186
        Height = 21
        TabOrder = 0
      end
      object CheckBoxStartupConnect: TCheckBox
        Left = 16
        Top = 80
        Width = 177
        Height = 17
        Caption = 'Connect at Startup?'
        TabOrder = 1
      end
      object ButtonSaveSettings: TButton
        Left = 231
        Top = 392
        Width = 75
        Height = 25
        Caption = 'Save'
        TabOrder = 2
      end
    end
  end
end
