unit TI.TCP.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, Vcl.ExtCtrls,
  { Own-Libs }
  Communication.Interfaces, Communication.Protocol, DataModul.TableIdentifier.Item,
  DataModul.TableIdentifier.QRCode, Vcl.StdCtrls, WinApi.RichEdit;

type
  TOnMsgSync = procedure(const aMsg: String) of Object;
  TOnConnected = procedure(const aConnected: Boolean) of Object;
  TOnInfoSync = procedure(const aMsg: String) of Object;

  TReadLnThread = class(TThread)
  private
    FIdTCPClient: TIdTCPClient;
    FsWholeMsg: String;
    FbReplyStarted: Boolean;
    FOnMsgSync: TOnMsgSync;
    FOnConnected: TOnConnected;
    FOnInfoSync: TOnInfoSync;
    FbLastConnState: Boolean;
    FsInfoText: String;
    FCommProt: ICommunicationProtocol;
  protected
    procedure Execute; override;
    procedure SyncMsg;
    procedure SyncConnected;
    procedure SyncInfo;
    procedure ServerLogOn;
  public
    constructor Create(CreateSuspended: Boolean; aCommProt: ICommunicationProtocol);
    destructor Destroy; override;
    procedure ConnectServer;
    property OnMsgSync: TOnMsgSync write FOnMsgSync;
    property OnConnected: TOnConnected write FOnConnected;
    property OnInfoSync: TOnInfoSync write FOnInfoSync;
  end;

type
  TTCPMainForm = class(TForm)
    PageControlMain: TPageControl;
    TabSheetOverview: TTabSheet;
    TabSheetSettings: TTabSheet;
    ButtonConnect: TButton;
    EditXBeeHexID: TEdit;
    LabelXBeeHexID: TLabel;
    CheckBoxStartupConnect: TCheckBox;
    ButtonSaveSettings: TButton;
    LabelLog: TLabel;
    panelStatus: TPanel;
    RichEditLog: TRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ButtonConnectClick(Sender: TObject);
  private
    { Private-Deklarationen }
    FReadLnThread: TReadLnThread;
    FCommProt: ICommunicationProtocol;
    procedure DoOnMsgSync(const aMsg: String);
    procedure DoOnConnSync(const aConnected: Boolean);
    procedure DoOnInfoSync(const aMsg: String);
    procedure AddLogEntry(const aMsg: String);
  public
    { Public-Deklarationen }
  end;

var
  TCPMainForm: TTCPMainForm;

implementation

{$R *.dfm}

procedure TTCPMainForm.DoOnMsgSync(const aMsg: String);
begin
  FCommProt.FromSendString(aMsg);
  // TODO Msg zur�cksenden
  AddLogEntry('Message empfangen!');
end;

procedure TTCPMainForm.ButtonConnectClick(Sender: TObject);
begin
  FReadLnThread.ConnectServer;
end;

procedure TTCPMainForm.DoOnConnSync(const aConnected: Boolean);
begin
  AddLogEntry('Verbindung zu Server ge�ndert: Verbunden? ' + BoolToStr(aConnected, True));
end;

procedure TTCPMainForm.DoOnInfoSync(const aMsg: string);
begin
  AddLogEntry(aMsg);
end;

procedure TTCPMainForm.AddLogEntry(const aMsg: string);
var
  cf: CHARFORMAT2;
  iSelStart: Integer;
  iSelStop: Integer;
  I: Integer;
  bFoundCnt: Integer;
  bFound: Boolean;
begin
  RichEditLog.Lines.Add('[' + FormatDateTime('hh:nn:ss', Now) + '] ' + aMsg);

  FillChar(cf, SizeOf(cf), 0);
  cf.cbSize := SizeOf(cf);
  cf.dwMask := CFM_BACKCOLOR;
  cf.crBackColor := clSilver;

  iSelStart := 0;
  iSelStop := 0;
  bFoundCnt := 0;

  for I := 1 to Length(RichEditLog.Text) do
  begin
    bFound := False;

    if RichEditLog.Text[I] = '[' then iSelStart := I;
    if RichEditLog.Text[I] = ']' then
    begin
      iSelStop := I;
      Inc(bFoundCnt);
      bFound := True;
    end;

    RichEditLog.SelStart := iSelStart - bFoundCnt;
    RichEditLog.SelLength := iSelStop - iSelStart + 1;

    if bFound then
      RichEditLog.Perform(EM_SETCHARFORMAT, SCF_SELECTION, LongInt(@cf));
  end;
end;

procedure TTCPMainForm.FormCreate(Sender: TObject);
begin
  PageControlMain.ActivePageIndex := 0;

  FCommProt := TCommunicationProtocol.Create;
  FCommProt.TableID := TTableIdentifier.Create;
  FCommProt.TableID.QRCode := TQRCode.Create;

  FReadLnThread := TReadLnThread.Create(False, FCommProt);
  FReadLnThread.FreeOnTerminate := False;
  FReadLnThread.OnMsgSync := DoOnMsgSync;
  FReadLnThread.OnConnected := DoOnConnSync;
  FReadLnThread.OnInfoSync := DoOnInfoSync;
end;

procedure TTCPMainForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FReadLnThread);
end;

{ TReadLnThread }

procedure TReadLnThread.ConnectServer;
begin
  // Connect to server and logon
  FsInfoText := 'Verbindungsaufbau zu Host: "' + FIdTCPClient.Host + '" - Port: "' + IntToStr(FIdTCPClient.Port) + '" . . .';
  FIdTCPClient.Connect;
  Synchronize(SyncInfo);

  if FIdTCPClient.Connected then
  begin
    FsInfoText := 'Verbindung wurde hergestellt!';
    Synchronize(SyncInfo);
    FCommProt.TableID.XBeeHexID := 'X123456789';
    FIdTCPClient.Socket.WriteLn('LOGON|' + FCommProt.ToSendString);
  end else
  begin
    FsInfoText := 'Verbindung konnte nicht hergestellt werden!';
    Synchronize(SyncInfo);
  end;
end;

constructor TReadLnThread.Create(CreateSuspended: Boolean; aCommProt: ICommunicationProtocol);
begin
  inherited Create(CreateSuspended);
  FCommProt := aCommProt;
  FsWholeMsg := '';
  FbLastConnState := False;
  FbReplyStarted := False;

  FIdTCPClient := TIdTCPClient.Create(nil);
  FIdTCPClient.Port := 60001;
  FIdTCPClient.Host := 'pHL_XE4-PC';

  ConnectServer;
end;

destructor TReadLnThread.Destroy;
begin
  Terminate;
  FIdTCPClient.Socket.WriteLn('DISCONNECT');
  FreeAndNil(FIdTCPClient);
  inherited;
end;

procedure TReadLnThread.Execute;
var
  sRead: String;
  iStartPos: Integer;
  iStopPos: Integer;

begin
  inherited;
  // TODO

  if Terminated then Exit;

  Sleep(200);
  try
//    if not FIdTCPClient.Connected then
//    begin
//      FsInfoText := 'Verbindungsaufbau zu Host: [' + FIdTCPClient.Host + '] - Port: [' + IntToStr(FIdTCPClient.Port) + '] . . .';
//      Synchronize(SyncInfo);
//      FIdTCPClient.Connect;
//    end;

    if FbLastConnState <> FIdTCPClient.Connected then
    begin
      FbLastConnState := FIdTCPClient.Connected;
      Synchronize(SyncConnected);
    end;

    if not FbLastConnState then Exit;

    sRead := FIdTCPClient.Socket.ReadLn();

    iStartPos := Pos(START_CHAR, sRead);
    iStopPos := Pos(END_CHAR, sRead);

    if (iStartPos > 0) AND (iStopPos > 0) then
    begin
      { Die gesamte Message befindet sich im aktuellen String }
      FsWholeMsg := Copy(sRead, iStartPos + 1, iStopPos - iStartPos - 1);
      // ########################## DO SMTH with the reply
      Synchronize(SyncMsg);
      FsWholeMsg := '';
      FbReplyStarted := False;
    end else
    if iStartPos > 0 then
    begin
      { Nur Beginn der Message vorhanden }
      FsWholeMsg := Copy(sRead, iStartPos + 1, Length(sRead) - iStartPos);
      FbReplyStarted := True;
    end else
    if FbReplyStarted then
    begin
      { wir befinden uns nun mitten in der Message }
      if iStopPos > 0 then
      begin
        { Ende der message gefunden }
        FsWholeMsg := FsWholeMsg + Copy(sRead, 1, iStopPos - 1);
        // ########################## DO SMTH with the reply
        Synchronize(SyncMsg);
        FsWholeMsg := '';
        FbReplyStarted := False;
      end else
      begin
        { Ende noch nicht gefunden }
        FsWholeMsg := FsWholeMsg + sRead;
      end;
    end;
  except
    on E: Exception do
    begin
      FsInfoText := 'Exception in ReadLnThread: ' + E.Message;
      Synchronize(SyncInfo);
    end;
  end;
end;

procedure TReadLnThread.SyncMsg;
begin
  if Assigned(FOnMsgSync) then
    FOnMsgSync(FsWholeMsg);
end;

procedure TReadLnThread.ServerLogOn;
begin
  FIdTCPClient.Socket.WriteLn();
end;

procedure TReadLnThread.SyncConnected;
begin
  if Assigned(FOnConnected) then
    FOnConnected(FbLastConnState);
end;

procedure TReadLnThread.SyncInfo;
begin
  if Assigned(FOnInfoSync) then
    FOnInfoSync(FsInfoText);
end;

end.
