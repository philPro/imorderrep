program TITcp;

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  TI.TCP.Main in 'TI.TCP.Main.pas' {TCPMainForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TTCPMainForm, TCPMainForm);
  Application.Run;
end.
